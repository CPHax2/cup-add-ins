﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.AddIn;
using Cup.Extensibility;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    [AddIn("CuPAddIn")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form2 form2;
        public static Form1 form;

        public void OnBoot()
        {
            
        }
        
        public void OnStart()
        {
            if (started)
            {
                form.ShowDialog();
            }
            else
            {
                form = new Form1();
                started = true;
            }
        }

        public void OnExit()
        {
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
            if (form2 != null)
            {
                form2.Close();
                form2.Dispose();
            }
            started = false;
        }
    }
}
