﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;
using System.Net;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
            loadMascotList();
            ApiPipeline.Hook();
        }

        public static Form2 mascotForm;
        List<mascot> mascotList = new List<mascot>();
        int playerID = 0;
        string playerSWID = "";
        string playerName = "";

        public class mascot
        {
            public string mascotID { get; set; }
            public string name { get; set; }
            public string[] IDs { get; set; }
        }

        private void loadMascotList()
        {
            string jsonString = new WebClient().DownloadString("http://media1.clubpenguin.com/play/en/web_service/game_configs/mascots.json");
            string[] json = jsonString.Split('}');
            for (int i = 0; i < json.Length; i++)
            {
                string mascotName = "", mascotId = "";
                string[] ids = null;
                string[] mascotString = json[i].Split('"');
                for (int j = 3; j < mascotString.Length; j++)
                {
                    if (mascotString[j] == "mascot_id")
                    {
                        mascotId = mascotString[++j];
                        mascotId = mascotId.Substring(1, mascotId.Length - 2);
                    }
                    else if (mascotString[j] == "name") mascotName = mascotString[j += 2];
                    else if (mascotString[j] == "ids")
                    {
                        string idString = mascotString[++j];
                        idString = idString.Substring(2, idString.Length - 2);
                        ids = idString.Split(',');
                    }
                }
                mascot newMascot = new mascot { mascotID = mascotId, name = mascotName, IDs = ids };
                mascotList.Add(newMascot);
            }
        }

        private string avatarURL(string swid, int size)
        {
            return "http://cdn.avatar.clubpenguin.com/" + swid + "/cp?size=" + size + "&language=en&photo=true&flag=true&bypassPlayerSettingCache=false";
        }

        private void reset()
        {
            PacketMonitor.Off();
            pictureBox1.Cursor = Cursors.Default;
            pictureBox1.Image = CuPAddIn.Properties.Resources._default;
            playerID = 0;
            playerSWID = "";
            playerName = "";
            nameBox.Clear();
            idBox.Clear();
            swidBox.Clear();
        }

        private void getDetailsByName(string name, Action action)
        {
            Core.SendPacket("%xt%s%u#pbn%-1%" + name + "%");
            PacketMonitor.On("pbn", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                playerName = Packet[6];
                int.TryParse(Packet[5], out playerID);
                playerSWID = Packet[4];
                PacketMonitor.Off();
                action();
            });
        }

        private void getDetailsById(int ID, Action action)
        {
            Core.SendPacket("%xt%s%u#pbi%-1%" + ID + "%");
            PacketMonitor.On("pbi", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                playerName = Packet[6];
                int.TryParse(Packet[5], out playerID);
                playerSWID = Packet[4];
                PacketMonitor.Off();
                action();
            });
        }

        private void getDetailsBySwid(string SWID, Action action)
        {
            Core.SendPacket("%xt%s%u#pbs%-1%" + SWID + "%");
            PacketMonitor.On("pbs", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                playerName = Packet[6];
                int.TryParse(Packet[5], out playerID);
                playerSWID = Packet[4];
                PacketMonitor.Off();
                action();
            });
        }

        private void loadAvatar(string swid)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { loadAvatar(swid); });
                return;
            }
            pictureBox1.LoadAsync(avatarURL(swid, 88));
            pictureBox1.Cursor = Cursors.Hand;
        }

        private bool loadMascotDetails(string name)
        {
            string mascotId = null, mascotName = null;
            string[] ids = null;
            foreach (mascot m in mascotList)
            {
                if (name.Equals(m.name, StringComparison.InvariantCultureIgnoreCase))
                {
                    mascotId = m.mascotID;
                    mascotName = m.name;
                    ids = m.IDs;
                    break;
                }
                else mascotName = null;
            }
            if (mascotName != null)
            {
                int.TryParse(mascotId, out playerID);
                playerName = mascotName;
                mascotForm = new Form2 { name = mascotName, mascotID = mascotId, IDs = ids };
                mascotForm.ShowDialog();
                return true;
            }
            return false;
        }

        private int activePlayerCardID()
        {
            string response = Core.ExecuteFunction("INTERFACE.getActivePlayerId").RawResult;
            double id;
            if (double.TryParse(response, out id)) return (int)id;
            return 0;
        }

        private void convertButton_Click(object sender, EventArgs e)
        {
            reset();
            int ID;
            if (int.TryParse(nameIdSwidBox.Text, out ID))
            {
                getDetailsById(ID, () =>
                {
                    idBox.Text = ID.ToString();
                    nameBox.Text = playerName;
                    swidBox.Text = playerSWID;
                    loadAvatar(playerSWID);
                });
            }
            else if (nameIdSwidBox.Text.Length>2 && nameIdSwidBox.Text[0] == '{' && nameIdSwidBox.Text[nameIdSwidBox.Text.Length - 1] == '}')
            {
                getDetailsBySwid(nameIdSwidBox.Text, () => 
                {
                    if (playerID != 0)
                    {
                        swidBox.Text = playerSWID;
                        nameBox.Text = playerName;
                        idBox.Text = playerID.ToString();
                        loadAvatar(playerSWID);
                    }
                });
            }
            else
            {
                string name = nameIdSwidBox.Text;
                getDetailsByName(name, () =>
                {
                    if (playerID != 0)
                    {
                        nameBox.Text = playerName;
                        idBox.Text = playerID.ToString();
                        swidBox.Text = playerSWID;
                        loadAvatar(playerSWID);
                    }
                    else if (loadMascotDetails(name) == false) MessageBox.Show("Wrong name!");
                });
            }
        }
        
        private void getFromPlayerCardButton_Click(object sender, EventArgs e)
        {
            int ID = activePlayerCardID();
            if (ID > 0)
            {
                reset();
                getDetailsById(ID, () =>
                {
                    idBox.Text = ID.ToString();
                    nameBox.Text = playerName;
                    swidBox.Text = playerSWID;
                    loadAvatar(playerSWID);
                });
            }
            else MessageBox.Show("No player card open!");
        }
        
        private void openPlayerCardButton_Click(object sender, EventArgs e)
        {
            if (playerID > 0) Core.ExecuteFunction("ENGINE.clickPlayer", playerID, playerName);
        }

        private void nameIdBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                convertButton_Click(sender, e);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (playerSWID.Length > 0) System.Diagnostics.Process.Start(avatarURL(playerSWID, 300));
        }

        private void nameBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A) nameBox.SelectAll();
        }

        private void idBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A) idBox.SelectAll();
        }

        private void swidBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A) swidBox.SelectAll();
        }

        
    }
}
