﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form2 : Cup.Extensibility.Library.CuPForm
    {
        public string name;
        public string mascotID;
        public string[] IDs;

        public Form2()
        {
            InitializeComponent();
        }
        
        private void Form2_Load(object sender, EventArgs e)
        {
            textBox1.Text = name;
            textBox2.Text = mascotID;
            string idList = "";
            foreach (string id in IDs) idList += id + ", ";
            idList = idList.Substring(0, idList.Length - 3);
            textBox3.Text = idList;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

    }
}
