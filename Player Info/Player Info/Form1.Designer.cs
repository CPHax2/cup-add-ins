﻿namespace CuPAddIn
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.nameIdSwidBox = new System.Windows.Forms.TextBox();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.idBox = new System.Windows.Forms.TextBox();
            this.swidBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.convertButton = new System.Windows.Forms.Button();
            this.openPlayerCardButton = new System.Windows.Forms.Button();
            this.getFromPlayerCardButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // nameIdSwidBox
            // 
            this.nameIdSwidBox.Location = new System.Drawing.Point(118, 14);
            this.nameIdSwidBox.MaxLength = 38;
            this.nameIdSwidBox.Name = "nameIdSwidBox";
            this.nameIdSwidBox.Size = new System.Drawing.Size(159, 20);
            this.nameIdSwidBox.TabIndex = 1;
            this.nameIdSwidBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.nameIdBox_KeyDown);
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(57, 78);
            this.nameBox.Name = "nameBox";
            this.nameBox.ReadOnly = true;
            this.nameBox.Size = new System.Drawing.Size(220, 20);
            this.nameBox.TabIndex = 6;
            this.nameBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.nameBox_KeyDown);
            // 
            // idBox
            // 
            this.idBox.Location = new System.Drawing.Point(57, 104);
            this.idBox.Name = "idBox";
            this.idBox.ReadOnly = true;
            this.idBox.Size = new System.Drawing.Size(220, 20);
            this.idBox.TabIndex = 8;
            this.idBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.idBox_KeyDown);
            // 
            // swidBox
            // 
            this.swidBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.swidBox.Location = new System.Drawing.Point(57, 130);
            this.swidBox.Name = "swidBox";
            this.swidBox.ReadOnly = true;
            this.swidBox.Size = new System.Drawing.Size(220, 20);
            this.swidBox.TabIndex = 10;
            this.swidBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.swidBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name / ID / SWID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "ID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "SWID:";
            // 
            // convertButton
            // 
            this.convertButton.Location = new System.Drawing.Point(283, 12);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(88, 23);
            this.convertButton.TabIndex = 2;
            this.convertButton.Text = "Convert";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.convertButton_Click);
            // 
            // openPlayerCardButton
            // 
            this.openPlayerCardButton.Location = new System.Drawing.Point(195, 41);
            this.openPlayerCardButton.Name = "openPlayerCardButton";
            this.openPlayerCardButton.Size = new System.Drawing.Size(176, 23);
            this.openPlayerCardButton.TabIndex = 4;
            this.openPlayerCardButton.Text = "Open player card";
            this.openPlayerCardButton.UseVisualStyleBackColor = true;
            this.openPlayerCardButton.Click += new System.EventHandler(this.openPlayerCardButton_Click);
            // 
            // getFromPlayerCardButton
            // 
            this.getFromPlayerCardButton.Location = new System.Drawing.Point(12, 41);
            this.getFromPlayerCardButton.Name = "getFromPlayerCardButton";
            this.getFromPlayerCardButton.Size = new System.Drawing.Size(177, 23);
            this.getFromPlayerCardButton.TabIndex = 3;
            this.getFromPlayerCardButton.Text = "Get details from player card";
            this.getFromPlayerCardButton.UseVisualStyleBackColor = true;
            this.getFromPlayerCardButton.Click += new System.EventHandler(this.getFromPlayerCardButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = global::CuPAddIn.Properties.Resources._default;
            this.pictureBox1.InitialImage = global::CuPAddIn.Properties.Resources._default;
            this.pictureBox1.Location = new System.Drawing.Point(283, 70);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(88, 88);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 170);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.getFromPlayerCardButton);
            this.Controls.Add(this.openPlayerCardButton);
            this.Controls.Add(this.convertButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.swidBox);
            this.Controls.Add(this.idBox);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.nameIdSwidBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Player Info";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameIdSwidBox;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.TextBox idBox;
        private System.Windows.Forms.TextBox swidBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.Button openPlayerCardButton;
        private System.Windows.Forms.Button getFromPlayerCardButton;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}