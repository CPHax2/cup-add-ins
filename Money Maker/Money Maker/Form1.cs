﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
            ApiPipeline.Hook();
        }
        
        System.Timers.Timer timer2 = new System.Timers.Timer();
        string nmb = "";
        private bool working = false;
        private bool inGame()
        {
            return Core.ExecuteFunction("SHELL.getCurrentRoomId").RawResult == "912";
        }
        private bool musicMuted = false, soundMuted = false;
        int coins = 0;
        int timeLeft;

        void sendJoin()
        {
            Core.SendPacket("%xt%s%j#jr%-1%912%0%0%");
        }

        void joinGame(int interval)
        {
            if (interval > 0)
            {
                System.Timers.Timer joinTimer = new System.Timers.Timer { Interval = interval };
                joinTimer.Elapsed += (obj, args) =>
                {
                    if (!inGame()) sendJoin();
                    joinTimer.Dispose();
                };
                joinTimer.Start();
            }
            else sendJoin();
        }

        void exitGame()
        {
            string lastRoomID = Core.ExecuteFunction("SHELL.getLastRoomId").RawResult;
            int roomId;
            if (int.TryParse(lastRoomID, out roomId)) Core.SendPacket("%xt%s%j#jr%-1%" + lastRoomID + "%0%0%");
            else Core.SendPacket("%xt%s%j#jr%-1%810%0%0%");
        }

        void addCoins(int Coins)
        {
            if(inGame()) Core.SendPacket("%xt%z%zo%" + nmb + "%" + Coins + "%");
        }

        void muteMusic()
        {
            Core.ExecuteFunction("MUSIC.muteMusic");
        }

        void unMuteMusic()
        {
            Core.ExecuteFunction("MUSIC.unMuteMusic");
        }

        void muteSound()
        {
            Core.ExecuteFunction("MUSIC.muteSound");
        }

        void unMuteSound()
        {
            Core.ExecuteFunction("MUSIC.unMuteSound");
        }

        bool isIgloo()
        {
            return Core.ExecuteFunction("SHELL.getIsRoomIgloo").RawResult == "true";
        }
        
        private void start()
        {
            working = true;
            Core.ExecuteFunction("SHELL.stopPlayerIdleCheck");
            textBox1.Enabled = false;
            button1.Text = "Cancel";
            label2.Text = "Working...";
            musicMuted = Core.GetVar("MUSIC.isMuted").RawResult == "true";
            soundMuted = Core.GetVar("MUSIC.isSoundMuted").RawResult == "true";
            muteSound();
        }

        private void stop()
        {
            working = false;
            if (inGame()) exitGame();
            Core.ExecuteFunction("SHELL.startPlayerIdleCheck");
            if (!soundMuted) unMuteSound();
            if (!musicMuted) unMuteMusic();
            button1.Text = "Start";
            label3.Text = "";
            textBox1.Enabled = true;
            timer2.Stop();
            timer2.Dispose();
        }

        int timeadd(int coins)
        {
            int x = 168500;
            if (coins < 5000) x = coins * 168 / 5 + 2500;
            if ((coins >= 150) && (coins <= 300)) x = 10500;
            if (coins < 120) x = 4500;
            return x;
        }

        int time(int coins)
        {
            int c = coins;
            int x = 0;
            while (c > 0)
            {
                x += timeadd(c)/1000;
                if (c >= 5000) c -= 5000;
                else c = 0;
                x -= 2;
                if (coins % 5000 == 0) x--;
            }
            x += (coins / 5000) * 3;
            return x;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (working == false)
            {
                int value;
                if (int.TryParse(textBox1.Text, out value))
                {
                    if ((value > 0) && (value <= 1000000))
                    {
                        coins = value;
                        start();
                    }
                    else
                    {
                        if (value <= 0) MessageBox.Show("Number of coins must be higher than 0");
                        else if (value > 1000000) MessageBox.Show("Max amount of coins is 1 000 000");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid number!");
                }

                if (working)
                {
                    if (isIgloo()) Core.SendPacket("%xt%s%j#jr%-1%810%0%0%");
                    if (nmb == "")
                    {
                        joinGame(0);
                        PacketMonitor.On("jg", (packet, mode) =>
                        {
                            nmb = packet.Split('%')[3];
                            PacketMonitor.Off();
                        });
                    }

                    timeLeft = time(coins);
                    var timer = new System.Timers.Timer() { Interval = 1000 };
                    timer.Elapsed += (obj, args) =>
                    {
                        if ((working == false) || (timeLeft < 0))
                        {
                            timer.Stop();
                            timer.Dispose();
                        }
                        else
                        {
                            TimeSpan t = TimeSpan.FromSeconds(timeLeft);
                            BeginInvoke((MethodInvoker)delegate { label3.Text = t.ToString(@"h\:mm\:ss"); });
                            timeLeft--;
                        }
                    };

                    timer2 = new System.Timers.Timer();
                    timer2.Interval = timeadd(coins);
                    timer2.Elapsed += (obj, args) =>
                    {
                        if (working)
                        {
                            if (coins > 5000)
                            {
                                addCoins(5000);
                                coins -= 5000;
                                exitGame();
                                joinGame(3000);
                                timeLeft = time(coins);
                                timer2.Interval = timeadd(coins) + 3000;
                            }
                            else
                            {
                                addCoins(coins);
                                coins = 0;
                                label2.Text = "Done";
                                stop();
                            }
                        }
                    };

                    if (!inGame()) joinGame(500);
                    timer.Start();
                    timer2.Start();
                }
                
            }
            else
            {
                stop();
                label2.Text = "";
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Modifiers == Keys.Control) || (e.Modifiers == (Keys.Control | Keys.Shift)))
            {
                if (e.KeyCode != Keys.A && e.KeyCode != Keys.C && e.KeyCode != Keys.V && e.KeyCode != Keys.X && e.KeyCode != Keys.Z && e.KeyCode != Keys.Y && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else if (e.Modifiers == Keys.Shift)
            {
                if (e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else
            {
                if ((e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) && (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) && (e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete) && (e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Right)) e.SuppressKeyPress = true;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    button1_Click(sender, e);
                }
            }
        }
    }
}
