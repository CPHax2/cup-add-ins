﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public class Game
    {
        private static string position;

        public enum Element : int
        {
            Fire = 'f',
            Water = 'w',
            Snow = 's'
        }

        public enum Color : int
        {
            Red = 'r',
            Blue = 'b',
            Yellow = 'y',
            Green = 'g',
            Orange = 'o',
            Purple = 'p'
        }

        public class Card
        {
            public Element element;
            public Color color;
            public int rank;
        }

        public static void Init()
        {
            PacketMonitor.On("jz", (packet, mode) =>
            {
                position = packet.Split('%')[4];
                AddInBase.form.SetGameMode(true);
                MonitorGame();
            });
            PacketMonitor.On("cjsi", (packet, mode) =>
            {
                PacketMonitor.Off("zm");
                AddInBase.form.Reset();
                AddInBase.form.SetGameMode(false);
            });

        }

        private static void setCardFromString(bool isMyPlayer, string cardStr)
        {
            string[] parts = cardStr.Split('|');
            int id = int.Parse(parts[0]);
            Card card = new Card { rank = int.Parse(parts[3]), element = (Element)parts[2][0], color = (Color)parts[4][0] };
            AddInBase.form.SetNext(isMyPlayer, id, card);
        }

        private static void MonitorGame()
        {
            PacketMonitor.On("zm", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                bool isMyPlayer = Packet[5] == position;
                if (Packet[4] == "deal")
                {
                    if (Packet.Length == 8)
                    {
                        setCardFromString(isMyPlayer, Packet[6]);
                    }
                    else
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            setCardFromString(isMyPlayer, Packet[i + 6]);
                        }
                    }
                    AddInBase.form.ClearSelection();
                }
                else if (Packet[4] == "pick")
                {
                    int id = int.Parse(Packet[6]);
                    AddInBase.form.PickCard(isMyPlayer, id);
                }
            });
        }
    }
}
