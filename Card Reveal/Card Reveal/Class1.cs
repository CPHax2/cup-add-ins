﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn;
using System.Windows.Forms;
using Cup.Extensibility;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    [AddIn("CuPAddIn")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form1 form;

        public void OnBoot()
        {
            
        }
        
        public void OnStart()
        {
            if (!started)
            {
                ApiPipeline.Hook();
                form = new Form1();
                Game.Init();
                started = true;
            }
            else
            {
                form.ShowDialog();
            }
        }

        public void OnExit()
        {
            PacketMonitor.Off();
            started = false;
        }        
    }
}
