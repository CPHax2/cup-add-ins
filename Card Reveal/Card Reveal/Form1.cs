﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        private PictureBox[] OpponentPictures, MyPictures;
        private Label[] OpponentText, MyText;
        private int[] OpponentCardIDs = new int[6], MyCardIDs = new int[6];
        private bool keepOnTop = false;
        System.Drawing.Text.PrivateFontCollection pfc;

        public Form1()
        {
            InitializeComponent();
            pfc = new System.Drawing.Text.PrivateFontCollection();
            int fontLength = Properties.Resources.CPBURBANKSMALLBOLD.Length;
            byte[] fontData = Properties.Resources.CPBURBANKSMALLBOLD;
            IntPtr data = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontLength);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, data, fontLength);
            pfc.AddMemoryFont(data, fontLength);
            OpponentPictures = new PictureBox[] { pictureBox1, pictureBox2, pictureBox3, pictureBox4, pictureBox5, pictureBox11 };
            OpponentText = new Label[] { label1, label2, label3, label4, label5, label11 };
            MyPictures = new PictureBox[] { pictureBox6, pictureBox7, pictureBox8, pictureBox9, pictureBox10, pictureBox12 };
            MyText = new Label[] { label6, label7, label8, label9, label10, label12 };
            label14.Font = new Font(pfc.Families[0], label14.Font.Size);
            label13.Font = new Font(pfc.Families[0], label13.Font.Size);
            label16.Font = new Font(pfc.Families[0], label16.Font.Size);
            label17.Font = new Font(pfc.Families[0], label17.Font.Size);
            foreach (Label lbl in OpponentText) lbl.Font = new Font(pfc.Families[0], lbl.Font.Size);
            foreach (Label lbl in MyText) lbl.Font = new Font(pfc.Families[0], lbl.Font.Size);
            Reset();
        }

        public void SetGameMode(bool enabled)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { SetGameMode(enabled); });
                return;
            }
            foreach (PictureBox pb in OpponentPictures) pb.Visible = enabled;
            foreach (PictureBox pb in MyPictures) pb.Visible = enabled;
            foreach (Label lbl in OpponentText) lbl.Visible = enabled;
            foreach (Label lbl in MyText) lbl.Visible = enabled;
            label13.Visible = enabled;
            label16.Visible = enabled;
            label17.Visible = enabled;
            label14.Visible = !enabled;
        }

        private void setPicture(PictureBox pb, Game.Element e, bool big)
        {
            if (big)
            {
                switch (e)
                {
                    case Game.Element.Fire:
                        pb.BackgroundImage = Properties.Resources.f60;
                        break;
                    case Game.Element.Water:
                        pb.BackgroundImage = Properties.Resources.w60;
                        break;
                    case Game.Element.Snow:
                        pb.BackgroundImage = Properties.Resources.s60;
                        break;
                    default:
                        pb.BackgroundImage = null;
                        break;
                }
            }
            else
            {
                switch (e)
                {
                    case Game.Element.Fire:
                        pb.BackgroundImage = Properties.Resources.f30;
                        break;
                    case Game.Element.Water:
                        pb.BackgroundImage = Properties.Resources.w30;
                        break;
                    case Game.Element.Snow:
                        pb.BackgroundImage = Properties.Resources.s30;
                        break;
                    default:
                        pb.BackgroundImage = null;
                        break;
                }
            }
        }

        private void setRank(bool isMyPlayer, int index, int rank)
        {
            PictureBox pb = isMyPlayer ? MyPictures[index] : OpponentPictures[index];
            Label label = isMyPlayer ? MyText[index] : OpponentText[index];
            label.Text = rank > 0 ? rank.ToString() : "";
            label.Location = new Point(pb.Location.X + (pb.Width - label.Width) / 2, pb.Location.Y + pb.Height + pb.Margin.Bottom + label.Margin.Top);
        }

        private void setColor(bool isMyPlayer, int index, Game.Color color)
        {
            PictureBox pb = isMyPlayer ? MyPictures[index] : OpponentPictures[index];
            switch (color)
            {
                case Game.Color.Red:
                    pb.BackColor = Color.FromArgb(227, 57, 33);
                    break;
                case Game.Color.Blue:
                    pb.BackColor = Color.FromArgb(17, 72, 161);
                    break;
                case Game.Color.Yellow:
                    pb.BackColor = Color.FromArgb(251, 236, 41);
                    break;
                case Game.Color.Green:
                    pb.BackColor = Color.FromArgb(97, 186, 68);
                    break;
                case Game.Color.Orange:
                    pb.BackColor = Color.FromArgb(247, 150, 39);
                    break;
                case Game.Color.Purple:
                    pb.BackColor = Color.FromArgb(164, 154, 203);
                    break;
                default:
                    pb.BackColor = Color.White;
                    break;
            }
        }

        public void SetCard(bool isMyPlayer, int index, int id, Game.Card card)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)delegate { SetCard(isMyPlayer, index, id, card); });
                return;
            }
            PictureBox pb = isMyPlayer ? MyPictures[index] : OpponentPictures[index];
            if (card == null)
            {
                card = new Game.Card { element = 0, rank = 0, color = 0 };
            }
            setPicture(pb, card.element, index == 5);
            pb.Tag = card;
            setRank(isMyPlayer, index, card.rank);
            setColor(isMyPlayer, index, card.color);
            if (isMyPlayer) MyCardIDs[index] = id;
            else OpponentCardIDs[index] = id;
        }

        public void SetNext(bool isMyPlayer, int id, Game.Card card)
        {
            int[] cards = isMyPlayer? MyCardIDs : OpponentCardIDs;
            for (int i = 0; i <= 4; i++)
            {
                if (cards[i] == -1)
                {
                    SetCard(isMyPlayer, i, id, card);
                    break;
                }
            }
        }

        public void PickCard(bool isMyPlayer, int id)
        {
            int[] cards = isMyPlayer ? MyCardIDs : OpponentCardIDs;
            for (int i = 0; i < 5; i++)
            {
                if (cards[i] == id)
                {
                    Game.Card card = isMyPlayer ? (Game.Card)MyPictures[i].Tag : (Game.Card)OpponentPictures[i].Tag;
                    string text = isMyPlayer ? MyText[i].Text : OpponentText[i].Text;
                    SetCard(isMyPlayer, 5, id, card);
                    SetCard(isMyPlayer, i, -1, null);
                    break;
                }
            }
        }

        public void ClearSelection()
        {
            SetCard(false, 5, -1, null);
            SetCard(true, 5, -1, null);
        }

        public void Reset()
        {
            for (int i = 0; i <= 5; i++)
            {
                SetCard(false, i, -1, null);
                SetCard(true, i, -1, null);
            }
        }

        private void pinButton_Click(object sender, EventArgs e)
        {
            keepOnTop = !keepOnTop;
            if (keepOnTop)
            {
                this.TopMost = true;
                pinButton.BackgroundImage = global::CuPAddIn.Properties.Resources.unpin;
                toolTip1.SetToolTip(pinButton, "Do not keep on top");
            }
            else
            {
                this.TopMost = false;
                pinButton.BackgroundImage = global::CuPAddIn.Properties.Resources.pin;
                toolTip1.SetToolTip(pinButton, "Keep on top");
            }
        }
    }
}
