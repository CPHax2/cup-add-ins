﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;
using System.Net;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public static bool working = false;
        public static System.Timers.Timer timer;

        public Form1()
        {
            InitializeComponent();
            loadMascotList();
            ApiPipeline.Hook();
        }

        public class mascot
        {
            public int mascotID { get; set; }
            public string name { get; set; }

            public override string ToString()
            {
                return name;
            }
        }

        private void loadMascotList()
        {
            string jsonString = new WebClient().DownloadString("http://media1.clubpenguin.com/play/en/web_service/game_configs/mascots.json");
            string[] json = jsonString.Split('}');
            for (int i = 0; i < json.Length - 2; i++)
            {
                string mascotName = "";
                int mascotId = 0;
                string[] mascotString = json[i].Split('"');
                for (int j = 3; j < mascotString.Length; j++)
                {
                    if (mascotString[j] == "mascot_id")
                    {
                        string sMascotId = mascotString[++j];
                        sMascotId = sMascotId.Substring(1, sMascotId.Length - 2);
                        int.TryParse(sMascotId, out mascotId);
                    }
                    else if (mascotString[j] == "name") mascotName = mascotString[j += 2];
                }
                mascot newMascot = new mascot { mascotID = mascotId, name = mascotName };
                comboBox1.Items.Add(newMascot);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void addMascot(int mascotID)
        {
            Core.ExecuteFunction("SHELL.addMascotFriend", mascotID);
        }

        private void enable()
        {
            comboBox1.Enabled = true;
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
        }

        private void disable()
        {
            comboBox1.Enabled = false;
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
        }

        private void removeMascot(int mascotID)
        {
            Core.ExecuteFunction("SHELL.removeMascotFriend", mascotID);
        }

        private void start(bool isAdd)
        {
            disable();
            if (isAdd)
            {
                button3.Text = "Cancel";
                button3.Enabled = true;
            }
            else
            {
                button4.Text = "Cancel";
                button4.Enabled = true;
            }
            working = true;
            int count = comboBox1.Items.Count;
            int i = 0;
            timer = new System.Timers.Timer { Interval = 250 };
            timer.Elapsed += (obj, args) => 
            {
                if (i++ < count)
                {
                    int mascotID = ((mascot)comboBox1.Items[i - 1]).mascotID;
                    if (isAdd)
                    {
                        addMascot(mascotID);
                        label2.Text = "Adding mascots: " + i.ToString() + "/" + count.ToString();
                    }
                    else
                    {
                        removeMascot(mascotID);
                        label2.Text = "Removing mascots: " + i.ToString() + "/" + count.ToString();
                    }
                    
                }
                else stop();
            };
            timer.Start();
        }

        private void stop()
        {
            timer.Dispose();
            working = false;
            enable();
            button3.Text = "Add all";
            button4.Text = "Remove all";
            label2.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int mascotID = ((mascot)comboBox1.SelectedItem).mascotID;
            if (mascotID != 0) addMascot(mascotID);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int mascotID = ((mascot)comboBox1.SelectedItem).mascotID;
            if (mascotID != 0) removeMascot(mascotID);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!working)
            {
                start(true);
            }
            else stop();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!working)
            {
                start(false);
            }
            else stop();
        }

        private void Form1_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            MessageBox.Show("This add-in allows you to add or remove mascots from your Friends list."
            + "\r\nNote: After removing a mascot you may need to re-login in order to add it again.", "Information");
            e.Cancel = true;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(working) stop();
        }

    }
}
