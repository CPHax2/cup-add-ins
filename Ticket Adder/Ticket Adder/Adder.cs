﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    class Adder
    {
        private static int ticketsPerSecond = 400, maxTickets = 2000, minTime = 6000, ticketsLeft;
        public static System.Timers.Timer AddTimer;
        private static System.Timers.Timer loadingScreenTimer = new System.Timers.Timer { Interval = 300 };
        public static bool Working = false;
        private static bool unMuteMusicOnFinish = true;
        private static DateTime joinTime;
        private static string internalId;

        public static void Init()
        {
            loadingScreenTimer.Elapsed += (obj, args) =>
            {
                Core.ExecuteFunction("SHELL.showLoadingScreen", "Adding Tickets...");
                loadingScreenTimer.Stop();
            };
        }

        private static void join()
        {
            Core.SendPacket("%xt%s%j#jr%-1%946%0%0%");
        }

        private static void leave()
        {
            Core.ExecuteFunction("SHELL.sendJoinLastRoom");
        }

        private static void addTickets(int tickets)
        {
            Core.SendPacket("%xt%s%fair#fendgame%" + internalId + "%" + tickets + "%");
            ticketsLeft -= tickets;
        }

        private static void muteMusic()
        {
            Core.ExecuteFunction("MUSIC.muteMusic");
        }

        private static void unMuteMusic()
        {
            Core.ExecuteFunction("MUSIC.unMuteMusic");
        }

        private static int timeFromTickets(int tickets)
        {
            int time = tickets * 1000 / ticketsPerSecond + 50;
            return time > minTime ? time : minTime;
        }

        private static int totalTime(int tickets)
        {
            int time = 0;
            while (tickets > 0)
            {
                if (tickets > maxTickets)
                {
                    time += timeFromTickets(maxTickets);
                    tickets -= maxTickets;
                }
                else
                {
                    time += timeFromTickets(tickets);
                    tickets = 0;
                }
            }
            return time;
        }

        private static void stop(bool complete)
        {
            AddTimer.Stop();
            if (!complete)
            {
                TimeSpan timeSinceStart = DateTime.Now - joinTime;
                int tickets = (int)timeSinceStart.TotalSeconds * ticketsPerSecond;
                addTickets(tickets);
                AddInBase.form.Stop("Cancelled");
            }
            else AddInBase.form.Stop("Done");
            leave();
            if (unMuteMusicOnFinish) unMuteMusic();
            Working = false;
        }

        public static void Start(int tickets)
        {
            if (AddTimer != null) AddTimer.Dispose();
            ticketsLeft = tickets;
            tickets = tickets < maxTickets ? tickets : maxTickets;
            AddTimer = new System.Timers.Timer { Interval = timeFromTickets(tickets) };
            AddTimer.Elapsed += (obj, args) =>
            {
                addTickets(tickets);
                if (ticketsLeft > 0)
                {
                    PacketMonitor.On("jr", (packet, mode) =>
                    {
                        PacketMonitor.Off("jr");
                        Start(ticketsLeft);
                    });
                    leave();
                }
                else stop(true);
            };
            PacketMonitor.On("fair#fstartgame", (packet, mode) =>
            {
                PacketMonitor.On("fair#fstartgame", null);
                internalId = packet.Split('%')[4];
                loadingScreenTimer.Start();
                joinTime = DateTime.Now;
                AddTimer.Start();
                AddInBase.form.StartCountdown(totalTime(ticketsLeft));
            }, PacketMode.Send);
            if (!Working) unMuteMusicOnFinish = Core.GetVar("MUSIC.isMuted").RawResult != "true";
            muteMusic();
            Working = true;
            join();
        }

        public static void Cancel()
        {
            stop(false);
            loadingScreenTimer.Stop();
            PacketMonitor.Off();
            Working = false;
        }

    }
}
