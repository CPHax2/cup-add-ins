﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
            finishedTimer.Elapsed += (obj, args) =>
            {
                label2.Text = "";
                this.Height = 62;
            };
        }

        private static System.Timers.Timer displayTimer, finishedTimer = new System.Timers.Timer { Interval = 20000 };

        private string timeString(TimeSpan t)
        {
            return string.Format("{0}:{1}", (int)t.TotalHours, t.ToString(@"mm\:ss"));
        }

        private void Start()
        {
            finishedTimer.Stop();
            button1.Text = "Cancel";
            textBox1.Enabled = false;
        }

        public void Stop(string status)
        {
            textBox1.Enabled = true;
            button1.Text = "Add";
            button1.Enabled = true;
            if (displayTimer != null) displayTimer.Dispose();
            this.Height = 78;
            label2.Text = status;
            finishedTimer.Start();
        }

        public void UpdateStatus(string text)
        {
            label2.Text = text;
        }

        public void StartCountdown(int time)
        {
            if (displayTimer != null) displayTimer.Dispose();
            TimeSpan t = TimeSpan.FromMilliseconds(time);
            displayTimer = new System.Timers.Timer { Interval = 1000 };
            displayTimer.Elapsed += (obj, args) => { t = t.Subtract(TimeSpan.FromSeconds(1)); };
            if (t.TotalSeconds < 3600) displayTimer.Elapsed += (obj, args) => { label2.Text = t.ToString(@"mm\:ss"); };
            else displayTimer.Elapsed += (obj, args) => { label2.Text = timeString(t); };
            this.Height = 78;
            label2.Text = t.TotalSeconds < 3600 ? t.ToString(@"mm\:ss") : timeString(t);
            displayTimer.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Adder.Working)
            {
                Adder.Cancel();
                return;
            }
            int tickets;
            if (!int.TryParse(textBox1.Text, out tickets)) MessageBox.Show("Invalid number!");
            else if (tickets <= 0) MessageBox.Show("The number of tickets must be higher than 0!");
            else if (tickets > 1000000) MessageBox.Show("You can not add more than 1 000 000 tickets!");
            else
            {
                Start();
                Adder.Start(tickets);
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Adder.Working) return;
            if ((e.Modifiers == Keys.Control) || (e.Modifiers == (Keys.Control | Keys.Shift)))
            {
                if (e.KeyCode != Keys.A && e.KeyCode != Keys.C && e.KeyCode != Keys.V && e.KeyCode != Keys.Z && e.KeyCode != Keys.Y && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else if (e.Modifiers == Keys.Shift)
            {
                if (e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else
            {
                if ((e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) && (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) && (e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete) && (e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Right)) e.SuppressKeyPress = true;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    button1_Click(sender, e);
                }
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!Adder.Working)
            {
                finishedTimer.Stop();
                label2.Text = "";
                this.Height = 62;
            }
        }

    }
}
