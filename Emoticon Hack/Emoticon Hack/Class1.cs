﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.AddIn;
using Cup.Extensibility;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    [AddIn("CuPAddIn")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form1 form;

        public void OnBoot()
        {

        }

        public void OnStart()
        {
            if (started)
            {
                form.ShowDialog();
            }
            else
            {
                started = true;
                ApiPipeline.Hook();
                form = new Form1();
                AuxServer.OnPacketReceived = (type, packet) =>
                {
                    if (type == AuxServer.Type.SendRoom)
                    {
                        int playerID = Convert.ToInt32(packet.Split('|')[0]);
                        int emote = Convert.ToInt32(packet.Split('|')[1]);
                        Core.ExecuteFunction("INTERFACE.showEmoteBalloon", playerID, emote);
                    }
                };
            }
        }

        public void OnExit()
        {
            if(form!=null)
            {
                form.Close();
                form.Dispose();
            }
            AuxServer.OnPacketReceived = null;
            started = false;
        }
    }
}
