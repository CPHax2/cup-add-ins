﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
            ApiPipeline.Hook();
        }

        bool isServerSide()
        {
            return radioButton2.Checked;
        }

        private void sendServerSide(int e)
        {
            Core.ExecuteFunction("INTERFACE.sendEmote", e);
        }

        private void sendAuxServer(int e)
        {
            AuxServer.SendRoom(LocalPlayer.Id + "|" + e.ToString());
            Core.ExecuteFunction("INTERFACE.showEmoteBalloon", LocalPlayer.Id, e);
        }

        private void send(int e)
        {
            if (!isServerSide()) sendAuxServer(e);
            else sendServerSide(e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            send(31);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            send(32);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            send(33);
        }
        
        private void button4_Click(object sender, EventArgs e)
        {
            int emoteId;
            if (int.TryParse(textBox1.Text, out emoteId)) sendAuxServer(emoteId);
            else MessageBox.Show("Invalid ID!");
        }
        
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                if (MessageBox.Show("If a moderator sees you using unavailable emotes, you may get banned!\r\n" +
                "Do you want to continue?",
                "Emoticon Hack", MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2) ==
                DialogResult.No)
                {
                    radioButton1.Checked = true;
                }
            }
            button4.Enabled = textBox1.Enabled = radioButton1.Checked;
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button4_Click(sender, e);
                e.SuppressKeyPress = true;
            }
        }

    }
}
