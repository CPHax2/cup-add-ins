﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;
using System.Net;
using System.Text.RegularExpressions;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        private bool working = false,
        isAddingGames = false,
        isAddAll = false,
        wasMusicMuted = false,
        wasSoundMuted = false,
        cancelled = false;
        private static System.Timers.Timer timer = new System.Timers.Timer { Interval = 300 },
        addAllTimer = new System.Timers.Timer(),
        loadingScreenTimer = new System.Timers.Timer { Interval = 1000 },
        freezeTimer = new System.Timers.Timer { Interval = 3050 };

        private void setupTimers()
        {
            loadingScreenTimer.Elapsed += (obj, args) =>
            {
                showLoadingScreen();
                loadingScreenTimer.Stop();
            };
            freezeTimer.Elapsed += (obj, args) =>
            {
                if (InvokeRequired) BeginInvoke((MethodInvoker)delegate { button2.Enabled = true; });
                else button2.Enabled = true;
                freezeTimer.Stop();
            };
        }
        
        private void loadCategories()
        {
            comboBox1.Items.Add(new StampAdder.Category { Name = "Party", StampsList = StampAdder.partyStamps });
            comboBox1.Items.Add(new StampAdder.Category { Name = "Activities", StampsList = StampAdder.ActivitiesStamps });
            comboBox1.Items.Add(new StampAdder.Category { Name = "Games", StampsList = StampAdder.GameStamps });
            comboBox1.SelectedIndex = 0;
        }

        public Form1()
        {
            InitializeComponent();
            setupTimers();
            StampAdder.loadGameStamps();
            loadCategories();
            ApiPipeline.Hook();
        }

        private void start()
        {
            working = true;
            cancelled = false;
            comboBox1.Enabled = false;
            comboBox2.Enabled = false;
            button2.Enabled = false;
            if (!isAddAll)
            {
                button1.Enabled = true;
                button1.Text = "Cancel";
                button3.Enabled = false;
            }
            else
            {
                button1.Enabled = false;
                button3.Enabled = true;
                button3.Text = "Cancel";
            }
            showLoadingScreen();
            wasMusicMuted = Core.GetVar("MUSIC.isMuted").RawResult == "true";
            wasSoundMuted = Core.GetVar("MUSIC.isSoundMuted").RawResult == "true";
            muteMusic();
            muteSound();
        }

        private void stop(string status)
        {
            if (timer != null) timer.Dispose();
            working = false;
            PacketMonitor.Off();
            button1.Text = "Add all stamps in category";
            button3.Text = "Add all";
            setStatusText(status);
            if (isAddingGames)
            {
                disconnect();
                button1.Enabled = false;
                button3.Enabled = false;
                return;
            }
            loadingScreenTimer.Stop();
            if (!wasSoundMuted) unMuteSound();
            if (!wasMusicMuted) unMuteMusic();
            comboBox1.Enabled = true;
            comboBox2.Enabled = true;
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            System.Timers.Timer hideTimer = new System.Timers.Timer { Interval = 1050 };
            hideTimer.Elapsed += (obj, args) => 
            {
                hideLoadingScreen();
                hideTimer.Dispose();
            };
            hideTimer.Start();            
        }

        private string currentRoomID()
        {
            return Core.ExecuteFunction("SHELL.getCurrentRoomId").RawResult;
        }

        private bool isInRequiredRoom(int requiredRoomID)
        {
            if (requiredRoomID <= 0 || currentRoomID() == requiredRoomID.ToString()) return true;
            return false;
        }

        private void teleport(int ID)
        {
            Core.SendPacket("%xt%s%j#jr%-1%" + ID + "%0%0%");
            showLoadingScreen();
            loadingScreenTimer.Start();
        }

        private void showLoadingScreen()
        {
            Core.ExecuteFunction("SHELL.showLoadingScreen", "Adding Stamps...");
        }

        private void hideLoadingScreen()
        {
            Core.ExecuteFunction("SHELL.hideLoadingScreen");
        }

        private void disconnect()
        {
            Core.ExecuteFunction("SHELL.AIRTOWER._airtower.server.disconnect");
            Core.ExecuteFunction("SHELL.AIRTOWER._airtower.server.connectionClosed");
        }

        private void muteMusic()
        {
            Core.ExecuteFunction("MUSIC.muteMusic");
        }

        private void unMuteMusic()
        {
            Core.ExecuteFunction("MUSIC.unMuteMusic");
        }

        private void muteSound()
        {
            Core.ExecuteFunction("MUSIC.muteSound");
        }

        private void unMuteSound()
        {
            Core.ExecuteFunction("MUSIC.unMuteSound");
        }

        private void addStamp(StampAdder.Stamp stamp)
        {
            int ID = 0;
            int.TryParse(stamp.ID, out ID);
            if (isInRequiredRoom(stamp.RoomID)) Core.ExecuteFunction("SHELL.stampEarned", ID, false);
        }

        private void addCategory(List<StampAdder.Stamp> stampList, string categoryName)
        {
            int count = stampList.Count;
            start();
            isAddingGames = false;
            int i = 0;
            timer = new System.Timers.Timer { Interval = 3050 };
            int RoomID = stampList[0].RoomID;
            if (!isInRequiredRoom(RoomID)) teleport(RoomID);
            else addStamp(stampList[i++]);
            updateStatus(categoryName, i, count);
            timer.Elapsed += (obj, args) =>
            {
                if (i < count && working)
                {
                    addStamp(stampList[i++]);
                    updateStatus(categoryName, i, count);
                    if (i < count)
                    {
                        RoomID = stampList[i].RoomID;
                        if (!isInRequiredRoom(RoomID)) teleport(RoomID);
                        showLoadingScreen();
                    }
                }
                else stop("Done");
            };
            timer.Start();
            PacketMonitor.On("epfgr", (packet, mode) =>
            {
                showLoadingScreen();
                loadingScreenTimer.Start();
            });
        }

        private void addAllGames(List<StampAdder.Stamp> stampList)
        {
            start();
            isAddingGames = true;
            timer = new System.Timers.Timer { Interval = 300 };
            string gameName = stampList[0].Name;
            if (gameName == "Astro Barrier" || gameName == "System Defender" || gameName == "Thin Ice") timer.Interval = 3050;
            int i = 0, count = stampList.Count;
            timer.Elapsed += (obj, args) =>
            {
                if (i < count)
                {
                    if (stampList[i].RoomID <= 0)
                    {
                        showLoadingScreen();
                        if ((!stampList[i].IsMember || LocalPlayer.IsMember) && !StampAdder.hasStamp(stampList[i].ID)) addStamp(stampList[i]);
                    }
                    else
                    {
                        teleport(stampList[i].RoomID);
                        gameName = stampList[i].Name;
                        if (gameName == "Astro Barrier" || gameName == "System Defender" || gameName == "Thin Ice") timer.Interval = 3050;
                        else timer.Interval = 300;
                        setStatusText("Adding " + gameName + " stamps...");
                    }
                    i++;
                }
                else stop("Done");
            };
            teleport(stampList[i++].RoomID);
            setStatusText("Adding " + gameName + " stamps...");
            timer.Start();
        }

        private void setStatusText(string text)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { setStatusText(text); });
                return;
            }
            toolStripStatusLabel1.Text = text;
        }

        private void updateStatus(string category, int added, int totalCount)
        {
            setStatusText("Adding " + category + " Stamps: " + added.ToString() + "/" + totalCount.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            isAddAll = false;
            if (working == false)
            {
                isAddingGames = ((StampAdder.Category)comboBox1.SelectedItem).Name == "Games";
                System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {
                    button1.Enabled = false;
                    button2.Enabled = false;
                    button3.Enabled = false;
                    setStatusText("Making list of stamps to add...");
                    if (!isAddingGames)
                    {
                        List<StampAdder.Stamp> stampList = StampAdder.stampsToAdd(((StampAdder.Category)comboBox1.SelectedItem).StampsList);
                        setStatusText("");
                        if (stampList.Count != 0) addCategory(stampList, ((StampAdder.Category)comboBox1.SelectedItem).Name);
                        else
                        {
                            button1.Enabled = true;
                            button2.Enabled = true;
                            button3.Enabled = true;
                            MessageBox.Show("No stamps to add!");
                        }
                    }
                    else
                    {
                        List<StampAdder.Stamp> stampList = StampAdder.stampsToAdd(StampAdder.GameStamps);
                        setStatusText("");
                        if (stampList.Count != 0) addAllGames(stampList);
                        else
                        {
                            isAddingGames = false;
                            button1.Enabled = true;
                            button3.Enabled = true;
                            MessageBox.Show("No stamps to add!");
                        }
                    }
                }));
                thread.Start();
            }
            else stop("Cancelled");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StampAdder.Stamp stamp = (StampAdder.Stamp)comboBox2.SelectedItem;
            if (StampAdder.hasStamp(stamp.ID)) MessageBox.Show("You already have this stamp!");
            else if (stamp.IsMember && !LocalPlayer.IsMember) MessageBox.Show("Non-members can not add member stamps!");
            else
            {
                button2.Enabled = false;
                if (isInRequiredRoom(stamp.RoomID))
                {
                    addStamp(stamp);
                    freezeTimer.Start();
                }
                else
                {
                    teleport(stamp.RoomID);
                    var wait = new System.Timers.Timer { Interval = 500 };
                    wait.Elapsed += (obj, args) => 
                    {
                        addStamp(stamp);
                        freezeTimer.Start();
                        wait.Dispose();
                    };
                    wait.Start();
                }
            }
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            if (!working)
            {
                isAddAll = true;
                List<StampAdder.Stamp> party = new List<StampAdder.Stamp>();
                List<StampAdder.Stamp> activities = new List<StampAdder.Stamp>();
                List<StampAdder.Stamp> games = new List<StampAdder.Stamp>();
                System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() => 
                {
                    button1.Enabled = false;
                    button2.Enabled = false;
                    button3.Enabled = false;
                    setStatusText("Making list of stamps to add...");
                    party = StampAdder.stampsToAdd(StampAdder.partyStamps);
                    activities = StampAdder.stampsToAdd(StampAdder.ActivitiesStamps);
                    games = StampAdder.stampsToAdd(StampAdder.GameStamps);
                    setStatusText("");
                    button3.Enabled = true;
                    bool activitiesAdded = false;
                    addAllTimer = new System.Timers.Timer { Interval = 1000 };
                    addAllTimer.Elapsed += (obj, args) =>
                    {
                        if (!cancelled && !working)
                        {
                            if (!activitiesAdded && activities.Count != 0)
                            {
                                addCategory(activities, "Activities");
                                activitiesAdded = true;
                            }
                            else if (games.Count != 0)
                            {
                                addAllGames(games);
                                addAllTimer.Dispose();
                            }
                            else
                            {
                                stop("Done");
                                addAllTimer.Dispose();
                            }
                        }
                    };
                    if (party.Count != 0)
                    {
                        addCategory(party, "Party");
                        addAllTimer.Start();
                    }
                    else if (activities.Count != 0)
                    {
                        addCategory(activities, "Activities");
                        activitiesAdded = true;
                        addAllTimer.Start();
                    }
                    else if (games.Count != 0) addAllGames(games);
                    else
                    {
                        button1.Enabled = true;
                        button2.Enabled = ((StampAdder.Category)comboBox1.SelectedItem).Name != "Games";
                        MessageBox.Show("No stamps to add!");
                    }
                }));
                thread.Start();
            }
            else
            {
                addAllTimer.Dispose();
                stop("Cancelled");
                cancelled = true;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            if (((StampAdder.Category)comboBox1.SelectedItem).Name == "Games")
            {
                comboBox2.Enabled = false;
                button2.Enabled = false;
            }
            else
            {
                foreach (StampAdder.Stamp stamp in ((StampAdder.Category)comboBox1.SelectedItem).StampsList)
                {
                    comboBox2.Items.Add(stamp);
                }
                comboBox2.SelectedIndex = 0;
                comboBox2.Enabled = true;
                button2.Enabled = true;
            }
        }
        
        private void comboBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) button2_Click(sender, e);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (working) stop("");
        }
        
    }
}
