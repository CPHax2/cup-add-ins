﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    class StampAdder
    {
        public class Stamp
        {
            public string ID { get; set; }
            public string Name { get; set; }
            public bool IsMember { get; set; }
            public int RoomID { get; set; }

            public override string ToString()
            {
                return Name + (IsMember ? " (member)" : "");
            }
        }

        public class Category
        {
            public string Name { get; set; }
            public List<Stamp> StampsList { get; set; }
            public int RoomID { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }

        public class Game
        {
            public string Name { get; set; }
            public int ID { get; set; }
        }

        public static List<Stamp> GameStamps = new List<Stamp>(),

        partyStamps = new List<Stamp>
        { 
            new Stamp { Name = "Party Puffle", ID = "330", IsMember = false }, 
            new Stamp { Name = "Snack Shack", ID = "184", IsMember = false },
            new Stamp { Name = "Celebration", ID = "186", IsMember = false },
            new Stamp { Name = "Construction", ID = "189", IsMember = false },
            new Stamp { Name = "Explorer", ID = "190", IsMember = true },
            new Stamp { Name = "Volunteer", ID = "191", IsMember = false },
            new Stamp { Name = "Out At Sea", ID = "292", IsMember = false },
            new Stamp { Name = "Food Fight", ID = "332", IsMember = false },
            new Stamp { Name = "Go Green", ID = "362", IsMember = false },
            new Stamp { Name = "Snowboarder", ID = "440", IsMember = true },
            new Stamp { Name = "Happy Room", ID = "182", IsMember = false },
            new Stamp { Name = "Party Puzzle", ID = "183", IsMember = false },
            new Stamp { Name = "Target Champion", ID = "185", IsMember = false },
            new Stamp { Name = "Monster Mash", ID = "187", IsMember = false },
            new Stamp { Name = "Scavenger Hunt", ID = "188", IsMember = false },
            new Stamp { Name = "Path Finder", ID = "193", IsMember = false },
            new Stamp { Name = "Noble Knight", ID = "360", IsMember = true },
            new Stamp { Name = "Tree Mob", ID = "364", IsMember = false },
            new Stamp { Name = "Music Maestro", ID = "426", IsMember = false },
            new Stamp { Name = "Top Volunteer", ID = "294", IsMember = false },
            new Stamp { Name = "Stunt Penguin", ID = "438", IsMember = false },
            new Stamp { Name = "Mountaineer", ID = "439", IsMember = false },
            new Stamp { Name = "Epic Volunteer", ID = "296", IsMember = false }
        },

        ActivitiesStamps = new List<Stamp>
        {
            new Stamp { Name = "Stage Crew", ID = "9", IsMember = false, RoomID = 340 },
            new Stamp { Name = "Underground", ID = "10", IsMember = false },
            new Stamp { Name = "Snapshot", ID = "11", IsMember = false, RoomID = 230 },
            new Stamp { Name = "Go Swimming", ID = "12", IsMember = false, RoomID = 806 },
            new Stamp { Name = "Clock Target", ID = "13", IsMember = false, RoomID = 801 },
            new Stamp { Name = "Puffle Dig", ID = "489", IsMember = false },
            new Stamp { Name = "Tasty Treasure", ID = "495", IsMember = true },
            new Stamp { Name = "Going Places", ID = "15", IsMember = false },
            new Stamp { Name = "Dance Party", ID = "16", IsMember = false, RoomID = 120 },
            new Stamp { Name = "Igloo Party", ID = "17", IsMember = false },
            new Stamp { Name = "Coffee Server", ID = "18", IsMember = false, RoomID = 110 },
            new Stamp { Name = "Pizza Waiter", ID = "19", IsMember = false, RoomID = 330 },
            new Stamp { Name = "First Dig", ID = "490", IsMember = false },
            new Stamp { Name = "Every Color", ID = "491", IsMember = false },
            new Stamp { Name = "Dig All Day", ID = "492", IsMember = false },
            new Stamp { Name = "Puffle Owner", ID = "21", IsMember = false },
            new Stamp { Name = "Floor Filler", ID = "22", IsMember = false, RoomID = 120 },
            new Stamp { Name = "Full House", ID = "23", IsMember = true },
            new Stamp { Name = "Play It Loud!", ID = "24", IsMember = false, RoomID = 410 },
            new Stamp { Name = "Berg Drill", ID = "26", IsMember = false, RoomID = 805 },
            new Stamp { Name = "Fort Battle", ID = "27", IsMember = false, RoomID = 801 },
            new Stamp { Name = "Hockey Team", ID = "29", IsMember = false },
            new Stamp { Name = "Big Dig", ID = "493", IsMember = false },
            new Stamp { Name = "Treasure Box", ID = "494", IsMember = true },
            new Stamp { Name = "Soccer Team", ID = "25", IsMember = false },
            new Stamp { Name = "Party Host", ID = "28", IsMember = false },
            new Stamp { Name = "Ninja Meeting", ID = "30", IsMember = false, RoomID = 320 },
            new Stamp { Name = "3 Gems", ID = "488", IsMember = false }
        };

        public static List<Game> Games = new List<Game>
        {
            new Game{ Name = "Astro Barrier", ID = 900},
            new Game{ Name = "System Defender", ID = 950},
            new Game{ Name = "Thin Ice", ID = 909},
            new Game{ Name = "Aqua Grabber", ID = 916},
            new Game{ Name = "Cart Surfer", ID = 905},
            new Game{ Name = "Catchin' Waves", ID = 912},
            new Game{ Name = "Ice Fishing", ID = 904},
            new Game{ Name = "Jet Pack Adventure", ID = 906},
            new Game{ Name = "Pizzatron 3000", ID = 910},
            new Game{ Name = "Puffle Launch", ID = 955},
            new Game{ Name = "Puffle Rescue", ID = 949},
            new Game{ Name = "Pufflescape", ID = 957},
            new Game{ Name = "Smoothie Smash", ID = 959}
        };

        public static void loadGameStamps()
        {
            string jsonString = new WebClient().DownloadString("http://media1.clubpenguin.com/play/en/web_service/game_configs/stamps.json");
            jsonString = jsonString.Substring(2, jsonString.Length - 6);
            string[] json = Regex.Split(jsonString, "]},{");
            int startIndex = 0;
            for (int i = 0; i < json.Length; i++)
            {
                if (json[i].Split('"')[3] == "Games")
                {
                    startIndex = i + 1;
                    break;
                }
            }
            List<Category> tempList = new List<Category>();
            for (int i = startIndex; i < json.Length - 1; i++)
            {
                if (json[i].Split('"')[10] == ":8,")
                {
                    string categoryName = json[i].Split('"')[3];
                    if (categoryName == "Missions" || categoryName == "Treasure Hunt" || (categoryName.Length >= 10 && categoryName.Substring(0, 10) == "Card-Jitsu")) continue;
                    List<Stamp> stampsList = new List<Stamp>();
                    string[] stamps = (json[i].Split('[')[1]).Split('}');
                    for (int j = 0; j < stamps.Length - 1; j++)
                    {
                        string stampID = stamps[j].Split('"')[2];
                        stampID = stampID.Substring(1, stampID.Length - 2);
                        string stampName = stamps[j].Split('"')[5];
                        bool isMember = false;
                        string member = stamps[j].Split('"')[8];
                        member = member.Substring(1, member.Length - 2);
                        bool.TryParse(member, out isMember);
                        stampsList.Add(new Stamp { ID = stampID, Name = stampName, IsMember = isMember });
                    }
                    if (categoryName == "Astro Barrier" || categoryName == "System Defender" || categoryName == "Thin Ice")
                    {
                        GameStamps.Add(new Stamp { Name = categoryName, RoomID = gameIdByName(categoryName) });
                        foreach (Stamp stamp in stampsList)
                        {
                            GameStamps.Add(stamp);
                        }
                    }
                    else tempList.Add(new Category { Name = categoryName, StampsList = stampsList, RoomID = gameIdByName(categoryName) });
                }
                else break;
            }
            foreach (Category game in tempList)
            {
                GameStamps.Add(new Stamp { Name = game.Name, RoomID = game.RoomID });
                foreach (Stamp stamp in game.StampsList)
                {
                    GameStamps.Add(stamp);
                }
            }
        }

        private static int gameIdByName(string name)
        {
            int ID = -1;
            foreach (Game game in Games)
            {
                if (game.Name == name)
                {
                    ID = game.ID;
                    break;
                }
            }
            return ID;
        }

        public static bool hasStamp(string ID)
        {
            return Core.ExecuteFunction("SHELL.stampIsOwnedByMe", ID).RawResult == "true";
        }

        public static List<Stamp> stampsToAdd(List<Stamp> stampList)
        {
            bool member = LocalPlayer.IsMember;
            List<Stamp> stamps = new List<Stamp>();
            foreach (Stamp stamp in stampList)
            {
                if (stamp.ID == null || ((!stamp.IsMember || member) && !hasStamp(stamp.ID))) stamps.Add(stamp);
            }
            for (int i = 0; i < stamps.Count; i++)
            {
                if (stamps[i].ID == null)
                {
                    if (i == stamps.Count - 1) stamps.RemoveAt(i);
                    else if (stamps[i + 1].ID == null)
                    {
                        stamps.RemoveAt(i--);
                    }
                }
            }
            return stamps;
        }
    }
}
