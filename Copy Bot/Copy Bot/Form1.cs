﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
            loadTimers();
        }

        private bool working = false;
        private bool checking = false;
        private bool igloo = false;
        private bool backyard = false;
        private bool regularRoom = true;
        private int targetID = -1;
        private int targetLocation = -1;
        private int shiftX = 0, shiftY = 0;

        private void loadTimers()
        {
            AddInBase.closePromptTimer.Elapsed += (obj, args) =>
            {
                closePrompt();
                AddInBase.closePromptTimer.Stop();
            };
            AddInBase.moveTimer.Elapsed += (obj, args) =>
            {
                sendMove(targetX(), targetY());
                AddInBase.moveTimer.Stop();
            };
            AddInBase.checkLocationTimer.Elapsed += (obj, args) =>
            {
                updateTargetLocation(() =>
                {
                    if (igloo)
                    {
                        checking = false;
                        toolStripStatusLabel1.Text = "Target player is in their igloo";
                        joinRoom(targetLocation);
                        start();
                        AddInBase.checkLocationTimer.Stop();
                    }
                    else if (backyard)
                    {
                        checking = false;
                        toolStripStatusLabel1.Text = "Target player is in their backyard";
                        joinRoom(targetLocation);
                        start();
                        AddInBase.checkLocationTimer.Stop();
                    }
                    else if (targetLocation > 0 && targetLocation < 900 && targetLocation != 898)
                    {
                        checking = false;
                        toolStripStatusLabel1.Text = "";
                        joinRoom(targetLocation);
                        start();
                        AddInBase.checkLocationTimer.Stop();
                    }
                    else if ((targetLocation >= 900 && targetLocation < 1000) || targetLocation == 898)
                    {
                        toolStripStatusLabel1.Text = "Target player is in a game";
                    }
                    else if (targetLocation > 1000 && targetLocation != 1100)
                    {
                        checking = false;
                        toolStripStatusLabel1.Text = "Target player is in an igloo";
                        joinRoom(targetLocation);
                        start();
                        AddInBase.checkLocationTimer.Stop();
                    }
                    else toolStripStatusLabel1.Text = "Target player is offline";
                });
            };
        }

        private void sendMove(int X, int Y)
        {
            if (X != -1)
            {
                Core.SetVarAsync("ENGINE.is_mouse_active", false, null);
                Core.ExecuteFunction("ENGINE.sendPlayerMove", (X + shiftX), (Y + shiftY));
            }
            else if (!checking) updateTargetLocation(() => { joinRoom(targetLocation); });
        }

        private void sendFrame(int ID)
        {
            Core.ExecuteFunction("INTERFACE.sendPlayerFrame", ID);
        }

        private void throwSnowBall(int X, int Y)
        {
            Core.ExecuteFunction("INTERFACE.sendThrowBall", X, Y);
        }

        private void sendMessage(string message)
        {
            Core.ExecuteFunction("INTERFACE.sendMessage", message);
        }

        private void sendSafeMessage(int ID)
        {
            Core.ExecuteFunction("INTERFACE.sendSafeMessage", ID);
        }

        private void sendJoke(int ID)
        {
            Core.ExecuteFunction("SHELL.sendJoke", ID);
            string joke = Core.ExecuteFunction("SHELL.getJokeById", ID).RawResult;
            Core.ExecuteFunction("INTERFACE.showBalloon", LocalPlayer.Id, joke);
        }

        private void sendTourGuideMessage(int ID)
        {
            Core.ExecuteFunction("SHELL.sendTourGuideMessage", ID);
            string roomName = Core.ExecuteFunction("SHELL.getRoomNameById", ID).RawResult;
            string message = Core.ExecuteFunction("SHELL.getTourGuideMessageByRoomName", roomName).RawResult;
            Core.ExecuteFunction("INTERFACE.showBalloon", LocalPlayer.Id, message);
        }

        private void sendStageScriptMessage(int ID)
        {
            Core.ExecuteFunction("INTERFACE.sendLineMessage", ID);
        }

        private void sendEmote(int ID)
        {
            Core.ExecuteFunction("INTERFACE.sendEmote", ID);
        }

        private void followPath(int pathID, int internalRoomID)
        {
            Core.SendPacketAsync("%xt%s%u#followpath%" + internalRoomID + "%" + pathID + "%", null);
        }

        private void teleport(int X, int Y)
        {
            Core.ExecuteFunction("ENGINE.sendPlayerTeleport", X + shiftX, Y + shiftY);
        }

        private void joinRoom(int roomID)
        {
            if (currentRoomID() != targetLocation) Core.SendPacket("%xt%s%j#jr%-1%" + roomID.ToString() + "%0%0%");
            else if (!regularRoom) joinLastRoom();
            regularRoom = (igloo || backyard || targetLocation > 1000) ? false : true;
        }

        private void joinLastRoom()
        {
            Core.ExecuteFunction("SHELL.sendJoinLastRoom");
        }

        private void sendCheckLocation()
        {
            Core.SendPacket("%xt%s%u#bf%-1%" + targetID.ToString() + "%");
        }

        private void closePrompt()
        {
            Core.ExecuteFunction("INTERFACE.closePrompt");
        }

        private void updateTargetLocation(Action action)
        {
            sendCheckLocation();
            PacketMonitor.On("bf", (packet, mode) => 
            {
                string[] Packet = packet.Split('%');
                if (Packet[6] == targetID.ToString() || Packet[6] == "-1")
                {
                    string roomID = Packet[4];
                    int.TryParse(roomID, out targetLocation);
                    igloo = Packet[5] == "igloo" ? true : false;
                    backyard = Packet[5] == "backyard" ? true : false;
                    if (AddInBase.closePromptTimer.Enabled) AddInBase.closePromptTimer.Stop();
                    AddInBase.closePromptTimer.Start();
                    if (InvokeRequired) BeginInvoke((MethodInvoker)delegate { action(); });
                    else action();
                    PacketMonitor.On("bf", null);
                }
            });
        }

        private int activePlayerCardID()
        {
            string response = Core.ExecuteFunction("INTERFACE.getActivePlayerId").RawResult;
            double id;
            if (double.TryParse(response, out id)) return (int)id;
            return 0;
        }

        private bool isIgloo()
        {
            return Core.ExecuteFunction("SHELL.getIsRoomIgloo").RawResult == "true";
        }

        private int currentRoomID()
        {
            int roomID = -1;
            if (isIgloo()) return roomID;
            string ID = Core.ExecuteFunction("SHELL.getCurrentRoomId").RawResult;
            int.TryParse(ID, out roomID);
            return roomID;
        }

        private int targetX()
        {
            string response = Core.GetVar("SHELL.playerModel._playerCollection._itemLookup." + targetID.ToString() + ".x").RawResult;
            double x;
            if (double.TryParse(response, out x)) return (int)x;
            return -1;
        }

        private int targetY()
        {
            string response = Core.GetVar("SHELL.playerModel._playerCollection._itemLookup." + targetID.ToString() + ".y").RawResult;
            double y;
            if (double.TryParse(response, out y)) return (int)y;
            return -1;
        }

        private void enableFollow()
        {
            PacketMonitor.On("jr", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                int currentRoomID = Convert.ToInt32(Packet[4]);
                bool isPlayerInRoom = false;
                for (int i = 5; i < Packet.Length - 1; i++)
                {
                    if (Packet[i].Split('|')[0] == targetID.ToString())
                    {
                        isPlayerInRoom = true;
                        break;
                    }
                }
                if (!isPlayerInRoom) updateTargetLocation(() =>
                {
                    if (igloo)
                    {
                        joinRoom(targetLocation);
                        toolStripStatusLabel1.Text = "Target player is in their igloo";
                    }
                    else if (backyard)
                    {
                        joinRoom(targetLocation);
                        toolStripStatusLabel1.Text = "Target player is in their backyard";
                    }
                    else if (targetLocation > 0 && targetLocation < 900 && targetLocation != 898)
                    {
                        joinRoom(targetLocation);
                        toolStripStatusLabel1.Text = "";
                    }
                    else if ((targetLocation >= 900 && targetLocation < 1000) || targetLocation == 898)
                    {
                        if (!checking)
                        {
                            toolStripStatusLabel1.Text = "Target player is in a game";
                            AddInBase.checkLocationTimer.Start();
                        }
                    }
                    else if (targetLocation > 1000 && targetLocation != 1100)
                    {
                        joinRoom(targetLocation);
                        toolStripStatusLabel1.Text = "Target player is in an igloo";
                    }
                    else if (!checking)
                    {
                        if (MessageBox.Show("Target player has logged off.\r\nDo you want to keep checking until the target is online?",
                        "Copy Bot", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            checking = true;
                            toolStripStatusLabel1.Text = "Target player is offline";
                            AddInBase.checkLocationTimer.Start();
                        }
                        else stop();
                    }
                });
            });
            PacketMonitor.On("rp", (packet, mode) => 
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    updateTargetLocation(() => 
                    {
                        if (igloo)
                        {
                            joinRoom(targetLocation);
                            toolStripStatusLabel1.Text = "Target player is in their igloo";
                        }
                        else if (backyard)
                        {
                            joinRoom(targetLocation);
                            toolStripStatusLabel1.Text = "Target player is in their backyard";
                        }
                        else if (targetLocation > 0 && targetLocation < 900 && targetLocation != 898)
                        {
                            joinRoom(targetLocation);
                            toolStripStatusLabel1.Text = "";
                        }
                        else if ((targetLocation >= 900 && targetLocation < 1000) || targetLocation == 898)
                        {
                            toolStripStatusLabel1.Text = "Target player is in a game";
                            checking = true;
                            AddInBase.checkLocationTimer.Start();
                        }
                        else if (targetLocation > 1000 && targetLocation != 1100)
                        {
                            joinRoom(targetLocation);
                            toolStripStatusLabel1.Text = "Target player is in an igloo";
                        }
                        else
                        {
                            if (MessageBox.Show("Target player has logged off.\r\nDo you want to keep checking until the target is online?",
                            "Copy Bot", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                            {
                                checking = true;
                                toolStripStatusLabel1.Text = "Target player is offline";
                                AddInBase.checkLocationTimer.Start();
                            }
                            else stop();
                        }
                    });
                }
            });
        }

        private void disableFollow()
        {
            PacketMonitor.On("jr", null);
            PacketMonitor.On("rp", null);
            PacketMonitor.On("bf", null);
        }

        private void enableCopyMove()
        {
            if (!checking)
            {
                if (checkBox6.Checked) enableMoveShift();
                else sendMove(targetX(), targetY());
            }
            PacketMonitor.On("sp", (packet, mode) => 
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int X, Y;
                    int.TryParse(Packet[5], out X);
                    int.TryParse(Packet[6], out Y);
                    sendMove(X, Y);
                }
            });
            PacketMonitor.On("ap", (packet, mode) => 
            {
                string ID = packet.Split('%')[4].Split('|')[0];
                if (ID == LocalPlayer.Id.ToString()) AddInBase.moveTimer.Start();
            });
            PacketMonitor.On("followpath", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int pathID, internalRoomID;
                    int.TryParse(Packet[5], out pathID);
                    int.TryParse(Packet[3], out internalRoomID);
                    followPath(pathID, internalRoomID);
                }
            });
            PacketMonitor.On("tp", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int x, y;
                    int.TryParse(Packet[5], out x);
                    int.TryParse(Packet[6], out y);
                    teleport(x, y);
                }
            });
        }

        private void disableCopyMove()
        {
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            PacketMonitor.On("sp", null);
            PacketMonitor.On("ap", null);
            PacketMonitor.On("followpath", null);
            PacketMonitor.On("tp", null);
        }

        private void enableCopyFrame()
        {
            PacketMonitor.On("sf", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int frameID;
                    int.TryParse(Packet[5], out frameID);
                    sendFrame(frameID);
                }
            });
            PacketMonitor.On("sa", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int frameID;
                    int.TryParse(Packet[5], out frameID);
                    sendFrame(frameID);
                }
            });
        }

        private void disableCopyFrame()
        {
            PacketMonitor.On("sf", null);
            PacketMonitor.On("sa", null);
        }

        private void enableCopySnowBall()
        {
            PacketMonitor.On("sb", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int X, Y;
                    int.TryParse(Packet[5], out X);
                    int.TryParse(Packet[6], out Y);
                    throwSnowBall(X, Y);
                }
            });
        }

        private void disableCopySnowBall()
        {
            PacketMonitor.On("sb", null);
        }

        private void enableCopyMessage()
        {
            PacketMonitor.On("sm", (packet, mode) => 
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString()) sendMessage(Packet[5]);
            });
            PacketMonitor.On("ss", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int messageID;
                    int.TryParse(Packet[5], out messageID);
                    sendSafeMessage(messageID);
                }
            });
            PacketMonitor.On("sj", (packet, mode) => 
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int jokeID;
                    int.TryParse(Packet[5], out jokeID);
                    sendJoke(jokeID);
                }
            });
            PacketMonitor.On("sg", (packet, mode) => 
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int ID;
                    int.TryParse(Packet[5], out ID);
                    sendTourGuideMessage(ID);
                }
            });
            PacketMonitor.On("sl", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int ID;
                    int.TryParse(Packet[5], out ID);
                    sendStageScriptMessage(ID);
                }
            });
        }

        private void disableCopyMessage()
        {
            PacketMonitor.On("sm", null);
            PacketMonitor.On("ss", null);
            PacketMonitor.On("sj", null);
            PacketMonitor.On("sg", null);
            PacketMonitor.On("sl", null);
        }

        private void enableCopyEmote()
        {
            PacketMonitor.On("se", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == targetID.ToString())
                {
                    int emoteID;
                    int.TryParse(Packet[5], out emoteID);
                    sendEmote(emoteID);
                }
            });
        }

        private void disableCopyEmote()
        {
            PacketMonitor.On("se", null);
        }

        private void start()
        {
            working = true;
            button1.Text = "Stop";
            button2.Enabled = false;
            textBox1.Enabled = false;
            enableFollow();
            if (checkBox1.Checked) enableCopyMove();
            if (checkBox2.Checked) enableCopyFrame();
            if (checkBox3.Checked) enableCopySnowBall();
            if (checkBox4.Checked) enableCopyMessage();
            if (checkBox5.Checked) enableCopyEmote();
        }

        private void stop()
        {
            AddInBase.checkLocationTimer.Stop();
            PacketMonitor.Off();
            working = false;
            checking = false;
            button1.Text = "Start";
            button2.Enabled = true;
            toolStripStatusLabel1.Text = "";
            textBox1.Enabled = true;
            Core.SetVarAsync("ENGINE.is_mouse_active", true, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!working)
            {
                if (int.TryParse(textBox1.Text, out targetID))
                {
                    if (targetID > 0)
                    {
                        if (targetID != LocalPlayer.Id)
                        {
                            updateTargetLocation(() => 
                            {
                                if (igloo)
                                {
                                    start();
                                    joinRoom(targetLocation);
                                    toolStripStatusLabel1.Text = "Target player is in their igloo";
                                }
                                else if (backyard)
                                {
                                    start();
                                    joinRoom(targetLocation);
                                    toolStripStatusLabel1.Text = "Target player is in their backyard";
                                }
                                if (targetLocation > 0 && targetLocation < 900 && targetLocation != 898)
                                {
                                    toolStripStatusLabel1.Text = "";
                                    if (targetLocation != currentRoomID()) joinRoom(targetLocation);
                                    start();
                                }
                                else if ((targetLocation >= 900 && targetLocation < 1000) || targetLocation == 898)
                                {
                                    checking = true;
                                    start();
                                    toolStripStatusLabel1.Text = "Target player is in a game";
                                    AddInBase.checkLocationTimer.Start();
                                }
                                else if (targetLocation > 1000 && targetLocation != 1100)
                                {
                                    start();
                                    joinRoom(targetLocation);
                                    toolStripStatusLabel1.Text = "Target player is in an igloo";
                                }
                                else
                                {
                                    if (MessageBox.Show("Target player is offline.\r\nDo you want to keep checking until the target is online?",
                                    "Copy Bot", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                                    {
                                        checking = true;
                                        start();
                                        toolStripStatusLabel1.Text = "Target player is offline";
                                        AddInBase.checkLocationTimer.Start();
                                    }
                                    else stop();
                                }
                            });
                        }
                        else MessageBox.Show("You cannot target yourself!");
                    }
                    else MessageBox.Show("Wrong ID!");
                }
                else
                {
                    Core.SendPacket("%xt%s%u#pbn%-1%" + textBox1.Text + "%");
                    PacketMonitor.On("pbn", (packet, mode) =>
                    {
                        int.TryParse(packet.Split('%')[5], out targetID);
                        if (targetID > 0)
                        {
                            if (targetID != LocalPlayer.Id)
                            {
                                updateTargetLocation(() =>
                                {
                                    if (igloo)
                                    {
                                        start();
                                        joinRoom(targetLocation);
                                        toolStripStatusLabel1.Text = "Target player is in their igloo";
                                    }
                                    else if (backyard)
                                    {
                                        start();
                                        joinRoom(targetLocation);
                                        toolStripStatusLabel1.Text = "Target player is in their backyard";
                                    }
                                    if (targetLocation > 0 && targetLocation < 900 && targetLocation != 898)
                                    {
                                        toolStripStatusLabel1.Text = "";
                                        if (targetLocation != currentRoomID()) joinRoom(targetLocation);
                                        start();
                                    }
                                    else if ((targetLocation >= 900 && targetLocation < 1000) || targetLocation == 898)
                                    {
                                        checking = true;
                                        start();
                                        toolStripStatusLabel1.Text = "Target player is in a game";
                                        AddInBase.checkLocationTimer.Start();
                                    }
                                    else if (targetLocation > 1000 && targetLocation != 1100)
                                    {
                                        start();
                                        joinRoom(targetLocation);
                                        toolStripStatusLabel1.Text = "Target player is in an igloo";
                                    }
                                    else
                                    {
                                        if (MessageBox.Show("Target player is offline.\r\nDo you want to keep checking until the target is online?",
                                        "Copy Bot", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                                        {
                                            checking = true;
                                            start();
                                            toolStripStatusLabel1.Text = "Target player is offline";
                                            AddInBase.checkLocationTimer.Start();
                                        }
                                        else stop();
                                    }
                                });
                            }
                            else MessageBox.Show("You cannot target yourself!");
                        }
                        else MessageBox.Show("Wrong name!");
                        PacketMonitor.Off("pbn");
                    });
                }
            }
            else stop();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            int playerID = activePlayerCardID();
            if (playerID > 0)
            {
                if (playerID != LocalPlayer.Id)
                {
                    targetID = playerID;
                    textBox1.Text = targetID.ToString();
                    updateTargetLocation(() => 
                    {
                        if (igloo)
                        {
                            start();
                            joinRoom(targetLocation);
                            toolStripStatusLabel1.Text = "Target player is in their igloo";
                        }
                        else if (backyard)
                        {
                            start();
                            joinRoom(targetLocation);
                            toolStripStatusLabel1.Text = "Target player is in their backyard";
                        }
                        if (targetLocation > 0 && targetLocation < 900 && targetLocation != 898)
                        {
                            toolStripStatusLabel1.Text = "";
                            if (targetLocation != currentRoomID()) joinRoom(targetLocation);
                            start();
                        }
                        else if ((targetLocation >= 900 && targetLocation < 1000) || targetLocation == 898)
                        {
                            checking = true;
                            start();
                            toolStripStatusLabel1.Text = "Target player is in a game";
                            AddInBase.checkLocationTimer.Start();
                        }
                        else if (targetLocation > 1000 && targetLocation != 1100)
                        {
                            start();
                            joinRoom(targetLocation);
                            toolStripStatusLabel1.Text = "Target player is in an igloo";
                        }
                        else
                        {
                            if (MessageBox.Show("Target player is offline.\r\nDo you want to keep checking until the target is online?",
                            "Copy Bot", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                            {
                                checking = true;
                                start();
                                toolStripStatusLabel1.Text = "Target player is offline";
                                AddInBase.checkLocationTimer.Start();
                            }
                            else stop();
                        }
                    });
                }
                else MessageBox.Show("You cannot target yourself!");
            }
            else MessageBox.Show("No player card open!");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                if (working) enableCopyMove();
            }
            else
            {
                disableCopyMove();
                checkBox6.Checked = false;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (working)
            {
                if (checkBox2.Checked) enableCopyFrame();
                else disableCopyFrame();
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (working)
            {
                if (checkBox3.Checked) enableCopySnowBall();
                else disableCopySnowBall();
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (working)
            {
                if (checkBox4.Checked) enableCopyMessage();
                else disableCopyMessage();
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (working)
            {
                if (checkBox5.Checked) enableCopyEmote();
                else disableCopyEmote();
            }
        }

        private void enableMoveShift()
        {
            if (int.TryParse(textBox2.Text, out shiftX) && int.TryParse(textBox3.Text, out shiftY))
            {
                checkBox1.Checked = true;
                if (working)
                {
                    textBox2.Enabled = false;
                    textBox3.Enabled = false;
                    if (!checking) sendMove(targetX(), targetY());
                }
            }
            else
            {
                textBox2.Text = "0";
                textBox3.Text = "0";
                checkBox6.Checked = false;
                MessageBox.Show("Wrong coordinates!");
            }
        }

        private void disableMoveShift()
        {
            shiftX = 0;
            shiftY = 0;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            if (working && !checking) sendMove(targetX(), targetY());
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked) enableMoveShift();
            else disableMoveShift();
        }

        private void textBoxCheck(TextBox textBox, KeyEventArgs e)
        {
            if ((e.Modifiers == Keys.Control) || (e.Modifiers == (Keys.Control | Keys.Shift)))
            {
                if (e.KeyCode != Keys.A && e.KeyCode != Keys.C && e.KeyCode != Keys.V && e.KeyCode != Keys.Z && e.KeyCode != Keys.Y && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else if (e.Modifiers == Keys.Shift)
            {
                if (e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else
            {
                if (e.KeyCode == Keys.OemMinus)
                {
                    if ((textBox.Text.Length != 0) && ((textBox.SelectionStart != 0) || ((textBox.Text[0] == '-') && (textBox.SelectionLength == 0)))) e.SuppressKeyPress = true;
                }
                else if ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9))
                {
                    if ((textBox.Text.Length >= 3) && (textBox.Text[0] != '-') && (textBox.SelectionLength == 0)) e.SuppressKeyPress = true;
                    if ((textBox.Text.Length != 0) && (textBox.Text[0] == '-') && (textBox.SelectionStart == 0) && (textBox.SelectionLength == 0)) e.SuppressKeyPress = true;
                }
                else if ((e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete) && (e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Right)) e.SuppressKeyPress = true;
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                button1_Click(sender, e);
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                textBox3.Focus();
            }
            else textBoxCheck(textBox2, e);
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                checkBox6.Checked = true;
            }
            else textBoxCheck(textBox3, e);
        }

    }
}
