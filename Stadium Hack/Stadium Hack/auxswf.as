﻿var INTERFACE = _global.getCurrentInterface();
var CROSSHAIR = INTERFACE.CROSSHAIR;
var SHELL = _global.getCurrentShell();
var ENGINE = SHELL.ENGINE;

function gameCoordinates()
{
	var room_mc = ENGINE.my_room_movieclips.room_mc;
	var xc = "";
	var yc = "";
	if(room_mc.roomfunctionality._GAME != null)
	{
		xc = _global.getCurrentEngine().my_room_movieclips.room_mc.roomfunctionality._GAME._x;
		yc = _global.getCurrentEngine().my_room_movieclips.room_mc.roomfunctionality._GAME._y;
	}
	if(room_mc.roomFunctionality._GAME != null)
	{
		xc = _global.getCurrentEngine().my_room_movieclips.room_mc.roomFunctionality._GAME._x;
		yc = _global.getCurrentEngine().my_room_movieclips.room_mc.roomFunctionality._GAME._y;
	}
	else if(room_mc.GAME != null)
	{
		xc = _global.getCurrentEngine().my_room_movieclips.room_mc.GAME._x;
		yc = _global.getCurrentEngine().my_room_movieclips.room_mc.GAME._y;
	}
	return xc + "%" + yc;
}

function activateCrosshair()
{
	CROSSHAIR._visible = true;
	CROSSHAIR.startDrag(true, 20, 20, 740, 440);
	CROSSHAIR._x = this._xmouse;
	CROSSHAIR._y = this._ymouse;
	CROSSHAIR.target_btn.onRelease = _global.mx.utils.Delegate.create(this, crosshairRelease);
	INTERFACE.snowballCrosshairShown.dispatch();
}

function crosshairRelease()
{
	var target_x = Math.round(CROSSHAIR._x);
	var target_y = Math.round(CROSSHAIR._y);
	stopDrag();
	CROSSHAIR._y = -100;
	CROSSHAIR._visible = false;
	Selection.setFocus(null);
	sendToAddIn(target_x + "%" + target_y);
}