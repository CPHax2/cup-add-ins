﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        private static bool working = false, random = true, isServerSide = false;
        private static string serverRoomId = "";
        private static int gameX = -1, gameY = -1;
        System.Timers.Timer timer = new System.Timers.Timer();
        Random randomNumber = new Random();

        public Form1()
        {
            InitializeComponent();
            timer.Elapsed += (obj, args) =>
            {
                move();
                timer.Interval = (int)numericUpDown1.Value;
            };
            AuxSWF.OnMessage = (msg) =>
            {
                string[] parts = msg.Split('%');
                int targetX, targetY;
                targetX = int.Parse(parts[0]);
                targetY = int.Parse(parts[1]);
                SetTarget(targetX, targetY);
            };
        }

        private static void getStadiumDetails()
        {
            serverRoomId = Core.ExecuteFunction("SHELL.getCurrentServerRoomId").RawResult;
            string res = Core.ExecuteFunction("AuxSWF.gameCoordinates").RawResult;
            string[] p = res.Split('%');
            if (int.TryParse(p[0], out gameX) && int.TryParse(p[1], out gameY))
            {
                return;
            }
            else
            {
                gameX = -1;
                gameY = -1;
            }
        }

        private void tryGetConfigs()
        {
            if (Core.ExecuteFunction("SHELL.getCurrentRoomId").RawResult != "802")
            {
                PacketMonitor.On("j#crl", (packet, mode) =>
                {
                    if (Core.ExecuteFunction("SHELL.getCurrentRoomId").RawResult != "802") return;
                    getStadiumDetails();
                    PacketMonitor.Off();
                }, PacketMode.Send);
                Core.SendPacket("%xt%s%j#jr%-1%802%0%0%");
            }
            else getStadiumDetails();
        }

        public void SetTarget(int x, int y)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { SetTarget(x, y); });
                return;
            }
            int xSpeed = 0, ySpeed = 0;
            if (gameX > 0 && gameY > 0)
            {
                xSpeed = (x - gameX) / 6;
                ySpeed = (y - gameY) / 6;
            }
            textBoxX.Text = xSpeed.ToString();
            textBoxY.Text = ySpeed.ToString();
            radioButton2.Checked = true;
        }

        private void move()
        {
            if (random)
            {
                int randomNumber1 = randomNumber.Next(-100, 100);
                int randomNumber2 = randomNumber.Next(-100, 100);
                sendMove(randomNumber1, randomNumber2);
            }
            else
            {
                int X, Y;
                if (int.TryParse(textBoxX.Text, out X) && int.TryParse(textBoxY.Text, out Y)) sendMove(X, Y);
                else
                {
                    stop();
                    MessageBox.Show("Invalid coordinates!");
                }
            }
        }

        private void sendMove(int x, int y)
        {
            if (isServerSide) Core.SendPacket("%xt%z%m%" + serverRoomId + "%0%0%0%" + x.ToString() + "%" + y.ToString() + "%");
            else
            {
                AuxServer.SendServer(x.ToString() + "%" + y.ToString());
                Core.ReceivePacket("%xt%zm%-1%0%0%0%" + x.ToString() + "%" + y.ToString() + "%");
                //Core.ExecuteFunction("AuxSWF.move", x, y);
            }
        }

        private void start()
        {
            working = true;
            button1.Text = "Stop";
            textBoxX.Enabled = false;
            textBoxY.Enabled = false;
            radioButton1.Enabled = false;
            radioButton2.Enabled = false;
            radioButton3.Enabled = false;
            radioButton4.Enabled = false;
            if (isServerSide && serverRoomId == "") tryGetConfigs();
            move();
            timer.Interval = (int)numericUpDown1.Value;
            timer.Start();
        }

        private void stop()
        {
            timer.Stop();
            working = false;
            button1.Text = "Start";
            textBoxX.Enabled = true;
            textBoxY.Enabled = true;
            radioButton1.Enabled = true;
            radioButton2.Enabled = true;
            radioButton3.Enabled = true;
            radioButton4.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!working) start();
            else stop();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            sendMove(0, 0);
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            tryGetConfigs();
            Core.ExecuteFunction("AuxSWF.activateCrosshair");
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) random = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked) random = false;
        }
        
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                isServerSide = false;
                checkBox1.Enabled = false;
                checkBox1.Checked = true;
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
            {
                if (MessageBox.Show("Server side mode is not 100% safe.\r\nDo you want to continue?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    isServerSide = true;
                    checkBox1.Enabled = true;
                }
                else radioButton3.Checked = true;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) AddInBase.enableAuxServer();
            else AddInBase.disableAuxServer();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            radioButton2.Checked = true;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            radioButton2.Checked = true;
        }
        
        private void textBoxX_Click(object sender, EventArgs e)
        {
            radioButton2.Checked = true;
        }

        private void textBoxY_Click(object sender, EventArgs e)
        {
            radioButton2.Checked = true;
        }

        private void textBoxCheck(TextBox textBox, KeyEventArgs e)
        {
            if ((e.Modifiers == Keys.Control) || (e.Modifiers == (Keys.Control | Keys.Shift)))
            {
                if (e.KeyCode != Keys.A && e.KeyCode != Keys.C && e.KeyCode != Keys.V && e.KeyCode != Keys.Z && e.KeyCode != Keys.Y && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else if (e.Modifiers == Keys.Shift)
            {
                if (e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else
            {
                if (e.KeyCode == Keys.OemMinus || e.KeyCode == Keys.Subtract)
                {
                    if ((textBox.Text.Length != 0) && ((textBox.SelectionStart != 0) || ((textBox.Text[0] == '-') && (textBox.SelectionLength == 0)))) e.SuppressKeyPress = true;
                }
                else if ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9))
                {
                    if ((textBox.Text.Length >= 3) && (textBox.Text[0] != '-') && (textBox.SelectionLength == 0)) e.SuppressKeyPress = true;
                    if ((textBox.Text.Length != 0) && (textBox.Text[0] == '-') && (textBox.SelectionStart == 0) && (textBox.SelectionLength == 0)) e.SuppressKeyPress = true;
                }
                else if ((e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete) && (e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Right)) e.SuppressKeyPress = true;
            }
        }

        private void textBoxX_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                textBoxY.Focus();
            }
            else textBoxCheck(textBoxX, e);
        }

        private void textBoxY_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radioButton2.Checked = true;
                e.SuppressKeyPress = true;
                button1_Click(sender, e);
                button1.Focus();
            }
            else textBoxCheck(textBoxY, e);
        }
        
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            stop();
        }
        
    }
}
