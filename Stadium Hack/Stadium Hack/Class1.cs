﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.AddIn;
using Cup.Extensibility;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    [AddIn("CuPAddin")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form1 form;

        public void OnBoot()
        {

        }
        
        public void OnStart()
        {
            if (started)
            {
                form.ShowDialog();
            }
            else
            {
                ApiPipeline.Hook();
                AuxSWF.Load();
                enableAuxServer();
                form = new Form1();
                started = true;
            }
        }

        public void OnExit()
        {
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
            disableAuxServer();
            PacketMonitor.Off();
            started = false;
        }

        public static void enableAuxServer()
        {
            AuxServer.OnPacketReceived = (type, packet) =>
            {
                Core.ReceivePacket("%xt%zm%-1%0%0%0%" + packet + "%");
            };
        }

        public static void disableAuxServer()
        {
            AuxServer.OnPacketReceived = null;
        }
    }
}
