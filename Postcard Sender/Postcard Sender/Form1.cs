﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ApiPipeline.Hook();
            toolStripStatusLabel1.Text = "";
        }

        bool sendById = false;

        public void reset()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            toolStripStatusLabel1.Text = "";
        }

        private void sendStatusReport()
        {
            PacketMonitor.On("ms", (packet1, mode1) =>
            {
                if (packet1.Split('%')[5] == "1") toolStripStatusLabel1.Text = "Postcard sent";
                else toolStripStatusLabel1.Text = "Error";
                PacketMonitor.Off();
            });
        }

        private void sendPostcard(int playerId, int postcardId)
        {
            if (playerId > 0)
            {
                if (postcardId > 0)
                {
                    if (playerId != LocalPlayer.Id) 
                    {
                        Core.ExecuteFunction("SHELL.sendMail", playerId, postcardId);
                        sendStatusReport();
                    }
                    else toolStripStatusLabel1.Text = "You can not send postcards to yourself!";
                }
                else toolStripStatusLabel1.Text = "Wrong postcard ID!";
            }
            else toolStripStatusLabel1.Text = "Wrong penguin " + (sendById ? "ID" : "name") + "!";
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "";
            int playerID = 0, postcardID = 0;

            int.TryParse(textBox2.Text, out postcardID);

            if (sendById == false)
            {
                Core.SendPacket("%xt%s%u#pbn%-1%" + textBox1.Text + "%");
                PacketMonitor.On("pbn", (packet, mode) =>
                {
                    int.TryParse(packet.Split('%')[5], out playerID);
                    PacketMonitor.Off();
                    sendPostcard(playerID, postcardID);
                });
            }
            else
            {
                int.TryParse(textBox1.Text, out playerID);
                sendPostcard(playerID, postcardID);
            }
        }
        
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                textBox1.Clear();
                sendById = false;
                label1.Text = "Player name:";
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                textBox1.Clear();
                sendById = true;
                label1.Text = "Player ID:";
            }
        }

        void textBoxCheck(KeyEventArgs e)
        {
            if ((e.Modifiers == Keys.Control) || (e.Modifiers == (Keys.Control | Keys.Shift)))
            {
                if (e.KeyCode != Keys.A && e.KeyCode != Keys.C && e.KeyCode != Keys.V && e.KeyCode != Keys.X && e.KeyCode != Keys.Z && e.KeyCode != Keys.Y && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else if (e.Modifiers == Keys.Shift)
            {
                if (e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else
            {
                if ((e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) && (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) && (e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete) && (e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Right)) e.SuppressKeyPress = true;
                if (e.KeyCode == Keys.Enter) e.SuppressKeyPress = true;
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (sendById) textBoxCheck(e);
            if (e.KeyCode == Keys.Enter) textBox2.Focus();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            textBoxCheck(e);
            if (e.KeyCode == Keys.Enter) button1_Click(sender, e);
        }

        
    }
}
