﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ApiPipeline.Hook();
        }

        bool snglmode;
        bool working;

        private void button1_Click(object sender, EventArgs e)
        {
            if (snglmode == true) Core.SendPacket("%xt%s%u#sf%-1%32%");
            else
            {
                if (working == false)
                {
                    working = true;
                    single.Enabled = false;
                    button1.Text = "Stop";
                    Core.SendPacket("%xt%s%u#sf%-1%32%");
                    var timer = new Timer() { Interval = 1000 };
                    timer.Tick += (obj, args) =>
                    {
                        Core.SendPacket("%xt%s%u#sf%-1%32%");
                        if (working == false)
                        {
                            timer.Stop();
                            timer.Dispose();
                        }
                    };
                    timer.Start();
                }
                else
                {
                    working = false;
                    single.Enabled = true;
                    button1.Text = "Server Jump";
                }
            }
        }

        private void single_CheckedChanged(object sender, EventArgs e)
        {
            if (single.Checked == true)
            {
                snglmode = true;
                button1.Enabled = true;
            }
        }

        private void continuous_CheckedChanged(object sender, EventArgs e)
        {
            if (continuous.Checked == true)
            {
                snglmode = false;
                button1.Enabled = true;
            }
        }

        private void Form1_HelpButtonClicked(Object sender, CancelEventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();
            e.Cancel = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            working = false;
            single.Enabled = true;
            button1.Text = "Server Jump";
        }
    }
}
