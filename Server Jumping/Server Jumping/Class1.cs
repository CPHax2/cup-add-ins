﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.AddIn;
using Cup.Extensibility;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    [AddIn("CuPAddIn")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form1 form;
        public static Form2 form2;
        public void OnBoot()
        {
            throw new NotImplementedException();
        }

        public void OnStart()
        {
            if (started)
            {
                form.ShowDialog();
            }
            else
            {
                started = true;
                form = new Form1();
            }
        }
        public void OnExit()
        {
            if (form2 != null)
            {
                form2.Close();
                form2.Dispose();
            }
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
            started = false;
        }
    }
}
