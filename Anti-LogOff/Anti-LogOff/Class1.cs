﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.AddIn;
using Cup.Extensibility;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    [AddIn("CuPAddIn")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form1 form;
        public static System.Timers.Timer timer = new System.Timers.Timer { Interval = 60000 };

        public void OnBoot()
        {
            
        }

        public void OnStart()
        {
            if (started)
            {
                form.ShowDialog();
            }
            else
            {
                started = true;
                ApiPipeline.Hook();
                timer.Elapsed += (obj, args) =>
                {
                    Core.SendPacket("%xt%s%u#h%-1%");
                    Core.ExecuteFunction("SHELL.stopPlayerIdleCheck");
                };
                form = new Form1();
            }
        }

        public void OnExit()
        {
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
            started = false;
        }

    }
}
