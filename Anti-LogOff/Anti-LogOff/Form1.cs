﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
            if (UserSettings.Get() != "")
            {
                checkBox1.Checked = true;
                radioButton1.Checked = true;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                Core.SendPacket("%xt%s%u#h%-1%");
                Core.ExecuteFunction("SHELL.stopPlayerIdleCheck");
                AddInBase.timer.Start();
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                Core.SendPacket("%xt%s%u#h%-1%");
                Core.ExecuteFunction("SHELL.startPlayerIdleCheck");
                AddInBase.timer.Stop();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) UserSettings.Set("1");
            else UserSettings.Set("");
        }

    }
}
