﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void GetSettings(string settings)
        {
            checkBox1.Checked = settings != "0";
        }

        public void UpdateStatus(string text)
        {
            if (InvokeRequired) BeginInvoke((MethodInvoker)delegate { UpdateStatus(text); });
            else toolStripStatusLabel1.Text = text;
        }

        public void Disable(bool shadowDisabled)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { Disable(shadowDisabled); });
                return;
            }
            radioButton1.Enabled = false;
            radioButton2.Enabled = false;
            radioButton3.Enabled = false;
            radioButton4.Enabled = false;
            radioButton5.Enabled = false;
            trackBar1.Enabled = false;
            if (shadowDisabled) trackBar2.Enabled = false;
        }

        public void Enable(bool shadowOnly = false)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { Enable(shadowOnly); });
                return;
            }
            trackBar2.Enabled = true;
            if (shadowOnly) return;
            radioButton1.Enabled = true;
            radioButton2.Enabled = true;
            radioButton3.Enabled = true;
            radioButton4.Enabled = true;
            radioButton5.Enabled = true;
            trackBar1.Enabled = true;
        }

        public void Reset()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { Reset(); });
                return;
            }
            radioButton1.CheckedChanged -= radioButton1_CheckedChanged;
            radioButton2.CheckedChanged -= radioButton2_CheckedChanged;
            radioButton3.CheckedChanged -= radioButton3_CheckedChanged;
            radioButton4.CheckedChanged -= radioButton4_CheckedChanged;
            radioButton5.CheckedChanged -= radioButton5_CheckedChanged;
            trackBar1.ValueChanged -= trackBar1_ValueChanged;
            trackBar2.ValueChanged -= trackBar2_ValueChanged;
            radioButton1.Checked = true;
            trackBar1.Value = 1;
            trackBar2.Value = 0;
            radioButton1.CheckedChanged += radioButton1_CheckedChanged;
            radioButton2.CheckedChanged += radioButton2_CheckedChanged;
            radioButton3.CheckedChanged += radioButton3_CheckedChanged;
            radioButton4.CheckedChanged += radioButton4_CheckedChanged;
            radioButton5.CheckedChanged += radioButton5_CheckedChanged;
            trackBar1.ValueChanged += trackBar1_ValueChanged;
            trackBar2.ValueChanged += trackBar2_ValueChanged;
        }

        private string selectedState()
        {
            if (radioButton1.Checked) return "default";
            else if (radioButton2.Checked) return "fire";
            else if (radioButton3.Checked) return "water";
            else if (radioButton4.Checked) return "snow";
            else return "tie";
        }

        private string modeByValue(int value)
        {
            switch (value)
            {
                default:
                case 0:
                case 1:
                    return "A";
                case 2:
                    return "B";
                case 3:
                    return "C";
            }
        }

        private void updateColorState()
        {
            string mode;
            string state = selectedState();
            if (state == "default") mode = "A";
            else mode = modeByValue(trackBar1.Value);
            WeatherEffects.ApplyColorState(mode, state);
        }

        private void updateShadowState()
        {
            int level = trackBar2.Value;
            string mode = modeByValue(level), state;
            if (level == 0) state = "empty";
            else state = selectedState();
            WeatherEffects.ApplyShadowState(mode, state);
        }
        
        public void ApplyCurrentSettings()
        {
            updateColorState();
            updateShadowState();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) WeatherEffects.StartControl();
            else WeatherEffects.StopControl();
            UserSettings.Set(checkBox1.Checked ? "1" : "0");
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) updateColorState();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked) updateColorState();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked) updateColorState();
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked) updateColorState();
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked) updateColorState();
        }
        
        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            if (selectedState() != "default") updateColorState();
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            updateShadowState();
        }

    }
}
