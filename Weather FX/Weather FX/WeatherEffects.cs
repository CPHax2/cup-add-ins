﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    class WeatherEffects
    {
        private static bool available = false, control = true;

        private static void initWem()
        {
            if (Core.ExecuteFunction("AuxSWF.initWem").RawResult == "true")
            {
                available = true;
                if (control)
                {
                    Core.ExecuteFunction("AuxSWF.stopCheck");
                    AddInBase.form.Enable();
                }
                AddInBase.form.UpdateStatus("");
            }
            else onWemUnavailable();
        }

        private static void onWemUnavailable()
        {
            available = false;
            AddInBase.form.UpdateStatus("Weather effects not available in this room");
            AddInBase.form.Disable(false);
        }

        private static void reset()
        {
            Core.ExecuteFunction("AuxSWF.resetShadow");
            AddInBase.form.Reset();
        }

        public static void StartControl()
        {
            Core.ExecuteFunction("AuxSWF.stopCheck");
            AddInBase.form.ApplyCurrentSettings();
            AddInBase.form.Enable(available ? false : true);
            control = true;
        }

        public static void StopControl()
        {
            AddInBase.form.Disable(true);
            ApplyShadowState("A", "empty");
            ApplyColorState("A", "default");
            Core.ExecuteFunction("AuxSWF.startCheck");
            control = false;
        }

        public static void Init()
        {
            AuxSWF.Load();
            AddInBase.form.GetSettings(UserSettings.Get());
            initWem();
            PacketMonitor.On("j#crl", (packet, mode) =>
            {
                initWem();
                reset();
            }, PacketMode.Send);
            PacketMonitor.On("jg", (packet, mode) => 
            {
                onWemUnavailable();
                reset();
            });
            PacketMonitor.On("jnbhg", (packet, mode) =>
            {
                onWemUnavailable();
                reset();
            });
        }

        public static void ApplyColorState(string mode, string state)
        {
            Core.ExecuteFunction("AuxSWF.applyColorState", mode, state);
        }

        public static void ApplyShadowState(string mode, string state)
        {
            Core.ExecuteFunction("AuxSWF.applyShadowState", mode, state);
        }
    }
}
