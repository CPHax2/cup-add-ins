﻿var ENGINE = _global.getCurrentEngine();
var room_mc = ENGINE.my_room_movieclips.room_mc;
var wem;
var LAST_COLOR_STATE = "default";
var LAST_COLOR_MODE = "A";
var LAST_SHADOW_STATE = "empty";
var LAST_SHADOW_MODE = "A";

function initWem()
{
	getCurrentWem();
	LAST_COLOR_STATE = "default";
	LAST_COLOR_MODE = "A";
	LAST_SHADOW_STATE = "empty";
	LAST_SHADOW_MODE = "A";
	return wem != null;
}

function stopCheck()
{
	wem.stopWeatherEffectCheck();
}

function startCheck()
{
	wem.startWeatherEffectCheck();
}

function getCurrentWem()
{
	if(room_mc.wem != null) 
	{
		wem = room_mc.wem;
	}
	else if(room_mc.roomFunctionality.wem != null)
	{
		wem = room_mc.roomFunctionality.wem;
	}
	else if(room_mc.room.wem != null) 
	{
		wem = room_mc.room.wem;
	}
	else wem = null;
}

function removeClouds()
{
	var cloudsEffectColor = wem.getBackogrundMC().weatherEffects_mc.cloudsEffect.cloudsEffectColor;
	if(cloudsEffectColor._currentFrame != 1) cloudsEffectColor.gotoAndPlay("leave");
}

function removeTieCloudsEffect()
{
	var tieEffect = wem.getBackogrundMC().weatherEffects_mc.tieEffect;
	if(tieEffect._currentFrame != 1) wem._colorTieState.removeTieCloudsEffect();
}

function stopWeatherEffects()
{
	wem._colorFireState.stopFireEffect();
	wem._colorWaterState.stopWaterEffect();
	wem._colorWaterState.waterEffect.gotoAndStop("off");
	wem._colorWaterState.waterEffect.gotoAndStop("leave");
	wem._colorSnowState.stopSnowEffect();
	wem._colorSnowState.snowEffect.gotoAndStop("off");
	wem._colorSnowState.snowEffect.gotoAndStop("leave");
	wem._colorTieState.stopTieEffect();
}

function applyColorState(colorMode, effectType)
{
	if(wem == null) return;
	colorMode = colorMode.toUpperCase();
	effectType = effectType.toLowerCase();
	var currentState = currentColorState();
	var currentMode = currentColorMode();
	var color_state = getColorState(effectType);
	updateColorMode(colorMode);
	stopWeatherEffects();
	if(colorMode == "A" && (LAST_COLOR_MODE != "A" || currentMode != "A") && LAST_SHADOW_MODE == "A") removeClouds();
	if((LAST_COLOR_STATE == "tie" || currentState == wem._colorTieState) && effectType != "tie") removeTieCloudsEffect();
	var _type = "";
	if(effectType.length > 0) _type = effectType.substr(0, 1).toUpperCase() + effectType.substr(1, effectType.length - 1);
	color_state.applyTinting(colorMode, _type);
	if(LAST_SHADOW_STATE != "empty") applyShadowState(LAST_SHADOW_MODE, effectType);
	if(effectType == "tie")
	{
		if(LAST_COLOR_STATE != "tie") color_state.showTieCloudsEffect();
		else if(colorMode == "A") color_state.stopLightningEffect();
		if(colorMode == "B" || colorMode == "C")
		{
			color_state.showLightningEffect();
			color_state.showClouds();
		}
		if(colorMode == "C") color_state.playTieEffect();
		else color_state.stopTieEffect();
	}
	else if(effectType != "default")
	{
		if(colorMode == "B" || colorMode == "C") color_state.showClouds();
		if(colorMode == "C")
		{
			if(effectType == "fire") color_state.playFireEffect();
			else if(effectType == "water") color_state.playWaterEffect();
			else if(effectType == "snow") color_state.playSnowEffect();
		}
	}
	LAST_COLOR_STATE = effectType;
	LAST_COLOR_MODE = colorMode;
}

function currentColorMode()
{
	return wem.__get__currentColorMode().getMode();
}

function currentColorState()
{
	return wem._currentColorState;
}

function getColorState(effectType)
{
	switch(effectType)
	{
		case "fire":
			return wem._colorFireState;
		case "water":
			return wem._colorWaterState;
		case "snow":
			return wem._colorSnowState;
		case "tie":
			return wem._colorTieState;
		case "default":
			return wem._colorDefaultState;
	}
}

function updateColorMode(colorMode)
{
	var _mode;
	switch(colorMode)
	{
		case "A":
			_mode = wem._colorModeA;
			break;
		case "B":
			_mode = wem._colorModeB;
			break;
		case "C":
			_mode = wem._colorModeC;
			break;
		default:
			_mode = wem._colorModeA;
	}
	wem._currentColorMode = _mode;
}

function getShadowState(colorState)
{
	switch(colorState)
	{
		case "default":
			return wem._shadowDefaultState;
		case "fire":
			return wem._shadowFireState;
		case "water":
			return wem._shadowWaterState;
		case "snow":
			return wem._shadowSnowState;
		case "tie":
			return wem._shadowTieState;
		case "empty":
		default:
			return wem._shadowEmptyState;
	}
}

function currentShadowMode()
{
	return wem.__get__currentShadowMode().getMode();
}

function updateShadowMode(shadowMode)
{
	var _mode;
	switch(shadowMode)
	{
		case "A":
			_mode = wem._shadowModeA;
			break;
		case "B":
			_mode = wem._shadowModeB;
			break;
		case "C":
			_mode = wem._shadowModeC;
			break;
	}
	wem._currentShadowMode = _mode;
}

function applyShadowTint(shadowMode, effectType)
{
	var SHELL = _global.getCurrentShell();
	var _objectType = _global.com.clubpenguin.world.rooms.weathereffects.WeatherEffectsManager.OBJECT_TYPE_SHELL;
	var _tintObjectShell;
	if(effectType != "Empty") 
	{
		_tintObjectShell = _global.com.clubpenguin.world.rooms.weathereffects.WeatherEffectsSettings["shadow" + _objectType + "Mode" + shadowMode];
	}
	else
	{
		_tintObjectShell = _global.com.clubpenguin.world.rooms.weathereffects.WeatherEffectsSettings["shadow" + effectType + _objectType + "ModeA"];
		LAST_SHADOW_STATE = "empty";
	}
	LAST_SHADOW_STATE = effectType.toLowerCase();
	_global.com.greensock.TweenLite.to(SHELL, 0, {colorTransform: _tintObjectShell, onComplete: null});
}

function resetShadow()
{
	applyShadowTint("A", "Empty");
}

function applyShadowState(shadowMode, colorState)
{
	shadowMode = shadowMode.toUpperCase();
	colorState = colorState.toLowerCase();
	var _color = colorState.substr(0, 1).toUpperCase() + colorState.substr(1, colorState.length - 1);
	if(wem == null)
	{
		applyShadowTint(shadowMode, _color);
		return;
	}
	var shadow_state = getShadowState(colorState);
	var shadow_mode = currentShadowMode();
	if(shadowMode == "A" && (shadow_mode != "A" || LAST_SHADOW_MODE != "A") && LAST_COLOR_MODE == "A") removeClouds();
	if(shadowMode != "C" && (shadow_mode == "C" || LAST_SHADOW_MODE == "C")) shadow_state.stopShadowEffect();
	shadow_state.applyTinting(shadowMode, _color);
	applyShadowTint(shadowMode, _color);
	if(colorState != "empty")
	{
		if(shadowMode == "B" || shadowMode == "C") shadow_state.showClouds();
		if(shadowMode == "C") shadow_state.playShadowEffect();
	}
	updateShadowMode(shadowMode);
	LAST_SHADOW_STATE = colorState;
	LAST_SHADOW_MODE = shadowMode;
}