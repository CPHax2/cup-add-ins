﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn;
using System.Windows.Forms;
using Cup.Extensibility;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    [AddIn("CuPAddIn")]
    public class AddInBase : ICuPAddIn
    {
        private static bool started = false;
        public static Form1 form;

        public void OnBoot()
        {
            
        }
        
        public void OnStart()
        {
            if (started)
            {
                form.ShowDialog();
            }
            else
            {
                ApiPipeline.Hook();
                form = new Form1();
                WeatherEffects.Init();
                started = true;
            }
        }

        public void OnExit()
        {
            WeatherEffects.StopControl();
            PacketMonitor.Off();
            AuxSWF.Unload();
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
            started = false;
        }

    }
}
