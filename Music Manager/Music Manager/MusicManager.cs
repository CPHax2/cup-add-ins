﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    static class MusicManager
    {
        private static bool tracksDisabled = false, musicStarted = true, musicLocked = false;

        public static bool MusicLocked
        {
            get { return musicLocked; }
            set
            {
                if (value) Core.ExecuteFunction("AuxSWF.lockMusic");
                else Core.ExecuteFunction("AuxSWF.unlockMusic");
                musicLocked = value;
            }
        }

        public static string CurrentMusicID()
        {
            return Core.ExecuteFunction("AuxSWF.currentMusicId").RawResult;
        }

        private static bool isIgloo()
        {
            return Core.ExecuteFunction("SHELL.getIsRoomIgloo").RawResult == "true";
        }

        private static bool isMyIgloo()
        {
            return Core.ExecuteFunction("SHELL.isMyIgloo").RawResult == "true";
        }

        private static int currentRoomID()
        {
            string response = Core.ExecuteFunction("SHELL.getCurrentRoomId").RawResult;
            int roomId = -1;
            int.TryParse(response, out roomId);
            return roomId;
        }

        public static void PlayMusic(int musicId)
        {
            bool lockMusic = MusicLocked;
            if (MusicLocked) MusicLocked = false;
            if (isIgloo()) Core.ExecuteFunction("SHELL.sendStopIglooStudioMusic");
            Core.ExecuteFunction("MUSIC.unMuteSound");
            Core.ExecuteFunction("MUSIC.unMuteMusic");
            Core.ExecuteFunction("SHELL.startMusicById", musicId);
            if (lockMusic) MusicLocked = true;
        }

        public static void SetMusic(int musicId)
        {
            Core.ExecuteFunction("SHELL.setIglooMusicId", musicId);
            if (isMyIgloo())
            {
                string internalId = Core.ExecuteFunction("SHELL.getCurrentServerRoomId").RawResult;
                Core.SendPacket("%xt%s%g#im%" + internalId + "%0%");
                PlayMusic(musicId);
            }
        }

        private static void disableTracks()
        {
            Core.ExecuteFunction("SHELL.sendHideMusicPlayerWidget");
            tracksDisabled = true;
        }

        private static void startMusic()
        {
            Core.ExecuteFunction("SHELL.startRoomMusic");
            musicStarted = true;
        }

        public static void EnableTracks()
        {
            if (currentRoomID() == 120) Core.ExecuteFunction("SHELL.sendShowMusicPlayerWidget");
            PacketMonitor.Off("musictrack#broadcastingmusictracks");
            PacketMonitor.Off("canliketrack");
            PacketMonitor.Off("jr");
            tracksDisabled = false;
        }

        public static void KeepTracksDisabled()
        {
            if (currentRoomID() == 120)
            {
                disableTracks();
                startMusic();
            }
            PacketMonitor.On("musictrack#broadcastingmusictracks", (packet, mode) =>
            {
                if (!tracksDisabled) disableTracks();
            }, PacketMode.Send);
            PacketMonitor.On("canliketrack", (packet, mode) =>
            {
                if (!musicStarted) startMusic();
            });
            PacketMonitor.On("jr", (packet, mode) =>
            {
                tracksDisabled = false;
                musicStarted = false;
            });
        }
    }
}
