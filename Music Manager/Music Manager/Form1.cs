﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {

        public Form1()
        {
            InitializeComponent();
        }

        public void GetSettings()
        {
            if (UserSettings.Get() != "")
            {
                checkBox1.Checked = true;
                onRadioButton.Checked = true;
            }
            checkBox1.CheckedChanged += checkBox1_CheckedChanged;
        }

        public void UpdateMusicID(string id)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { UpdateMusicID(id); });
                return;
            }
            label2.Text = "Currently playing music: " + (id.Length > 0 && id[0] == '-' ? "custom (" + id + ")" : id);
        }
        
        private void playButton_Click(object sender, EventArgs e)
        {
            int id;
            if (int.TryParse(textBox1.Text, out id)) MusicManager.PlayMusic(id);
            else MessageBox.Show("Wrong ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void setButton_Click(object sender, EventArgs e)
        {
            if (LocalPlayer.IsMember)
            {
                int musicId;
                if ((int.TryParse(textBox1.Text, out musicId)) && (musicId >= 0))
                {
                    MusicManager.SetMusic(musicId);
                }
                else MessageBox.Show("Wrong ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else MessageBox.Show("Non-members can not set igloo music!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        
        private void onRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (onRadioButton.Checked) MusicManager.KeepTracksDisabled();
            else MusicManager.EnableTracks();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            UserSettings.Set(checkBox1.Checked ? "1" : "");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateMusicID(MusicManager.CurrentMusicID());
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Modifiers == Keys.Control) || (e.Modifiers == (Keys.Control | Keys.Shift)))
            {
                if (e.KeyCode != Keys.A && e.KeyCode != Keys.C && e.KeyCode != Keys.V && e.KeyCode != Keys.X && e.KeyCode != Keys.Z && e.KeyCode != Keys.Y && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else if (e.Modifiers == Keys.Shift)
            {
                if (e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    playButton_Click(sender, e);
                }
                else if (e.KeyCode == Keys.OemMinus || e.KeyCode == Keys.Subtract)
                {
                    if ((textBox1.Text.Length != 0) && ((textBox1.SelectionStart != 0) || ((textBox1.Text[0] == '-') && (textBox1.SelectionLength == 0)))) e.SuppressKeyPress = true;
                }
                else if ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9))
                {
                    if ((textBox1.Text.Length != 0) && (textBox1.Text[0] == '-') && (textBox1.SelectionStart == 0) && (textBox1.SelectionLength == 0)) e.SuppressKeyPress = true;
                }
                else if ((e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete) && (e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Right)) e.SuppressKeyPress = true;
            }
        }

        private void lockButton_Click(object sender, EventArgs e)
        {
            if (MusicManager.MusicLocked)
            {
                lockButton.Image = CuPAddIn.Properties.Resources.padlock_open;
                toolTip1.SetToolTip(lockButton, "Lock music");
            }
            else
            {
                lockButton.Image = CuPAddIn.Properties.Resources.padlock_closed;
                toolTip1.SetToolTip(lockButton, "Unlock music");
            }
            MusicManager.MusicLocked = !MusicManager.MusicLocked;
        }
    }
}
