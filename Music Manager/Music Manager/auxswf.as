﻿var SHELL;

function bindOnUnlocked()
{
	bindFunctionAfter('SHELL.MUSIC.stopMusic', function(result)
	{
		sendToAddIn('0');
	});
	bindFunctionAfter('SHELL.sendUnmuteMusicPlayer', function(result)
	{
		sendToAddIn(currentMusicId());  
	});
}

function init()
{
	SHELL = _global.getCurrentShell();

	bindFunctionAfter('SHELL.startMusicById', function(result, musicID)
    {
        sendToAddIn(currentMusicId());
    });
	bindFunctionAfter('SHELL.MUSIC.stopSound', function(result)
	{
		sendToAddIn('0');
	});
	
	bindOnUnlocked();
}

function currentMusicId()
{
	var url = SHELL.MUSIC.currentURL;
	if (url == '' || SHELL.MUSIC.isMuted || SHELL.MUSIC.isSoundMuted) return '0';
	var id = url.substr(59, url.length - 63);
	return id;
}

function bindBeforePlayMusicURL(lockedMusicUrl)
{
	bindFunctionBefore('SHELL.MUSIC.playMusicURL', function(url)
	{
    	if (url != lockedMusicUrl) SHELL.MUSIC.currentURL = url;
	});
}

function unbindBeforePlayMusicURL()
{
	bindFunctionBefore('SHELL.MUSIC.playMusicURL', null);
}

function lockMusic()
{
	var lockedMusicUrl = SHELL.MUSIC.currentURL;
	var lockedMusicId = currentMusicId();
	
	bindBeforePlayMusicURL(lockedMusicUrl);
	bindFunctionAfter('SHELL.MUSIC.playMusicURL', function(result, url)
	{
    	SHELL.MUSIC.currentURL = lockedMusicUrl;
	});
	bindFunctionBefore('SHELL.MUSIC.stopMusic', function()
	{
		if (!SHELL.MUSIC.isMuted) SHELL.MUSIC.currentURL = '';
	});
	
	bindFunctionAfter('SHELL.MUSIC.stopMusic', function(result)
	{
		if (!SHELL.MUSIC.isMuted) SHELL.MUSIC.currentURL = lockedMusicUrl;
		else sendToAddIn('0');
	});
	bindFunctionAfter('SHELL.sendUnmuteMusicPlayer', function(result)
	{
		unbindBeforePlayMusicURL();
		SHELL.MUSIC.currentURL = '';
		SHELL.startMusicById(lockedMusicId);
		bindBeforePlayMusicURL(lockedMusicUrl);
	});
}

function unlockMusic()
{
	if (SHELL.MUSIC.isMuted) SHELL.MUSIC.currentURL = '';
	
	unbindBeforePlayMusicURL();
	bindFunctionAfter('SHELL.MUSIC.playMusicURL', null);
	bindFunctionBefore('SHELL.MUSIC.stopMusic', null);
	
	bindOnUnlocked();
}