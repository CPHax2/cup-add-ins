﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.AddIn;
using Cup.Extensibility;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    [AddIn("CuPAddIn")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form1 form;

        public void OnBoot()
        {

        }

        public void OnStart()
        {
            if (started)
            {
                form.ShowDialog();
            }
            else
            {
                started = true;
                form = new Form1();
                AuxSWF.Load();
                CuPChat.Init();
            }
        }

        public void OnExit()
        {
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
            AuxServer.OnPacketReceived = null;
            PacketMonitor.Off();
            started = false;
        }

    }
}
