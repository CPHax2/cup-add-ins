﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace CuPAddIn
{
    class Cryptography
    {
        private static string _publicKey;
        public static string PublicKey { get { return _publicKey; } }

        public static void Init()
        {
            RSA.Init();
        }

        public static string EncryptMessage(string message, string key)
        {
            string iv = AES.GenerateIV();
            return AES.Encrypt(message, key, iv) + "|" + iv;
        }

        public static string DecryptMessage(string message, string key, string IV)
        {
            return AES.Decrypt(message, key, IV);
        }

        public static string GenerateSecretKey()
        {
            return AES.GenerateKey();
        }

        public static string EncryptSecretKey(string secretKey, string publicKey)
        {
            return RSA.Encrypt(secretKey, publicKey);
        }

        public static string DecryptSecretKey(string key)
        {
            return RSA.Decrypt(key);
        }

        private static class RSA
        {
            private static RSACryptoServiceProvider rsacsp = new RSACryptoServiceProvider(1024);
            private static RSAParameters pubKey = rsacsp.ExportParameters(false);
            private static RSAParameters privKey = rsacsp.ExportParameters(true);
            private static string[] info = new string[2];

            public static void Init()
            {
                var sw = new StringWriter();
                var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
                xs.Serialize(sw, pubKey);
                string publicKey = sw.ToString();
                int startIndex = publicKey.IndexOf("<Modulus>") + 9;
                int endIndex = publicKey.IndexOf("</Modulus>");
                _publicKey = publicKey.Substring(startIndex, endIndex - startIndex);
                info[0] = publicKey.Substring(0, startIndex);
                info[1] = publicKey.Substring(endIndex, publicKey.Length - endIndex);
            }

            private static RSAParameters publicKeyFromString(string key)
            {
                key = info[0] + key + info[1];
                var sr = new StringReader(key);
                var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
                return (RSAParameters)xs.Deserialize(sr);
            }

            public static string Encrypt(string text, string key)
            {
                string encryptedMessage;
                using (var csp = new RSACryptoServiceProvider())
                {
                    RSAParameters publicKey = publicKeyFromString(key);
                    csp.ImportParameters(publicKey);
                    byte[] messageBytes = System.Text.Encoding.UTF8.GetBytes(text);
                    byte[] encryptedBytes = csp.Encrypt(messageBytes, false);
                    encryptedMessage = Convert.ToBase64String(encryptedBytes);
                }
                return encryptedMessage;
            }

            public static string Decrypt(string text)
            {
                byte[] encryptedBytes = Convert.FromBase64String(text);
                byte[] decryptedBytes = rsacsp.Decrypt(encryptedBytes, false);
                string decryptedMessage = System.Text.Encoding.UTF8.GetString(decryptedBytes);
                return decryptedMessage;
            }
        }

        private static class AES
        {
            public static string GenerateKey()
            {
                byte[] key;
                using (var csp = new AesCryptoServiceProvider())
                {
                    csp.KeySize = 256;
                    csp.GenerateKey();
                    key = csp.Key;
                }
                return Convert.ToBase64String(key);
            }

            public static string GenerateIV()
            {
                byte[] iv;
                using (var csp = new AesCryptoServiceProvider())
                {
                    csp.GenerateIV();
                    iv = csp.IV;
                }
                return Convert.ToBase64String(iv);
            }

            public static string Encrypt(string text, string key, string IV)
            {
                byte[] keyBytes = Convert.FromBase64String(key);
                byte[] ivBytes = Convert.FromBase64String(IV);
                string encryptedMessage;
                using (AesCryptoServiceProvider csp = new AesCryptoServiceProvider())
                {
                    csp.KeySize = 256;
                    csp.Key = keyBytes;
                    csp.IV = ivBytes;
                    ICryptoTransform encryptor = csp.CreateEncryptor();
                    byte[] encryptedBytes;
                    using (MemoryStream msEncrypt = new MemoryStream())
                    {
                        using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(text);
                            }
                            encryptedBytes = msEncrypt.ToArray();
                        }
                    }
                    encryptedMessage = Convert.ToBase64String(encryptedBytes);
                }
                return encryptedMessage;
            }

            public static string Decrypt(string text, string key, string IV)
            {
                byte[] messageBytes = Convert.FromBase64String(text);
                byte[] keyBytes = Convert.FromBase64String(key);
                byte[] ivBytes = Convert.FromBase64String(IV);
                string decryptedMessage;
                using (AesCryptoServiceProvider csp = new AesCryptoServiceProvider())
                {
                    csp.KeySize = 256;
                    csp.Key = keyBytes;
                    csp.IV = ivBytes;
                    ICryptoTransform decryptor = csp.CreateDecryptor();
                    using (MemoryStream msDecrypt = new MemoryStream(messageBytes))
                    {
                        using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                            {
                                decryptedMessage = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                return decryptedMessage;
            }
        }

    }

}