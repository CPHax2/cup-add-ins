﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class UsersForm : CuPForm
    {
        public UsersForm()
        {
            InitializeComponent();
        }

        private void AddUsersToList(string[] IDs, string[] names)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { AddUsersToList(IDs, names); });
                return;
            }
            for (int i = 0; i < IDs.Length; i++)
            {
                listView1.Items.Add(new ListViewItem { Text = names[i], Tag = IDs[i] });
            }
        }

        private void UsersForm_Load(object sender, EventArgs e)
        {

            AuxServer.GetUsersUsingThisAddIn((idList) =>
            {
                var timeoutTimer = new System.Timers.Timer { Interval = 3000 };
                string[] names = new string[idList.Length];
                timeoutTimer.Elapsed += (s1, e1) =>
                {
                    timeoutTimer.Stop();
                    PacketMonitor.Off("pbi");
                    for (int i = 0; i < names.Length; i++)
                    {
                        if (names[i] == null) names[i] = "[" + idList[i] + "]";
                    }
                    AddUsersToList(idList, names);
                    timeoutTimer.Dispose();
                };
                int nameCount = 0;
                for (int i = 0; i < idList.Length; i++)
                {
                    string name = CuPChat.TryGetNameById(idList[i]);
                    if (name == null) continue;
                    names[i] = name;
                    nameCount++;
                }
                if (nameCount == idList.Length)
                {
                    AddUsersToList(idList, names);
                    return;
                }
                PacketMonitor.On("pbi", (packet, mode) =>
                {
                    timeoutTimer.Stop();
                    timeoutTimer.Start();
                    string[] Packet = packet.Split('%');
                    string id = Packet[5];
                    for (int i = 0; i < idList.Length; i++)
                    {
                        if (idList[i] == id)
                        {
                            names[i] = Packet[6];
                            CuPChat.SaveUserName(id, names[i]);
                            if (++nameCount == idList.Length)
                            {
                                timeoutTimer.Dispose();
                                PacketMonitor.Off("pbi");
                                AddUsersToList(idList, names);
                            }
                        }
                    }
                });
                for (int i = 0; i < names.Length; i++)
                {
                    if (names[i] == null) Core.SendPacketAsync("%xt%s%u#pbi%-1%" + idList[i] + "%", null);
                }
                timeoutTimer.Start();
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string playerID = (string)listView1.SelectedItems[0].Tag;
            AddInBase.form.StartPrivateChat(playerID);
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Enabled = (listView1.SelectedItems.Count == 1 && (string)listView1.SelectedItems[0].Tag != LocalPlayer.Id.ToString());
        }
    }
}
