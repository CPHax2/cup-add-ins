﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public static class CuPChat
    {
        public static string LocalPlayerName;
        public static bool ShowMessages { get; set; }
        public static bool ShowPMsInGame { get; set; }
        private static bool colorMessages;
        public static bool ColorMessages
        {
            get
            {
                return colorMessages;
            }
            set
            {
                colorMessages = value;
                Core.SetVar("AuxSWF.colorMessages", value);
            }
        }
        public static NotificationLevel NotificationSettings { get; set; }
        private static Dictionary<string, User> Users = new Dictionary<string, User>(),
        Staff = new Dictionary<string, User>
        {
            // ...
        };
        
        public static void Init()
        {
            ApiPipeline.Hook();
            Cryptography.Init();
            User u;
            if (Staff.TryGetValue(LocalPlayer.Id.ToString(), out u))
            {
                AddInBase.form.EnableBroadcast();
                LocalPlayerName = u.Name;
            }
            if (LocalPlayerName == null) LocalPlayerName = Core.ExecuteFunction("SHELL.getMyPlayerNickname").RawResult;
            if (LocalPlayerName == "undefined") LocalPlayerName = "[" + LocalPlayer.Id + "]";
            loadSettings();
            initAuxServer();
            sendResetKey();
            Server.OnChanged = () =>
            {
                sendResetKey();
                resetKeys();
            };
        }
        
        public enum NotificationLevel : int
        {
            None = 0,
            BroadcastPM = 1,
            CuP = 2,
            All = 3
        };

        public enum MessageType : int
        {
            CPMessage = 1,
            CuPChatMessage = 2,
            PMSend = 3,
            PMReceive = 4,
            Broadcast = 5
        };

        public class User
        {
            public string Name { get; set; }
            public string EncryptionKey { get; set; }
            public string DecryptionKey { get; set; }
        }

        public static User UserById(string ID)
        {
            User u;
            Users.TryGetValue(ID, out u);
            return u;
        }
        
        public static string NicknameById(string ID)
        {
            return Core.ExecuteFunction("SHELL.getNicknameById", ID).RawResult;
        }
        
        public static string UserNameById(string ID)
        {
            User u;
            if (Users.TryGetValue(ID, out u)) return u.Name;
            return NicknameById(ID);
        }

        public static string StaffNameById(string ID)
        {
            User u;
            if (Staff.TryGetValue(ID, out u)) return u.Name;
            return null;
        }

        public static string TryGetNameById(string ID)
        {
            if (ID == LocalPlayer.Id.ToString()) return LocalPlayerName;
            string name = StaffNameById(ID);
            if (name != null) return name;
            User u;
            if (Users.TryGetValue(ID, out u)) return u.Name;
            name = NicknameById(ID);
            if (name != "undefined" && !string.IsNullOrEmpty(name))
            {
                Users.Add(ID, new User { Name = name });
            }
            return null;
        }

        public static void SaveUserName(string ID, string name)
        {
            User u;
            if (Users.TryGetValue(ID, out u)) u.Name = name;
            else Users.Add(ID, new User { Name = name });
        }

        public static void SendStandardMessage(string message)
        {
            AuxServer.SendRoom("m|" + LocalPlayer.Id.ToString() + '|' + message);
            if (AddInBase.form.IsLoggingAuxServer) AddInBase.form.AddLogMessage(LocalPlayer.Id.ToString(), message, MessageType.CuPChatMessage);
            Core.ExecuteFunction("AuxSWF.onMessageSent", message, (int)MessageType.CuPChatMessage);
        }

        public static void SendPrivateMessage(string recipientId, string message)
        {
            string key = GetEncryptionKey(recipientId);
            if (key == null)
            {
                KeyRequest(recipientId);
                System.Windows.Forms.MessageBox.Show("Couldn't send message to the selected user!");
                return;
            }
            string encryptedMessage = Cryptography.EncryptMessage(message, key);
            AuxServer.SendServer("pm|" + recipientId + '|' + LocalPlayer.Id + '|' + LocalPlayerName + '|' + encryptedMessage);
            AddInBase.form.AddLogMessage(recipientId, message, MessageType.PMSend);
            if (ShowPMsInGame)
            {
                Core.ExecuteFunction("AuxSWF.onMessageSent", message, (int)MessageType.PMSend);
            }
        }

        public static void SendBroadcastMessage(string message)
        {
            AuxServer.SendServer("b|" + LocalPlayer.Id + '|' + message);
            AddInBase.form.AddLogMessage(LocalPlayer.Id.ToString(), message, MessageType.Broadcast);
            Core.ExecuteFunction("AuxSWF.onMessageSent", message, (int)MessageType.Broadcast);
        }

        private static void loadSettings()
        {
            string settings = UserSettings.Get();
            ShowMessages = true;
            ShowPMsInGame = true;
            colorMessages = true;
            NotificationSettings = NotificationLevel.CuP;
            if (settings.Length == 5)
            {
                ShowMessages = settings[0] != '0';
                if (settings[1] == '0') NotificationSettings = NotificationLevel.None;
                if (settings[4] != '0')
                {
                    AddInBase.form.IsLoggingAuxServer = settings[2] != '0';
                    AddInBase.form.IsLoggingNormal = settings[3] != '0';
                }
                SaveSettings();
            }
            else if (settings.Length > 0)
            {
                byte[] b = Convert.FromBase64String(settings + (settings.Length == 1 ? "A==" : ""));
                var bits = new System.Collections.BitArray(b);
                ShowMessages = bits[7];
                int nl = 0;
                if (bits[6]) nl += 2;
                if (bits[5]) nl += 1;
                NotificationSettings = (NotificationLevel)nl;
                AddInBase.form.IsLoggingAuxServer = bits[4];
                AddInBase.form.IsLoggingNormal = bits[3];
                ShowPMsInGame = bits[2];
                if (settings.Length > 1) ColorMessages = bits[1];
            }
            LogRegularMessages(AddInBase.form.IsLoggingNormal);
            AddInBase.form.OnSettingsLoaded();
        }
        
        public static void SaveSettings()
        {
            var bits = new System.Collections.BitArray(8);
            bits.Set(7, ShowMessages);
            bits.Set(6, (int)NotificationSettings / 2 == 1);
            bits.Set(5, (int)NotificationSettings % 2 == 1);
            bits.Set(4, AddInBase.form.IsLoggingAuxServer);
            bits.Set(3, AddInBase.form.IsLoggingNormal);
            bits.Set(2, ShowPMsInGame);
            bits.Set(1, ColorMessages);
            byte[] b = new byte[1];
            bits.CopyTo(b, 0);
            string settings = Convert.ToBase64String(b);
            UserSettings.Set(settings);
        }
        
        public static void KeyRequest(string playerID)
        {
            AuxServer.SendServer("kr|" + playerID + "|" + LocalPlayer.Id);
        }

        public static void SendKey(string playerID)
        {
            AuxServer.SendServer("k1|" + playerID + "|" + LocalPlayer.Id + "|" + LocalPlayerName + "|" + Cryptography.PublicKey);
        }

        private static void sendResetKey()
        {
            AuxServer.SendServer("i|" + LocalPlayer.Id);
        }
        
        public static bool HasEncryptionKey(string ID)
        {
            User u = UserById(ID);
            return u != null && u.EncryptionKey != null;
        }

        public static string GetEncryptionKey(string ID)
        {
            User u = UserById(ID);
            if (u == null || string.IsNullOrEmpty(u.EncryptionKey)) return null;
            return u.EncryptionKey;
        }

        public static string GetDecryptionKey(string ID)
        {
            User u = UserById(ID);
            if (u == null || string.IsNullOrEmpty(u.DecryptionKey)) return null;
            return u.DecryptionKey;
        }

        private static void resetKeys()
        {
            foreach (User u in Users.Values)
            {
                u.EncryptionKey = null;
                u.DecryptionKey = null;
            }
        }

        private static void initAuxServer()
        {
            AuxServer.OnPacketReceived = (type, packet) =>
            {
                string[] Packet = packet.Split('|');
                switch (Packet[0])
                {
                    case "m":
                        ReceiveMessage(Packet, MessageType.CuPChatMessage);
                        break;
                    case "pm":
                        if (Packet[1] == LocalPlayer.Id.ToString()) ReceiveMessage(Packet, MessageType.PMReceive);
                        break;
                    case "b":
                        ReceiveMessage(Packet, MessageType.Broadcast);
                        break;
                    case "i":
                        User u1 = UserById(Packet[1]);
                        if (u1 != null)
                        {
                            u1.EncryptionKey = null;
                            u1.DecryptionKey = null;
                        }
                        if (AddInBase.form.PMPlayerId == Packet[1])
                        {
                            AddInBase.form.MessageMode = 3;
                            AddInBase.form.RequestedPlayerKey = Packet[1];
                            KeyRequest(Packet[1]);
                        }
                        break;
                    case "kr":
                        if (Packet[1] == LocalPlayer.Id.ToString()) SendKey(Packet[2]);
                        break;
                    case "k1":
                        if (Packet[1] != LocalPlayer.Id.ToString()) return;
                        User u = UserById(Packet[2]);
                        string encryptionKey;
                        if (u != null)
                        {
                            if (u.EncryptionKey == null) u.EncryptionKey = Cryptography.GenerateSecretKey();
                            encryptionKey = u.EncryptionKey;
                        }
                        else
                        {
                            encryptionKey = Cryptography.GenerateSecretKey();
                            Users.Add(Packet[2], new User { Name = Packet[3], EncryptionKey = encryptionKey });
                        }
                        AuxServer.SendServer("k2|" + Packet[2] + "|" + LocalPlayer.Id + "|" + LocalPlayerName + "|" + Cryptography.EncryptSecretKey(encryptionKey, Packet[4]));
                        if (AddInBase.form.RequestedPlayerKey == Packet[2]) AddInBase.form.SetPMPlayerID(Packet[2]);
                        break;
                    case "k2":
                        if (Packet[1] != LocalPlayer.Id.ToString()) return;
                        string decryptionKey = Cryptography.DecryptSecretKey(Packet[4]);
                        User u2 = UserById(Packet[2]);
                        if (u2 != null) u2.DecryptionKey = decryptionKey;
                        else Users.Add(Packet[2], new User { Name = Packet[3], DecryptionKey = decryptionKey });
                        break;
                    default:
                        break;
                }
            };
        }
        
        public static void LogRegularMessages(bool on)
        {
            if (!on)
            {
                PacketMonitor.Off("sm");
                PacketMonitor.Off("ss");
                PacketMonitor.Off("sj");
                PacketMonitor.Off("sg");
                PacketMonitor.Off("sl");
                return;
            }
            PacketMonitor.On("sm", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                AddInBase.form.AddLogMessage(Packet[4], Packet[5], MessageType.CPMessage);
                if (Packet[4] != LocalPlayer.Id.ToString()) notify(MessageType.CPMessage);
            });
            PacketMonitor.On("ss", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                string message = Core.ExecuteFunction("SHELL.getSafeMessageById", Convert.ToInt32(Packet[5])).RawResult;
                AddInBase.form.AddLogMessage(Packet[4], message, MessageType.CPMessage);
                if (Packet[4] != LocalPlayer.Id.ToString()) notify(MessageType.CPMessage);
            });
            PacketMonitor.On("sj", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                string joke = Core.ExecuteFunction("SHELL.getJokeById", Convert.ToInt32(Packet[5])).RawResult;
                AddInBase.form.AddLogMessage(Packet[4], joke, MessageType.CPMessage);
                if (Packet[4] != LocalPlayer.Id.ToString()) notify(MessageType.CPMessage);
            });
            PacketMonitor.On("sg", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                string roomName = Core.ExecuteFunction("SHELL.getRoomNameById", Convert.ToInt32(Packet[5])).RawResult;
                string message = Core.ExecuteFunction("SHELL.getTourGuideMessageByRoomName", roomName).RawResult;
                AddInBase.form.AddLogMessage(Packet[4], message, MessageType.CPMessage);
                if (Packet[4] != LocalPlayer.Id.ToString()) notify(MessageType.CPMessage);
            });
            PacketMonitor.On("sl", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                string message = Core.ExecuteFunction("SHELL.getLineMessageById", Convert.ToInt32(Packet[5])).RawResult;
                AddInBase.form.AddLogMessage(Packet[4], message, MessageType.CPMessage);
                if (Packet[4] != LocalPlayer.Id.ToString()) notify(MessageType.CPMessage);
            });
        }

        private static void ReceiveMessage(string[] parts, MessageType type)
        {
            if (!ShowMessages) return;
            string playerId = "", message = "";
            switch (type)
            {
                case MessageType.CuPChatMessage:
                    playerId = parts[1];
                    message = parts[2];
                    for (int i = 3; i < parts.Length; i++) message += '|' + parts[i];
                    if (AddInBase.form.IsLoggingAuxServer) AddInBase.form.AddLogMessage(playerId, message, MessageType.CuPChatMessage);
                    Core.ExecuteFunction("AuxSWF.onMessageReceived", message, playerId, (int)type);
                    notify(MessageType.CuPChatMessage);
                    break;
                case MessageType.PMReceive:
                    playerId = parts[2];
                    string key = GetDecryptionKey(playerId);
                    if (key == null)
                    {
                        SendKey(playerId);
                        return;
                    }
                    message = Cryptography.DecryptMessage(parts[4], key, parts[5]);
                    AddInBase.form.AddLogMessage(playerId, message, MessageType.PMReceive);
                    if (ShowPMsInGame)
                    {
                        Core.ExecuteFunction("AuxSWF.onMessageReceived", message, playerId, (int)type);
                    }
                    
                    notify(MessageType.PMReceive);
                    break;
                case MessageType.Broadcast:
                    playerId = parts[1];
                    message = parts[2];
                    for (int i = 3; i < parts.Length; i++) message += '|' + parts[i];
                    AddInBase.form.AddLogMessage(playerId, message, MessageType.Broadcast);
                    Core.ExecuteFunction("AuxSWF.onMessageReceived", message, playerId, (int)type);
                    notify(MessageType.Broadcast);
                    break;
            }
            
        }

        private static bool shouldNotify(MessageType type)
        {
            NotificationLevel nl = NotificationSettings;
            if (nl == NotificationLevel.None) return false;

            if (((AddInBase.form.KeepOnTop && !Utils.MainFormMinimized) || Utils.ApplicationIsActivated()) && AddInBase.form.IsLogOpen) return false;
            if ((Utils.GetActiveProcessFileName() == "Cloud Penguin") && (type == MessageType.CPMessage || type == MessageType.CuPChatMessage)) return false;

            if (nl == NotificationLevel.BroadcastPM) return (int)type >= 4;
            if (nl == NotificationLevel.CuP) return (int)type >= 2;
            return true;
        }

        private static void notification()
        {
            bool minimized = Utils.MainFormMinimized;
            if (!AddInBase.form.IsLogOpen && !minimized) AddInBase.form.SwitchLogView();
            if (!AddInBase.form.Visible)
            {
                AddInBase.form.ShowDialog();
                AddInBase.form.Activate();
            }
            else if (minimized)
            {
                AddInBase.form.OpenLogAfterRestore = true;
                Utils.FlashWindowEx(AddInBase.form);
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (!Utils.ApplicationIsActivated())
            {
                AddInBase.form.Activate();
                System.Media.SystemSounds.Asterisk.Play();
            }
        }

        private static void notify(MessageType type)
        {
            if (!shouldNotify(type)) return;
            if (AddInBase.form.InvokeRequired)
            {
                AddInBase.form.Invoke((System.Windows.Forms.MethodInvoker)delegate { notification(); });
            }
            else notification();
        }
    }

    static class Utils
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        private const UInt32 FLASHW_ALL = 3;
        private const UInt32 FLASHW_TIMERNOFG = 12;

        [StructLayout(LayoutKind.Sequential)]
        private struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public UInt32 dwFlags;
            public UInt32 uCount;
            public UInt32 dwTimeout;
        }

        public static bool FlashWindowEx(System.Windows.Forms.Form form)
        {
            IntPtr hWnd = form.Handle;
            FLASHWINFO fInfo = new FLASHWINFO();

            fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(fInfo));
            fInfo.hwnd = hWnd;
            fInfo.dwFlags = FLASHW_ALL | FLASHW_TIMERNOFG;
            fInfo.uCount = UInt32.MaxValue;
            fInfo.dwTimeout = 0;

            return FlashWindowEx(ref fInfo);
        }

        /// <summary>Returns true if the current application has focus, false otherwise</summary>
        public static bool ApplicationIsActivated()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero) return false;

            var procId = System.Diagnostics.Process.GetCurrentProcess().Id;
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return activeProcId == procId;
        }

        public static string GetActiveProcessFileName()
        {
            IntPtr hwnd = GetForegroundWindow();
            int pid;
            GetWindowThreadProcessId(hwnd, out pid);
            System.Diagnostics.Process p = System.Diagnostics.Process.GetProcessById((int)pid);
            return p.ProcessName;
        }

        public static bool MainFormMinimized
        {
            get { return AddInBase.form.WindowState == System.Windows.Forms.FormWindowState.Minimized; }
        }
    }
}
