﻿var SHELL = _global.getCurrentShell();
var INTERFACE = _global.getCurrentInterface();
var colorMessages = true;
var localPlayerId = SHELL.getMyPlayerId();
 
function onMessageSent(msg, type)
{
	INTERFACE.handlePhraseAutocompleteText(msg);
	if (colorMessages) colorBalloon(localPlayerId, type);
}

function onMessageReceived(msg, id, type)
{
	INTERFACE.handlePhraseRecieved(id, null, null, msg);
	if (colorMessages) colorBalloon(id, type);
}
 
function colorBalloon(id, type){
	var rgb;
	switch(type)
	{
		case 2:
		{
			rgb = 0x0080FF;
			break;
		}
		case 3:
		{
			rgb = 0x11B2B2;
			break;
		}
		case 4:
		{
			rgb = 0x9643BA;
			break;
		}
		case 5:
		{
			rgb = 0xFF0000;
			break;
		}
		default:
		{
			return;
		}
	}
    var balloon = INTERFACE.BALLOONS['p' + id];
    var color = new Color(balloon.balloon_mc);
    color.setRGB(rgb);
    var color = new Color(balloon.pointer_mc);
	color.setRGB(rgb);
}
