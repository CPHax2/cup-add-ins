﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
        }

        private static Size standardSize = new Size(317, 125), minLogModeSize = new Size(584, 362), logModeSize = new Size(584, 362);
        public string PMPlayerId = null;
        public string RequestedPlayerKey;
        public bool IsLogOpen = false, OpenLogAfterRestore = false, KeepOnTop = false;

        private int _messageMode = 0;
        public int MessageMode
        {
            get
            {
                return _messageMode;
            }
            set
            {
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)delegate { MessageMode = value; });
                    return;
                }
                sendButton.Enabled = value != 3;
                switch (value)
                {
                    case 0:
                        label1.Text = "Message:";
                        label1.ForeColor = Color.Black;
                        linkLabel1.Visible = false;
                        break;
                    case 1:
                        label1.Text = "Private Message to " + CuPChat.UserNameById(PMPlayerId) + ":";
                        label1.ForeColor = Color.Teal;
                        linkLabel1.Visible = true;
                        break;
                    case 2:
                        label1.Text = "Broadcast Message:";
                        label1.ForeColor = Color.Red;
                        linkLabel1.Visible = true;
                        break;
                    case 3:
                        label1.Text = "Initializing private chat...";
                        label1.ForeColor = Color.Gray;
                        linkLabel1.Visible = true;
                        break;
                }
                if (value != 3 && value != 1)
                {
                    PMPlayerId = null;
                    RequestedPlayerKey = null;
                }
                _messageMode = value;
            }
        }

        public bool IsLoggingAuxServer
        {
            get { return checkBox1.Checked; }
            set { checkBox1.Checked = value; }
        }
        public bool IsLoggingNormal
        {
            get { return checkBox2.Checked; }
            set { checkBox2.Checked = value; }
        }

        public void OnSettingsLoaded()
        {
            checkBox1.CheckedChanged += checkBox1_CheckedChanged;
            checkBox2.CheckedChanged += checkBox2_CheckedChanged;
        }

        public void EnableBroadcast()
        {
            broadcastButton.Visible = true;
        }        

        public void SetPMPlayerID(string ID)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)delegate { SetPMPlayerID(ID); });
                return;
            }
            RequestedPlayerKey = null;
            PMPlayerId = ID;
            MessageMode = ID == null ? 0 : 1;
            textBox1.Focus();
        }
        
        public void StartPrivateChat(string playerId)
        {
            if (playerId != null && !CuPChat.HasEncryptionKey(playerId))
            {
                RequestedPlayerKey = playerId;
                MessageMode = 3;
                CuPChat.KeyRequest(playerId);
            }
            else SetPMPlayerID(playerId);
        }

        private void sendMessage()
        {
            string message = textBox1.Text;
            textBox1.Clear();
            if (message == "") return;
            if (MessageMode == 2) CuPChat.SendBroadcastMessage(message);
            else if (MessageMode == 1)
            {
                if (PMPlayerId != null) CuPChat.SendPrivateMessage(PMPlayerId, message);
            }
            else if (MessageMode == 0) CuPChat.SendStandardMessage(message);
        }

        public void AddLogMessage(string playerId, string message, CuPChat.MessageType type)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { AddLogMessage(playerId, message, type); });
                return;
            }
            chatLog.BeginUpdate();
            ListViewItem newMessage = new ListViewItem { SubItems = { message } };
            newMessage.Tag = playerId;
            newMessage.SubItems[1].Tag = type;
            switch (type)
            {
                case CuPChat.MessageType.CPMessage:
                    newMessage.Text = CuPChat.StaffNameById(playerId);
                    if (string.IsNullOrEmpty(newMessage.Text)) newMessage.Text = CuPChat.NicknameById(playerId);
                    newMessage.ForeColor = Color.Green;
                    break;
                case CuPChat.MessageType.CuPChatMessage:
                    newMessage.Text = CuPChat.StaffNameById(playerId);
                    if (string.IsNullOrEmpty(newMessage.Text)) newMessage.Text = CuPChat.NicknameById(playerId);
                    newMessage.ForeColor = Color.Blue;
                    break;
                case CuPChat.MessageType.PMSend:
                    newMessage.Text = "[PM to] " + CuPChat.UserNameById(playerId);
                    newMessage.ForeColor = Color.Teal;
                    break;
                case CuPChat.MessageType.PMReceive:
                    newMessage.Text = "[PM] " + CuPChat.UserNameById(playerId);
                    newMessage.ForeColor = Color.DarkViolet;
                    break;
                case CuPChat.MessageType.Broadcast:
                    newMessage.Text = CuPChat.StaffNameById(playerId);
                    newMessage.ForeColor = Color.Red;
                    newMessage.Font = new Font(newMessage.Font, FontStyle.Bold);
                    break;
            }
            int count = chatLog.Items.Count;
            bool scrollToBottom = this.Visible && count > 0 && (WindowState == FormWindowState.Minimized || chatLog.ClientRectangle.Contains(chatLog.Items[count - 1].Bounds));
            count++;
            chatLog.Items.Add(newMessage);
            if (scrollToBottom) chatLog.Items[count - 1].EnsureVisible();
            resizeColumn();
            chatLog.EndUpdate();
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            sendMessage();
        }

        public void SwitchLogView()
        {
            if (!IsLogOpen)
            {
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                MaximizeBox = true;
                MinimumSize = minLogModeSize;
                this.Size = logModeSize;
                checkBox2.Visible = true;
                checkBox1.Visible = true;
                clearButton.Visible = true;
                chatLog.Height = label1.Location.Y - chatLog.Location.Y - label1.Margin.Top - chatLog.Margin.Bottom;
                chatLog.Visible = true;
                SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
                toolTip1.SetToolTip(logButton, "Hide Chat Log");
                if (chatLog.Items.Count > 0) chatLog.Items[chatLog.Items.Count - 1].EnsureVisible();
                IsLogOpen = true;
            }
            else
            {
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
                chatLog.Visible = false;
                checkBox2.Visible = false;
                checkBox1.Visible = false;
                clearButton.Visible = false;
                if (WindowState == FormWindowState.Maximized) WindowState = FormWindowState.Normal;
                MaximizeBox = false;
                MinimumSize = new Size(standardSize.Width, standardSize.Height);
                this.Size = new Size(standardSize.Width, standardSize.Height);
                SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
                toolTip1.SetToolTip(logButton, "Show Chat Log");
                IsLogOpen = false;
            }
            textBox1.Focus();
        }
        
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A)
            {
                textBox1.SelectAll();
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.OemPipe && e.Modifiers == Keys.Shift) e.SuppressKeyPress = true;
            else if (e.KeyCode == Keys.Enter && e.Modifiers != Keys.Shift)
            {
                e.SuppressKeyPress = true;
                if (sendButton.Enabled)
                {
                    sendButton_Click(sender, e);
                    textBox1.Clear();
                }
            }
        }
        
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CuPChat.SaveSettings();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            CuPChat.LogRegularMessages(checkBox2.Checked);
            CuPChat.SaveSettings();
        }

        private void Form1_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                contextMenuStrip1.Dispose();
                this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
                this.contextMenuStrip1.SuspendLayout();
                this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[]{
                this.sendPMMenuItem,
                this.copyMenuItem,
                this.selectAllMenuItem,
                this.removeMenuItem });
                this.contextMenuStrip1.Name = "contextMenuStrip1";
                this.contextMenuStrip1.ShowImageMargin = false;
                this.contextMenuStrip1.ShowItemToolTips = false;
                this.contextMenuStrip1.Size = new System.Drawing.Size(140, 92);
                this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
                this.contextMenuStrip1.ResumeLayout(false);
                if (chatLog.Items.Count > 0) chatLog.Items[chatLog.Items.Count - 1].EnsureVisible();
            }
        }

        private bool verticalScrollbarVisible
        {
            get
            {
                int headerHeight = 28;
                int itemHeight = 17;
                return chatLog.Items.Count * itemHeight + headerHeight > chatLog.Height;
            }
        }

        private void resizeColumn()
        {
            int width = chatLog.Width - chatLog.Columns[0].Width - 4;
            if (verticalScrollbarVisible) width -= SystemInformation.VerticalScrollBarWidth;
            chatLog.Columns[1].Width = width;
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            if (chatLog.Visible)
            {
                logModeSize = this.Size;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            resizeColumn();
            if (WindowState != FormWindowState.Minimized && OpenLogAfterRestore)
            {
                if (!IsLogOpen) SwitchLogView();
                OpenLogAfterRestore = false;
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            chatLog.Items.Clear();
            resizeColumn();
        }

        private void copyMessages()
        {
            string s = "";
            foreach (ListViewItem item in chatLog.SelectedItems)
            {
                string name = item.Text;
                string message = item.SubItems[1].Text;
                s += (s.Length > 0 ? "\r\n" : "") + name + ": " + message;
            }
            if (s == "") return;
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() => { Clipboard.SetText(s); }));
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
        }

        private void selectAllMessages()
        {
            foreach (ListViewItem message in chatLog.Items)
            {
                message.Selected = true;
            }
        }

        private void removeSelectedMessages()
        {
            foreach (ListViewItem message in chatLog.SelectedItems)
            {
                message.Remove();
            }
            resizeColumn();
        }

        private void copyMenuItem_Click(object sender, EventArgs e)
        {
            copyMessages();
        }

        private void selectAllMenuItem_Click(object sender, EventArgs e)
        {
            selectAllMessages();
        }

        private void removeMenuItem_Click(object sender, EventArgs e)
        {
            removeSelectedMessages();
        }
        
        private void sendPMMenuItem_Click(object sender, EventArgs e)
        {
            string playerId = (string)chatLog.FocusedItem.Tag;
            if (playerId != null && !CuPChat.HasEncryptionKey(playerId))
            {
                RequestedPlayerKey = playerId;
                MessageMode = 3;
                CuPChat.KeyRequest(playerId);
            }
            else SetPMPlayerID(playerId);
        }

        private void chatLog_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void chatLog_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) removeSelectedMessages();
            else if (e.KeyCode == Keys.Apps) contextMenuStrip1.Show(Cursor.Position);
            else if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.A) selectAllMessages();
                else if (e.KeyCode == Keys.C) copyMessages();
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            selectAllMenuItem.Enabled = chatLog.Items.Count > 0 ? true : false;
            sendPMMenuItem.Enabled = false;
            int count = chatLog.SelectedItems.Count;
            if (count > 0)
            {
                copyMenuItem.Enabled = true;
                removeMenuItem.Enabled = true;
                if (count == 1 && (CuPChat.MessageType)chatLog.SelectedItems[0].SubItems[1].Tag != CuPChat.MessageType.CPMessage && (string)chatLog.SelectedItems[0].Tag != LocalPlayer.Id.ToString())
                {
                    sendPMMenuItem.Enabled = true;
                } 
            }
            else
            {
                copyMenuItem.Enabled = false;
                removeMenuItem.Enabled = false;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SetPMPlayerID(null);
        }

        private void usersButton_Click(object sender, EventArgs e)
        {
            new UsersForm().ShowDialog(this);
        }

        private void logButton_Click(object sender, EventArgs e)
        {
            SwitchLogView();
        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            new Settings().ShowDialog(this);
        }

        private void chatLog_ItemActivate(object sender, EventArgs e)
        {
            if ((CuPChat.MessageType)chatLog.SelectedItems[0].SubItems[1].Tag != CuPChat.MessageType.CPMessage && (string)chatLog.SelectedItems[0].Tag != LocalPlayer.Id.ToString())
            {
                string playerId = (string)chatLog.SelectedItems[0].Tag;
                StartPrivateChat(playerId);
            }
        }

        private void broadcastButton_Click(object sender, EventArgs e)
        {
            MessageMode = MessageMode != 2 ? 2 : 0;
        }

        private void pinButton_Click(object sender, EventArgs e)
        {
            if (KeepOnTop)
            {
                TopMost = false;
                pinButton.BackgroundImage = global::CuPAddIn.Properties.Resources.pin;
                toolTip1.SetToolTip(pinButton, "Keep on top");
            }
            else
            {
                TopMost = true;
                pinButton.BackgroundImage = global::CuPAddIn.Properties.Resources.unpin;
                toolTip1.SetToolTip(pinButton, "Do not keep on top");
            }
            KeepOnTop = !KeepOnTop;
        }
    }
}
