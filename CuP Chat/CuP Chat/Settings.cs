﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            checkBox1.Checked = CuPChat.ShowMessages;
            checkBox2.Checked = CuPChat.ShowPMsInGame;
            checkBox3.Checked = CuPChat.ColorMessages;
            notificationLevel = CuPChat.NotificationSettings;
        }

        private CuPChat.NotificationLevel notificationLevel
        {
            get
            {
                if (radioButton1.Checked) return CuPChat.NotificationLevel.None;
                if (radioButton2.Checked) return CuPChat.NotificationLevel.BroadcastPM;
                if (radioButton4.Checked) return CuPChat.NotificationLevel.All;
                return CuPChat.NotificationLevel.CuP;
            }
            set
            {
                switch (value)
                {
                    case CuPChat.NotificationLevel.None:
                        radioButton1.Checked = true;
                        break;
                    case CuPChat.NotificationLevel.BroadcastPM:
                        radioButton2.Checked = true;
                        break;
                    case CuPChat.NotificationLevel.All:
                        radioButton4.Checked = true;
                        break;
                    default:
                        radioButton3.Checked = true;
                        break;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CuPChat.ShowMessages = checkBox1.Checked;
            CuPChat.ShowPMsInGame = checkBox2.Checked;
            CuPChat.ColorMessages = checkBox3.Checked;
            CuPChat.NotificationSettings = notificationLevel;
            CuPChat.SaveSettings();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }  
    }
}
