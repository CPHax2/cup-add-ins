﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;
using System.Net;
using System.Text.RegularExpressions;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
            loadHatList();
            ApiPipeline.Hook();
        }
        List<comboBoxItem> nonMemberHats = new List<comboBoxItem>();
        List<comboBoxItem> memberHats = new List<comboBoxItem>();
        private bool showMember = false;
        private bool addAll = false;
        private bool working = false;
        private int cost = 0;
        private int sendBuyInterval = 2100;
        System.Timers.Timer timer = new System.Timers.Timer();

        public class comboBoxItem
        {
            public string Text { get; set; }
            public string Value { get; set; }
            public int Cost { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        private void loadHatList()
        {
            string jsonString = new WebClient().DownloadString("http://media1.clubpenguin.com/play/en/web_service/game_configs/puffle_items.json");
            string[] json = Regex.Split(jsonString, ",{");
            for (int i = 1; i < json.Length; i++)
            {
                string ID = "", itemName = "";
                int itemCost = 0;
                bool isMemberOnly = false;
                string itemType = "";
                string[] item = json[i].Split('"');
                for (int j = 1; j < item.Length - 3; j++)
                {
                    if (item[j] == "puffle_item_id") ID = item[j += 2];
                    else if (item[j] == "label") itemName = item[j += 2];
                    else if (item[j] == "cost") int.TryParse(item[j += 2], out itemCost);
                    else if (item[j] == "is_member_only" && item[j += 2] != "0") isMemberOnly = true;
                    else if (item[j] == "type") itemType = item[j += 2];
                }
                if (itemType == "head")
                {
                    comboBoxItem newHat = new comboBoxItem { Text = itemName, Value = ID, Cost = itemCost };
                    if (isMemberOnly) memberHats.Add(newHat);
                    else nonMemberHats.Add(newHat);
                }
            }
            showNonMemberHats();
        }

        private void showNonMemberHats()
        {
            comboBox1.Items.Clear();
            foreach(comboBoxItem hat in nonMemberHats)
            {
                comboBox1.Items.Add(hat);
            }
            comboBox1.SelectedIndex = 0;
            showMember = false;
            button2.Text = "Show member hats";
        }

        private void showMemberHats()
        {
            comboBox1.Items.Clear();
            foreach (comboBoxItem hat in memberHats)
            {
                comboBox1.Items.Add(hat);
            }
            comboBox1.SelectedIndex = 0;
            showMember = true;
            button2.Text = "Show non-member hats";
        }

        private void disable()
        {
            comboBox1.Enabled = false;
            numericUpDown1.Enabled = false;
            checkBox1.Enabled = false;
            button2.Enabled = false;
        }

        private void enable()
        {
            comboBox1.Enabled = true;
            numericUpDown1.Enabled = true;
            checkBox1.Enabled = true;
            button2.Enabled = true;
        }

        private void start()
        {
            disable();
            working = true;
            button1.Text = "Cancel";
            if (addAll) handleBuyAll();
            else if (comboBox1.SelectedItem != null) handleBuyNormal();
            else stop();
        }

        private void stop()
        {
            timer.Dispose();
            working = false;
            setStatusText("");
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    enable();
                    button1.Text = "Add";
                    progressBar1.Value = 0;
                });
                return;
            }
            enable();
            button1.Text = "Add";
            progressBar1.Value = 0;
        }

        private void sendBuyHat(string ID)
        {
            Core.SendPacket("%xt%s%p#papi%-1%" + ID + "%");
            if (InvokeRequired) BeginInvoke((MethodInvoker)delegate { progressBar1.PerformStep(); });
            else progressBar1.PerformStep();
        }

        private void updateCost()
        {
            cost = 0;
            if (checkBox1.Checked)
            {
                foreach (comboBoxItem item in comboBox1.Items)
                {
                    cost += item.Cost;
                }
                cost *= (int)numericUpDown1.Value;
                setStatusText("Total cost: " + cost.ToString());
            }
            else if (comboBox1.SelectedItem != null)
            {
                cost = ((comboBoxItem)comboBox1.SelectedItem).Cost * (int)numericUpDown1.Value;
                setStatusText("Total cost: " + cost.ToString());
            }
            else setStatusText("Total cost: undefined");
            
        }

        private bool enoughCoins()
        {
            if (LocalPlayer.Coins >= cost) return true;
            return false;
        }

        private void setStatusText(string text)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { setStatusText(text); });
                return;
            }
            toolStripStatusLabel1.Text = text;
        }

        private void handleBuyNormal()
        {
            comboBoxItem selectedHat = (comboBoxItem)comboBox1.SelectedItem;
            string ID = selectedHat.Value;
            int quantity = (int)numericUpDown1.Value;
            int i = 1;
            progressBar1.Maximum = quantity;
            sendBuyHat(ID);
            setStatusText("Added: " + i.ToString() + "/" + quantity.ToString());
            timer = new System.Timers.Timer { Interval = sendBuyInterval };
            timer.Elapsed += (obj, args) =>
            {
                sendBuyHat(ID);
                i++;
                setStatusText("Added: " + i.ToString() + "/" + quantity.ToString());
                if (i == quantity)
                {
                    stop();
                    setStatusText("Done");
                }
            };
            if (quantity>1) timer.Start();
            else
            {
                stop();
                setStatusText("Done");
            }
        }

        private void handleBuyAll()
        {
            int n = comboBox1.Items.Count * (int)numericUpDown1.Value;
            int i = 0;
            progressBar1.Maximum = n;
            timer = new System.Timers.Timer { Interval = sendBuyInterval };
            timer.Elapsed += (obj, args) =>
            {
                comboBoxItem item = (comboBoxItem)comboBox1.Items[i % comboBox1.Items.Count];
                string ID = item.Value;
                sendBuyHat(ID);
                i++;
                setStatusText("Added: " + i.ToString() + "/" + n.ToString());
                if (i == n)
                {
                    stop();
                    setStatusText("Done");
                }
            };
            timer.Start();
        }

        private void loadHatPicture()
        {
            string ID = ((comboBoxItem)comboBox1.SelectedItem).Value;
            if (ID != null) pictureBox1.Load("http://media1.clubpenguin.com/game/items/images/puffles/icon/60/" + ID + ".png");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (working == false)
            {
                if (showMember && !LocalPlayer.IsMember) MessageBox.Show("Non-members can't add member hats!");
                else if (enoughCoins() || MessageBox.Show("Insufficient coins!\r\nContinue anyway?", "Warning", MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) start();
            }
            else
            {
                stop();
                setStatusText("Cancelled");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (showMember) showNonMemberHats();
            else showMemberHats();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                addAll = true;
                numericUpDown1.Value = 1;
            }
            else addAll = false;
            updateCost();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadHatPicture();
            updateCost();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            updateCost();
        }

    }
}
