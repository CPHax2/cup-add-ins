﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    class IglooItemsAdder
    {
        public static List<IglooItem> memberFurniture = new List<IglooItem>(),
        nonMemberFurniture = new List<IglooItem>(),
        igloos = new List<IglooItem>(),
        floors = new List<IglooItem>(),
        locations = new List<IglooItem>();
        private static string[] freeIgloos = { "29", "43", "55", "67", "76", "78", "83", "97" };

        public class IglooItem
        {
            public string Name { get; set; }
            public string ID { get; set; }
            public int Cost { get; set; }
            public int MaxQuantity { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }

        private static bool isFreeIgloo(string ID)
        {
            foreach (string freeIglooID in freeIgloos)
            {
                if (ID == freeIglooID) return true;
            }
            return false;
        }

        public static void loadFurnitureList()
        {
            string jsonString = new WebClient().DownloadString("http://media1.clubpenguin.com/play/en/web_service/game_configs/furniture_items.json");
            string[] json = jsonString.Split('}');
            for (int i = 0; i < json.Length - 1; i++)
            {
                string itemID = "", itemName = "";
                int itemCost = 0, itemMaxQuantity = 0;
                bool isMemberOnly = false, isRedeemable = false, isBait = false;
                string[] item = json[i].Split('"');
                for (int j = 1; j < item.Length - 2; j++)
                {
                    if (item[j] == "furniture_item_id")
                    {
                        itemID = item[++j];
                        itemID = itemID.Substring(1, itemID.Length - 2);
                    }
                    else if (item[j] == "cost")
                    {
                        string costString = item[++j];
                        int.TryParse(costString.Substring(1, costString.Length - 2), out itemCost);
                    }
                    else if (item[j] == "label") itemName = item[j += 2];
                    else if (item[j] == "is_member_only" && item[j += 2] != "0") isMemberOnly = true;
                    else if (item[j] == "max_quantity") int.TryParse(item[j += 2], out itemMaxQuantity);
                    else if (item[j] == "is_redeemable" && item[j += 2] != "0") isRedeemable = true;
                    else if (item[j] == "is_bait" && item[j += 2] != "") isBait = true;
                }
                if (!isRedeemable && !isBait)
                {
                    IglooItem newItem = new IglooItem { Name = itemName, ID = itemID, Cost = itemCost, MaxQuantity = itemMaxQuantity };
                    if (isMemberOnly) memberFurniture.Add(newItem);
                    else nonMemberFurniture.Add(newItem);
                }
            }
        }

        public static void loadIglooList()
        {
            string jsonString = new WebClient().DownloadString("http://media1.clubpenguin.com/play/en/web_service/game_configs/igloos.json");
            string[] json = jsonString.Split('}');
            for (int i = 2; i < json.Length - 1; i++)
            {
                string itemID = "", itemName = "";
                int itemCost = 0;
                string[] item = json[i].Split('"');
                for (int j = 3; j < item.Length - 2; j++)
                {
                    if (item[j] == "igloo_id") itemID = item[j += 2];
                    else if (item[j] == "name") itemName = item[j += 2];
                    else if (item[j] == "cost") int.TryParse(item[j += 2], out itemCost);
                }
                if (itemCost != 0 || isFreeIgloo(itemID))
                {
                    IglooItem newItem = new IglooItem { Name = itemName, ID = itemID, Cost = itemCost };
                    igloos.Add(newItem);
                }
            }
        }

        public static void loadFloorList()
        {
            string jsonString = new WebClient().DownloadString("http://media1.clubpenguin.com/play/en/web_service/game_configs/igloo_floors.json");
            string[] json = jsonString.Split('}');
            for (int i = 1; i < json.Length - 1; i++)
            {
                string itemID = "", itemName = "";
                int itemCost = 0;
                string[] item = json[i].Split('"');
                for (int j = 1; j < item.Length - 3; j++)
                {
                    if (item[j] == "igloo_floor_id") itemID = item[j += 2];
                    else if (item[j] == "label") itemName = item[j += 2];
                    else if (item[j] == "cost") int.TryParse(item[j += 2], out itemCost);
                }
                IglooItem newItem = new IglooItem { Name = itemName, ID = itemID, Cost = itemCost };
                floors.Add(newItem);
            }
        }

        public static void loadLocationList()
        {
            string jsonString = new WebClient().DownloadString("http://media1.clubpenguin.com/play/en/web_service/game_configs/igloo_locations.json");
            string[] json = jsonString.Split('}');
            for (int i = 1; i < json.Length - 1; i++)
            {
                string itemID = "", itemName = "";
                int itemCost = 0;
                string[] item = json[i].Split('"');
                for (int j = 1; j < item.Length; j++)
                {
                    if (item[j] == "igloo_location_id")
                    {
                        itemID = item[++j];
                        itemID = itemID.Substring(1, itemID.Length - 2);
                    }
                    else if (item[j] == "name") itemName = item[j += 2];
                    else if (item[j] == "cost")
                    {
                        string costString = item[++j];
                        int.TryParse(costString.Substring(1, costString.Length - 2), out itemCost);
                    }
                }
                IglooItem newItem = new IglooItem { Name = itemName, ID = itemID, Cost = itemCost };
                locations.Add(newItem);
            }
        }

        public static void sendAddFurniture(string ID)
        {
            Core.SendPacket("%xt%s%g#af%-1%" + ID + "%");
        }

        public static void sendAddIgloo(string ID)
        {
            Core.SendPacket("%xt%s%g#au%-1%" + ID + "%");
        }

        public static void sendAddFloor(string ID)
        {
            Core.SendPacket("%xt%s%g#ag%-1%" + ID + "%");
        }

        public static void sendAddLocation(string ID)
        {
            Core.SendPacket("%xt%s%g#aloc%-1%" + ID + "%");
        }
    }
}
