﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn;
using Cup.Extensibility;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    [AddIn("CuPAddIn")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form1 form;

        public void OnBoot()
        {

        }

        public void OnStart()
        {
            if (started)
            {
                form.ShowDialog();
            }
            else
            {
                form = new Form1();
                started = true;
            }
        }

        public void OnExit()
        {
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
            started = false;
        }
    }
}
