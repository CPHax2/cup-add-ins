﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;
using System.Net;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        bool working = false;
        bool showingMemberFurniture = false;
        int furnitureCost = 0;
        int iglooCost = 0;
        int floorCost = 0;
        int locationCost = 0;
        System.Timers.Timer timer = new System.Timers.Timer();

        public Form1()
        {
            InitializeComponent();
            init();
            ApiPipeline.Hook();
        }

        private void init()
        {
            IglooItemsAdder.loadFurnitureList();
            IglooItemsAdder.loadIglooList();
            IglooItemsAdder.loadFloorList();
            IglooItemsAdder.loadLocationList();
            showNonMemberFurniture();
            foreach (IglooItemsAdder.IglooItem igloo in IglooItemsAdder.igloos) comboBox2.Items.Add(igloo);
            foreach (IglooItemsAdder.IglooItem floor in IglooItemsAdder.floors) comboBox3.Items.Add(floor);
            foreach (IglooItemsAdder.IglooItem location in IglooItemsAdder.locations) comboBox4.Items.Add(location);
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
            comboBox4.SelectedIndex = 0;
        }

        private bool enoughCoins(int cost)
        {
            return LocalPlayer.Coins >= cost;
        }

        private bool isMyIgloo()
        {
            return (bool)Core.ExecuteFunction("SHELL.isMyIgloo").Result;
        }

        private void showMemberFurniture()
        {
            comboBox1.Items.Clear();
            foreach (IglooItemsAdder.IglooItem item in IglooItemsAdder.memberFurniture)
            {
                comboBox1.Items.Add(item);
            }
            comboBox1.SelectedIndex = 0;
            checkBox4.Visible = false;
            checkBox4.Checked = false;
            showingMemberFurniture = true;
        }

        private void showNonMemberFurniture()
        {
            comboBox1.Items.Clear();
            foreach (IglooItemsAdder.IglooItem item in IglooItemsAdder.nonMemberFurniture)
            {
                comboBox1.Items.Add(item);
            }
            comboBox1.SelectedIndex = 0;
            checkBox4.Visible = true;
            showingMemberFurniture = false;
        }

        private void enable()
        {
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            radioButton1.Enabled = true;
            radioButton2.Enabled = true;
            numericUpDown1.Enabled = true;
            comboBox1.Enabled = true;
            checkBox1.Enabled = true;
            checkBox2.Enabled = true;
            checkBox3.Enabled = true;
            checkBox4.Enabled = true;
        }

        private void disable(int tab)
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            switch (tab)
            {
                case 1:
                    button1.Enabled = true;
                    radioButton1.Enabled = false;
                    radioButton2.Enabled = false;
                    numericUpDown1.Enabled = false;
                    comboBox1.Enabled = false;
                    checkBox4.Enabled = false;
                    break;
                case 2:
                    button2.Enabled = true;
                    checkBox1.Enabled = false;
                    break;
                case 3:
                    button3.Enabled = true;
                    checkBox2.Enabled = false;
                    break;
                case 4:
                    button4.Enabled = true;
                    checkBox3.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        private void start(int type)
        {
            progressBar1.Value = 0;
            toolStripStatusLabel1.Text = "";
            switch (type)
            {
                case 1:
                    if (comboBox1.SelectedItem == null) break;
                    string furnitureID = ((IglooItemsAdder.IglooItem)comboBox1.SelectedItem).ID;
                    int quantity = ((checkBox4.Checked) ? comboBox1.Items.Count : 1) * (int)numericUpDown1.Value;
                    progressBar1.Maximum = quantity;
                    if (quantity == 1)
                    {
                        IglooItemsAdder.sendAddFurniture(furnitureID);
                        toolStripStatusLabel1.Text = "Done";
                    }
                    else
                    {
                        disable(1);
                        button1.Text = "Cancel";
                        working = true;
                        int n = 0;
                        progressBar1.PerformStep();
                        toolStripStatusLabel1.Text = "Adding furniture: 0/" + quantity.ToString();
                        timer = new System.Timers.Timer { Interval = 550 };
                        timer.Elapsed += (obj, args) =>
                        {
                            if (++n <= quantity)
                            {
                                if (!checkBox4.Checked) IglooItemsAdder.sendAddFurniture(furnitureID);
                                else IglooItemsAdder.sendAddFurniture(((IglooItemsAdder.IglooItem)comboBox1.Items[(n - 1) % comboBox1.Items.Count]).ID);
                                progressBar1.PerformStep();
                                toolStripStatusLabel1.Text = "Adding furniture: " + n.ToString() + "/" + quantity.ToString();
                            }
                            else
                            {
                                stop();
                                toolStripStatusLabel1.Text = "Done";
                            }
                        };
                        timer.Start();
                    }
                    break;

                case 2:
                    string iglooID;
                    if (!checkBox1.Checked)
                    {
                        if (comboBox2.SelectedItem == null) break;
                        iglooID = ((IglooItemsAdder.IglooItem)comboBox2.SelectedItem).ID;
                        IglooItemsAdder.sendAddIgloo(iglooID);
                        toolStripStatusLabel1.Text = "Done";
                    }
                    else
                    {
                        working = true;
                        button2.Text = "Cancel";
                        disable(2);
                        int iglooCount = comboBox2.Items.Count;
                        progressBar1.Maximum = iglooCount;
                        progressBar1.PerformStep();
                        toolStripStatusLabel1.Text = "Adding igloos: 0/" + iglooCount.ToString();
                        int n = 0;
                        timer = new System.Timers.Timer { Interval = 550 };
                        timer.Elapsed += (obj, args) =>
                        {
                            if (++n <= iglooCount)
                            {
                                iglooID = ((IglooItemsAdder.IglooItem)comboBox2.Items[n-1]).ID;
                                IglooItemsAdder.sendAddIgloo(iglooID);
                                progressBar1.PerformStep();
                                toolStripStatusLabel1.Text = "Adding igloos: " + n.ToString() + "/" + iglooCount.ToString();
                            }
                            else 
                            {
                                stop();
                                toolStripStatusLabel1.Text = "Done";
                            }
                        };
                        timer.Start();
                    }
                    break;

                case 3:
                    string floorID;
                    if (!checkBox2.Checked)
                    {
                        if (comboBox3.SelectedItem == null) break;
                        floorID = ((IglooItemsAdder.IglooItem)comboBox3.SelectedItem).ID;
                        IglooItemsAdder.sendAddFloor(floorID);
                        toolStripStatusLabel1.Text = "Done";
                    }
                    else
                    {
                        working = true;
                        button3.Text = "Cancel";
                        disable(3);
                        int floorCount = comboBox3.Items.Count;
                        int n = 0;
                        progressBar1.Maximum = floorCount;
                        progressBar1.PerformStep();
                        toolStripStatusLabel1.Text = "Adding floors: 0/" + floorCount.ToString();
                        timer = new System.Timers.Timer { Interval = 550 };
                        timer.Elapsed += (obj, args) =>
                        {
                            if (++n <= floorCount)
                            {
                                floorID = ((IglooItemsAdder.IglooItem)comboBox3.Items[n-1]).ID;
                                IglooItemsAdder.sendAddFloor(floorID);
                                progressBar1.PerformStep();
                                toolStripStatusLabel1.Text = "Adding floors: " + n.ToString() + "/" + floorCount.ToString();
                            }
                            else
                            {
                                stop();
                                toolStripStatusLabel1.Text = "Done";
                            }
                        };
                        timer.Start();
                    }
                    break;

                case 4:
                    string locationID;
                    if (!checkBox3.Checked)
                    {
                        if (comboBox4.SelectedItem == null) break;
                        locationID = ((IglooItemsAdder.IglooItem)comboBox4.SelectedItem).ID;
                        IglooItemsAdder.sendAddLocation(locationID);
                        toolStripStatusLabel1.Text = "Done";
                    }
                    else
                    {
                        working = true;
                        button4.Text = "Cancel";
                        disable(4);
                        int locationCount = comboBox4.Items.Count;
                        int n = 0;
                        progressBar1.Maximum = locationCount;
                        toolStripStatusLabel1.Text = "Adding locations: 0/" + locationCount.ToString();
                        timer = new System.Timers.Timer { Interval = 1050 };
                        timer.Elapsed += (obj, args) =>
                        {
                            if (++n <= locationCount)
                            {
                                locationID = ((IglooItemsAdder.IglooItem)comboBox4.Items[n-1]).ID;
                                IglooItemsAdder.sendAddLocation(locationID);
                                progressBar1.PerformStep();
                                toolStripStatusLabel1.Text = "Adding locations: " + n.ToString() + "/" + locationCount.ToString();
                            }
                            else
                            {
                                stop();
                                toolStripStatusLabel1.Text = "Done";
                            }
                        };
                        timer.Start();
                    }
                    break;
            }
        }

        private void stop()
        {
            timer.Dispose();
            working = false;
            button1.Text = "Add";
            button2.Text = "Add";
            button3.Text = "Add";
            button4.Text = "Add";
            progressBar1.Value = 0;
            enable();
        }

        private void updateCost(int tab)
        {
            switch (tab)
            {
                case 1:
                    furnitureCost = 0;
                    if (comboBox1.SelectedItem != null)
                    {
                        if (!checkBox4.Checked) furnitureCost = ((IglooItemsAdder.IglooItem)comboBox1.SelectedItem).Cost * (int)numericUpDown1.Value;
                        else foreach (IglooItemsAdder.IglooItem item in comboBox1.Items) furnitureCost += item.Cost * (int)numericUpDown1.Value;
                        label3.Text = "Total cost: " + furnitureCost.ToString();
                    }
                    else label3.Text = "Total cost: undefined";
                    break;
                case 2:
                    iglooCost = 0;
                    if (checkBox1.Checked)
                    {
                        foreach (IglooItemsAdder.IglooItem item in comboBox2.Items) iglooCost += item.Cost;
                        label5.Text = "Total cost: " + iglooCost.ToString();
                    }
                    else if (comboBox2.SelectedItem != null)
                    {
                        iglooCost = ((IglooItemsAdder.IglooItem)comboBox2.SelectedItem).Cost;
                        label5.Text = "Total cost: " + iglooCost.ToString();
                    }
                    else label5.Text = "Total cost: undefined";
                    break;
                case 3:
                    floorCost = 0;
                    if (checkBox2.Checked)
                    {
                        foreach (IglooItemsAdder.IglooItem item in comboBox3.Items) floorCost += item.Cost;
                        label7.Text = "Total cost: " + floorCost.ToString();
                    }
                    else if (comboBox3.SelectedItem != null)
                    {
                        floorCost = ((IglooItemsAdder.IglooItem)comboBox3.SelectedItem).Cost;
                        label7.Text = "Total cost: " + floorCost.ToString();
                    }
                    else label7.Text = "Total cost: undefined";
                    break;
                case 4:
                    locationCost = 0;
                    if(checkBox3.Checked)
                    {
                        foreach (IglooItemsAdder.IglooItem item in comboBox4.Items) locationCost += item.Cost;
                        label9.Text = "Total cost: " + locationCost.ToString();
                    }
                    else if (comboBox4.SelectedItem != null)
                    {
                        locationCost = ((IglooItemsAdder.IglooItem)comboBox4.SelectedItem).Cost;
                        label9.Text = "Total cost: " + locationCost.ToString();
                    }
                    else label9.Text = "Total cost: undefined";
                    break;
            }

        }

        private void updateMaxQuantity()
        {
            numericUpDown1.Maximum = (decimal)((IglooItemsAdder.IglooItem)comboBox1.SelectedItem).MaxQuantity;
        }

        private void loadFurniturePicture()
        {
            string ID = ((IglooItemsAdder.IglooItem)comboBox1.SelectedItem).ID;
            pictureBox1.Load("http://media8.clubpenguin.com/game/items/images/furniture/icon/60/" + ID + ".png");
        }

        private void loadIglooPicture()
        {
            string ID = ((IglooItemsAdder.IglooItem)comboBox2.SelectedItem).ID;
            pictureBox2.Load("http://media8.clubpenguin.com/game/items/images/igloos/buildings/icon/60/" + ID + ".png");
        }

        private void loadFloorPicture()
        {
            string ID = ((IglooItemsAdder.IglooItem)comboBox3.SelectedItem).ID;
            pictureBox3.Load("http://media8.clubpenguin.com/game/items/images/igloos/flooring/icon/60/" + ID + ".png");
        }

        private void loadLocationPicture()
        {
            string ID = ((IglooItemsAdder.IglooItem)comboBox4.SelectedItem).ID;
            pictureBox4.Load("http://mobcdn.clubpenguin.com/game/items/images/igloos/locations/icon/60/" + ID + ".png");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!working)
            {
                toolStripStatusLabel1.Text = "";
                if (showingMemberFurniture && !LocalPlayer.IsMember) MessageBox.Show("Non-members can not add member furniture!");
                else if (!enoughCoins(furnitureCost))
                {
                    if (MessageBox.Show("Insufficient coins!\r\nContinue anyway?", "Warning", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) start(1);
                }
                else start(1);
            }
            else
            {
                stop();
                toolStripStatusLabel1.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!working)
            {
                toolStripStatusLabel1.Text = "";
                if (!LocalPlayer.IsMember) MessageBox.Show("Non-members can not add igloos!");
                else if (!enoughCoins(iglooCost))
                {
                    if (MessageBox.Show("Insufficient coins!\r\nContinue anyway?", "Warning", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) start(2);
                }
                else start(2);
            }
            else
            {
                stop();
                toolStripStatusLabel1.Text = "";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!working)
            {
                toolStripStatusLabel1.Text = "";
                if (!LocalPlayer.IsMember) MessageBox.Show("Non-members can not add floors!");
                else if (!enoughCoins(floorCost))
                {
                    if (MessageBox.Show("Insufficient coins!\r\nContinue anyway?", "Warning", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) start(3);
                }
                else start(3);
            }
            else
            {
                stop();
                toolStripStatusLabel1.Text = "";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!working)
            {
                toolStripStatusLabel1.Text = "";
                if (!LocalPlayer.IsMember) MessageBox.Show("Non-members can not add igloo locations!");
                else if (!enoughCoins(locationCost))
                {
                    if (MessageBox.Show("Insufficient coins!\r\nContinue anyway?", "Warning", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) start(4);
                }
                else start(4);
            }
            else
            {
                stop();
                toolStripStatusLabel1.Text = "";
            }
        }

        private void playMusic(string musicId)
        {
            Core.ExecuteFunction("MUSIC.unMuteSound");
            Core.ExecuteFunction("MUSIC.unMuteMusic");
            Core.ExecuteFunction("SHELL.startMusicById", musicId);
        }

        private void setMusic(string musicId)
        {
            Core.ExecuteFunction("SHELL.setIglooMusicId", musicId);
        }
        
        private void button5_Click(object sender, EventArgs e)
        {
            int ID;
            if ((int.TryParse(textBox1.Text, out ID)) && (ID >= 0)) playMusic(ID.ToString());
            else MessageBox.Show("Wrong ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!LocalPlayer.IsMember)
            {
                MessageBox.Show("Non-members can not set igloo music!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int ID;
            if ((int.TryParse(textBox1.Text, out ID)) && (ID >= 0))
            {
                string musicID = textBox1.Text;
                setMusic(musicID);
                if (isMyIgloo()) playMusic(musicID);
            }
            else MessageBox.Show("Wrong ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked) showMemberFurniture();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked) showNonMemberFurniture();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadFurniturePicture();
            updateMaxQuantity();
            updateCost(1);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            updateCost(1);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadIglooPicture();
            updateCost(2);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            updateCost(2);
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadFloorPicture();
            updateCost(3);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            updateCost(3);
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadLocationPicture();
            updateCost(4);
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            updateCost(4);
        }
        
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            updateCost(1);
        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) button1_Click(sender, e);
        }

        private void comboBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) button2_Click(sender, e);
        }

        private void comboBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) button3_Click(sender, e);
        }

        private void comboBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) button4_Click(sender, e);
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Modifiers == Keys.Control) || (e.Modifiers == (Keys.Control | Keys.Shift)))
            {
                if (e.KeyCode != Keys.A && e.KeyCode != Keys.C && e.KeyCode != Keys.V && e.KeyCode != Keys.X && e.KeyCode != Keys.Z && e.KeyCode != Keys.Y && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else if (e.Modifiers == Keys.Shift)
            {
                if (e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else
            {
                if ((e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) && (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) && (e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete) && (e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Right)) e.SuppressKeyPress = true;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    button5_Click(sender, e);
                }
            }
        }
        
    }
}
