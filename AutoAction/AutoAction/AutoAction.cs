﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;
using System.Net.Http;

namespace CuPAddIn
{
    public class AutoAction
    {
        public static System.Timers.Timer timer;
        public static List<ListViewItem> actionClipboard = new List<ListViewItem>();
        private static HttpClient httpClient = new HttpClient();

        public static void copyActions(List<ListViewItem> actionList)
        {
            actionClipboard.Clear();
            for (int i = 0; i < actionList.Count; i++)
            {
                actionClipboard.Add((ListViewItem)actionList[i].Clone());
            }
        }

        public class PlayerAction
        {
            public Int16 ID { get; set; }
            public string name { get; set; }
            public Action<object[]> action { get; set; }
            public object[] args { get; set; }
            public string[] argNames { get; set; }

            public override string ToString()
            {
                return name;
            }

        }

        public static void Init()
        {
            httpClient.BaseAddress = new Uri("https://api.disney.com/social/autocomplete/v2/search/messages");
            httpClient.DefaultRequestHeaders.Host = "api.disney.com";
            httpClient.DefaultRequestHeaders.Connection.Add("keep-alive");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("FD", "08306ECE-C36C-4939-B65F-4225F37BD296:905664F40E29B95CF5810B2ACA85497C7430BB1498E74B52");
            httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Referrer = new Uri("http://media1.clubpenguin.com/play/v2/client/club_penguin.swf?");
        }

        public static async System.Threading.Tasks.Task<string> GetPCID(string text, string lang)
        {
            string json = "{\"product\":\"pen\",\"original_text\":\"" + text + "\",\"language\":\"" + lang + "\"}";
            var response = await httpClient.PostAsync("https://api.disney.com/social/autocomplete/v2/search/messages", new StringContent(json, Encoding.UTF8, "application/json"));
            var responseString = await response.Content.ReadAsStringAsync();
            string[] parts = responseString.Split('"');
            if (parts[1] == "id") return parts[3];
            return null;
        }

        public static PlayerAction[] actionList = new PlayerAction[]
        {
            new PlayerAction
            {
                ID = 0,
                name = "Send Message",
                args = new object[1]{ "" },
                argNames = new string[1]{ "Message" },
                action = async (args) => 
                {
                    string message = (string)(args[0]);
                    Core.ExecuteFunction("INTERFACE.handlePhraseAutocompleteText", message);
                    string pcid = await GetPCID(message, "en");
                    string internalRoomId = Core.ExecuteFunction("SHELL.getCurrentServerRoomId").RawResult;
                    if (pcid != null) Core.SendPacketAsync("%xt%s%m#pcam%" + internalRoomId + "%" + pcid + "%", null);
                    else Core.SendPacketAsync("%xt%s%m#sm%" + internalRoomId + "%" + LocalPlayer.Id + "%" + message + "%", null);
                }
            },
            new PlayerAction
            {
                ID = 7,
                name = "Send Safe Message",
                args = new object[1]{ new int() },
                argNames = new string[1]{ "Safe Message ID" },
                action = (args) =>
                {
                    int ID = (int)args[0];
                    Core.ExecuteFunction("INTERFACE.sendSafeMessage", ID);
                }
            },
            new PlayerAction
            {
                ID = 1,
                name = "Move",
                args = new object[2]{ new int(), new int() },
                argNames = new string[2]{ "X", "Y" },
                action = (args) => 
                {
                    int x = (int)(args[0]);
                    int y = (int)(args[1]);
                    Core.ExecuteFunction("ENGINE.sendPlayerMove", x, y);
                }
            },
            new PlayerAction
            {
                ID = 2,
                name = "Send Frame",
                args = new object[1]{ new int() },
                argNames = new string[1]{ "Frame ID" },
                action = (args) => 
                {
                    int ID = (int)(args[0]);
                    Core.ExecuteFunction("INTERFACE.sendPlayerFrame", ID);
                }
            },
            new PlayerAction
            {
                ID = 3,
                name = "Send Emote",
                args = new object[1]{ new int() },
                argNames = new string[1]{ "Emote ID" },
                action = (args) => 
                {
                    int ID = (int)(args[0]);
                    Core.ExecuteFunction("INTERFACE.sendEmote", ID);
                }
            },
            new PlayerAction
            {
                ID = 4,
                name = "Throw Snowball",
                args = new object[2]{ new int(), new int() },
                argNames = new string[2]{ "X", "Y" },
                action = (args) => 
                {
                    int x = (int)(args[0]);
                    int y = (int)(args[1]);
                    Core.ExecuteFunction("INTERFACE.sendThrowBall", x, y);
                }
            },
            new PlayerAction
            {
                ID = 5,
                name = "Send Joke",
                args = new object[1]{ new int() },
                argNames = new string[1]{ "Joke ID" },
                action = (args) =>
                {
                    int ID = (int)(args[0]);
                    Core.ExecuteFunction("SHELL.sendJoke", ID);
                    string joke = Core.ExecuteFunction("SHELL.getJokeById", ID).RawResult;
                    Core.ExecuteFunction("INTERFACE.showBalloon", LocalPlayer.Id, joke);
                }
            },
            new PlayerAction
            {
                ID = 6,
                name = "Join Room",
                args = new object[3]{ new int(), new int(), new int() },
                argNames = new string[3]{ "Room ID", "X", "Y" },
                action = (args) =>
                {
                    int ID = (int)(args[0]);
                    int x = (int)(args[1]);
                    int y = (int)(args[2]);
                    Core.SendPacket("%xt%s%j#jr%-1%" + ID.ToString() + "%" + x.ToString() + "%" + y.ToString() + "%");
                }
            }
        };

        private static PlayerAction playerActionById(int id)
        {
            foreach (PlayerAction pa in actionList)
            {
                if (pa.ID == id) return pa;
            }
            return null;
        }

        public static void openFile()
        {
            OpenFileDialog dialog = new OpenFileDialog { Filter = "AutoAction List (*.aal)|*.aal" };
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() => 
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string path = dialog.FileName;
                    byte[] file = System.IO.File.ReadAllBytes(path);
                    int actionCount = BitConverter.ToInt32(file, 0);
                    ListViewItem[] list = new ListViewItem[actionCount];
                    int pos = 4;
                    for (int i = 0; i < actionCount; i++)
                    {
                        Int16 actionID = BitConverter.ToInt16(file, pos);
                        pos += 2;
                        PlayerAction PA = playerActionById(actionID);
                        object[] args = new object[PA.args.Length];
                        int j = 0;
                        foreach (object arg in PA.args)
                        {
                            if (arg is string)
                            {
                                int stringLength = BitConverter.ToInt32(file, pos);
                                pos += 4;
                                args[j++] = System.Text.Encoding.Default.GetString(file, pos, stringLength);
                                pos += stringLength;
                            }
                            else
                            {
                                args[j++] = BitConverter.ToInt32(file, pos);
                                pos += 4;
                            }
                        }
                        string text = PA.name + " (";
                        foreach (object arg in args)
                        {
                            string argString = arg.ToString();
                            text += text[text.Length - 1] == '(' ? argString : ", " + argString;
                        }
                        text += ")";
                        int interval = BitConverter.ToInt32(file, pos);
                        pos += 4;
                        PlayerAction newPlayerAction = new PlayerAction { ID = actionID, name = PA.name, args = args, argNames = PA.argNames, action = PA.action };
                        ListViewItem newAction = new ListViewItem { Text = text, Tag = newPlayerAction, SubItems = { new ListViewItem.ListViewSubItem { Text = interval.ToString(), Tag = interval } } };
                        list[i] = newAction;
                    }
                    AddInBase.form.loadActions(list);
                }
            }));
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
        }

        public static void saveFile(ListViewItem[] actionList)
        {
            SaveFileDialog dialog = new SaveFileDialog { Filter = "AutoAction List (*.aal)|*.aal" };
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() => 
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string path = dialog.FileName;
                    int fileLength = 4 + actionList.Length * 6;
                    foreach (ListViewItem action in actionList)
                    {
                        foreach (object arg in ((PlayerAction)action.Tag).args)
                        {
                            fileLength += 4;
                            if (arg is string) fileLength += ((string)arg).Length;
                        }
                    }
                    byte[] file = new byte[fileLength];
                    byte[] numberOfActions = BitConverter.GetBytes(actionList.Length);
                    System.Buffer.BlockCopy(numberOfActions, 0, file, 0, 4);
                    int pos = 4;
                    foreach (ListViewItem action in actionList)
                    {
                        byte[] actionID = BitConverter.GetBytes(((PlayerAction)action.Tag).ID);
                        System.Buffer.BlockCopy(actionID, 0, file, pos, 2);
                        pos += 2;
                        foreach (object arg in ((PlayerAction)action.Tag).args)
                        {
                            if (arg is int)
                            {
                                byte[] byteArg = BitConverter.GetBytes((Int32)arg);
                                System.Buffer.BlockCopy(byteArg, 0, file, pos, 4);
                                pos += 4;
                            }
                            else
                            {
                                int stringLength = ((string)arg).Length;
                                byte[] size = BitConverter.GetBytes(stringLength);
                                System.Buffer.BlockCopy(size, 0, file, pos, 4);
                                pos += 4;
                                byte[] byteArg = System.Text.Encoding.Default.GetBytes((string)arg);
                                System.Buffer.BlockCopy(byteArg, 0, file, pos, stringLength);
                                pos += stringLength;
                            }
                        }
                        byte[] interval = BitConverter.GetBytes((Int32)action.SubItems[1].Tag);
                        System.Buffer.BlockCopy(interval, 0, file, pos, 4);
                        pos += 4;
                    }
                    System.IO.File.WriteAllBytes(path, file);
                }
            }));
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
        }
    }
}
