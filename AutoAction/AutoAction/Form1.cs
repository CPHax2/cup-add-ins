﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
        }

        private static Form2 newActionForm;
        private static Form3 editForm;
        private bool working = false;

        public void addAction(ListViewItem action)
        {
            actionList.Items.Add(action);
        }

        private void editSelectedItem()
        {
            editForm = new Form3();
            editForm.Index = actionList.SelectedItems[0].Index;
            editForm.Interval = (int)actionList.SelectedItems[0].SubItems[1].Tag; ;
            editForm.playerAction = (AutoAction.PlayerAction)actionList.SelectedItems[0].Tag;
            editForm.ShowDialog();
        }

        private void copySelectedItems()
        {
            List<ListViewItem> actionsToCopy = new List<ListViewItem>();
            foreach (ListViewItem action in actionList.SelectedItems)
            {
                actionsToCopy.Add(action);
            }
            AutoAction.copyActions(actionsToCopy);
        }

        private void pasteActions()
        {
            foreach (ListViewItem action in AutoAction.actionClipboard) actionList.Items.Add((ListViewItem)action.Clone());
            if (actionList.Items.Count > 0) actionList.Items[actionList.Items.Count - 1].EnsureVisible();
        }

        public void editItems(int index, string name, AutoAction.PlayerAction action, int interval)
        {
                ListViewItem oldItem = actionList.Items[index];
                ListViewItem newItem = new ListViewItem { SubItems = { new ListViewItem.ListViewSubItem(), new ListViewItem.ListViewSubItem() } };
                newItem.Text = name == null ? oldItem.Text : name;
                newItem.Tag = action == null ? oldItem.Tag : action;
                if (interval >= 100 && interval <= 120000)
                {
                    newItem.SubItems[1].Text = interval.ToString();
                    newItem.SubItems[1].Tag = interval;
                }
                else
                {
                    newItem.SubItems[1].Text = oldItem.SubItems[1].Text;
                    newItem.SubItems[1].Tag = oldItem.SubItems[1].Tag;
                }
                newItem.Checked = oldItem.Checked;
                actionList.Items.RemoveAt(index);
                actionList.Items.Insert(index, newItem);
        }

        private void checkAll(bool check)
        {
            foreach (ListViewItem action in actionList.Items) action.Checked = check;
        }

        private void removeSelectedActions()
        {
            foreach (ListViewItem action in actionList.SelectedItems) action.Remove();
        }

        public void loadActions(ListViewItem[] list)
        {
            foreach (ListViewItem action in list) actionList.Items.Add(action);
        }

        private void start()
        {
            working = true;
            actionList.Enabled = false;
            addButton.Enabled = false;
            openFileButton.Enabled = false;
            saveFileButton.Enabled = false;
            editButton.Enabled = false;
            upButton.Enabled = false;
            downButton.Enabled = false;
            checkAllButton.Enabled = false;
            unCheckAllButton.Enabled = false;
            deleteButton.Enabled = false;
            eraseButton.Enabled = false;
            startStopButton.BackgroundImage = Properties.Resources.stop;
            toolTip1.SetToolTip(startStopButton, "Stop");
            int i = 0;
            AutoAction.timer = new System.Timers.Timer();
            AutoAction.timer.Interval = (int)actionList.CheckedItems[0].SubItems[1].Tag;
            AutoAction.timer.Elapsed += (obj, args) =>
            {
                ((AutoAction.PlayerAction)actionList.CheckedItems[i].Tag).action.Invoke(((AutoAction.PlayerAction)actionList.CheckedItems[i].Tag).args);
                AutoAction.timer.Interval = (int)actionList.CheckedItems[i].SubItems[1].Tag;
                if (++i >= actionList.CheckedItems.Count)
                {
                    if (loopCheckBox.Checked) i = 0;
                    else stop();
                }
            };
            ((AutoAction.PlayerAction)actionList.CheckedItems[0].Tag).action.Invoke(((AutoAction.PlayerAction)actionList.CheckedItems[0].Tag).args);
            if (actionList.CheckedItems.Count > 1)
            {
                i++;
                AutoAction.timer.Start();
            }
            else if (loopCheckBox.Checked) AutoAction.timer.Start();
            else stop();
        }

        private void stop()
        {
            if (AutoAction.timer != null) AutoAction.timer.Dispose();
            startStopButton.BackgroundImage = Properties.Resources.start;
            toolTip1.SetToolTip(startStopButton, "Start");
            actionList.Enabled = true;
            addButton.Enabled = true;
            openFileButton.Enabled = true;
            saveFileButton.Enabled = true;
            editButton.Enabled = true;
            upButton.Enabled = true;
            downButton.Enabled = true;
            checkAllButton.Enabled = true;
            unCheckAllButton.Enabled = true;
            deleteButton.Enabled = true;
            eraseButton.Enabled = true;
            working = false;
        }

        private void startStopButton_Click(object sender, EventArgs e)
        {
            if (!working)
            {
                if (actionList.CheckedItems.Count > 0) start();
                else MessageBox.Show("Check at least one action!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else stop();
        }
        
        private void addButton_Click(object sender, EventArgs e)
        {
            newActionForm = new Form2();
            newActionForm.ShowDialog();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (actionList.SelectedItems.Count > 0) editSelectedItem();
        }
        
        private void upButton_Click(object sender, EventArgs e)
        {
            if (actionList.SelectedItems.Count == 0 || actionList.SelectedItems[0].Index == 0) return;
            foreach (ListViewItem action in actionList.SelectedItems)
            {
                int index = action.Index;
                actionList.Items.RemoveAt(index);
                actionList.Items.Insert(index - 1, action);
            }
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            int count = actionList.Items.Count;
            int selectedCount = actionList.SelectedItems.Count;
            if (selectedCount == 0 || actionList.SelectedItems[selectedCount - 1].Index == count - 1) return;
            for (int i = selectedCount - 1; i >= 0; i--)
            {
                ListViewItem action = actionList.SelectedItems[i];
                int index = action.Index;
                actionList.Items.RemoveAt(index);
                actionList.Items.Insert(index + 1, action);
            }
        }

        private void checkAllButton_Click(object sender, EventArgs e)
        {
            checkAll(true);
        }

        private void unCheckAllButton_Click(object sender, EventArgs e)
        {
            checkAll(false);
        }
        
        private void deleteButton_Click(object sender, EventArgs e)
        {
            removeSelectedActions();
        }

        private void eraseButton_Click(object sender, EventArgs e)
        {
            actionList.Items.Clear();
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            AutoAction.openFile();
        }

        private void saveFileButton_Click(object sender, EventArgs e)
        {
            if (actionList.Items.Count > 0)
            {
                ListViewItem[] list = new ListViewItem[actionList.Items.Count];
                int i = 0;
                foreach (ListViewItem action in actionList.Items)
                {
                    list[i++] = action;
                }
                AutoAction.saveFile(list);
            }
            else MessageBox.Show("There are no actions to save!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void performActionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ((AutoAction.PlayerAction)actionList.SelectedItems[0].Tag).action.Invoke(((AutoAction.PlayerAction)actionList.SelectedItems[0].Tag).args);
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editSelectedItem();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copySelectedItems();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pasteActions();
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            removeSelectedActions();
        }

        private void actionList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) removeSelectedActions();
            else if (e.KeyCode == Keys.Apps) contextMenuStrip.Show(Cursor.Position);
            else if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.C) copySelectedItems();
                else if (e.KeyCode == Keys.V) pasteActions();
                else if (e.KeyCode == Keys.A)
                {
                    foreach (ListViewItem action in actionList.Items) action.Selected = true;
                }
            }
        }
        
        private void actionList_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) contextMenuStrip.Show(Cursor.Position);
        }

        private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            contextMenuStrip.Items[3].Enabled = AutoAction.actionClipboard.Count > 0 ? true : false;
            if (actionList.SelectedItems.Count > 0)
            {
                contextMenuStrip.Items[0].Enabled = true;
                contextMenuStrip.Items[1].Enabled = true;
                contextMenuStrip.Items[2].Enabled = true;
                contextMenuStrip.Items[4].Enabled = true;
            }
            else
            {
                contextMenuStrip.Items[0].Enabled = false;
                contextMenuStrip.Items[1].Enabled = false;
                contextMenuStrip.Items[2].Enabled = false;
                contextMenuStrip.Items[4].Enabled = false;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            contextMenuStrip.Dispose();
            InitializeContextMenuStrip();
        }
        
    }
}
