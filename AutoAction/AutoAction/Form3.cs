﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form3 : Cup.Extensibility.Library.CuPForm
    {
        public int Index { get; set; }
        public int Interval { get; set; }
        public AutoAction.PlayerAction playerAction { get; set; }

        List<TextBox> textBoxList = new List<TextBox>();
        List<Label> labelList = new List<Label>();
        private int nextLabelY = 62;
        private int nextTextBoxY = 59;
        private bool loaded = false;

        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            foreach (AutoAction.PlayerAction action in AutoAction.actionList)
            {
                comboBox1.Items.Add(action);
                if (action.name == playerAction.name)
                {
                    comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
                    playerAction.action = action.action;
                }
            }
            if (Interval >= 100 && Interval <= 120000) numericUpDown1.Value = Interval;
        }

        private void addLabel(string text)
        {
            var label = new Label();
            label.Name = "lbl" + labelList.Count.ToString();
            label.AutoSize = true;
            label.Location = new Point(12, nextLabelY);
            nextLabelY += 26;
            label.Text = text;
            label.TabIndex = 4 + labelList.Count * 2;
            labelList.Add(label);
            this.Controls.Add(label);
        }
        
        private void addTextBox(string text)
        {
            var tb = new TextBox();
            tb.Name = "tb" + textBoxList.Count.ToString();
            tb.Location = new Point(12 + labelList[labelList.Count - 1].Width + 6, nextTextBoxY);
            nextTextBoxY += 26;
            tb.Size = new Size(204 - labelList[labelList.Count - 1].Width, 20);
            tb.Text = text;
            tb.TabIndex = labelList[labelList.Count - 1].TabIndex + 1;
            textBoxList.Add(tb);
            this.Controls.Add(tb);
            okButton.Location = new Point(okButton.Location.X, tb.Location.Y + tb.Height + 6);
            cancelButton.Location = new Point(cancelButton.Location.X, tb.Location.Y + tb.Height + 6);
            this.Height += 26;
        }

        private void showArgs()
        {
            foreach (Label label in labelList) this.Controls.Remove(label);
            foreach (TextBox textBox in textBoxList) this.Controls.Remove(textBox);
            labelList.Clear();
            textBoxList.Clear();
            okButton.Location = new Point(okButton.Location.X, 59);
            cancelButton.Location = new Point(cancelButton.Location.X, 59);
            nextLabelY = 62;
            nextTextBoxY = 59;
            this.Height = 122;
            AutoAction.PlayerAction selectedAction = (AutoAction.PlayerAction)comboBox1.SelectedItem;
            for (int i = 0; i < selectedAction.args.Length; i++)
            {
                addLabel(selectedAction.argNames[i] + ":");
                if (!loaded) addTextBox(playerAction.args[i].ToString());
                else addTextBox(selectedAction.args[i] is string ? "" : "0");
            }
            loaded = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            showArgs();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            string setName = playerAction.name + " (";
            foreach (TextBox tb in textBoxList)
            {
                setName += setName[setName.Length - 1] == '(' ? tb.Text : ", " + tb.Text;
            }
            setName += ")";
            AutoAction.PlayerAction selectedAction = (AutoAction.PlayerAction)comboBox1.SelectedItem;
            AutoAction.PlayerAction setAction = new AutoAction.PlayerAction { args = new object[selectedAction.args.Length] };
            setAction.ID = selectedAction.ID;
            setAction.name = selectedAction.name;
            setAction.action = selectedAction.action;
            setAction.argNames = selectedAction.argNames;
            for (int i = 0; i < textBoxList.Count; i++)
            {
                if (selectedAction.args[i] is string) setAction.args[i] = textBoxList[i].Text;
                else
                {
                    int arg = 0;
                    if (int.TryParse(textBoxList[i].Text, out arg)) setAction.args[i] = arg;
                    else
                    {
                        MessageBox.Show("Incorrect arguments!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
            }
            int setInterval = (int)numericUpDown1.Value;
            AddInBase.form.editItems(Index, setName, setAction, setInterval);
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
        
    }
}
