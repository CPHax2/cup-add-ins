﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form2 : Cup.Extensibility.Library.CuPForm
    {
        List<TextBox> textBoxList = new List<TextBox>();
        List<Label> labelList = new List<Label>();
        private int nextLabelY = 62;
        private int nextTextBoxY = 59;

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            foreach (AutoAction.PlayerAction action in AutoAction.actionList)
            {
                comboBox1.Items.Add(action);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void addTextBox(string text)
        {
            var tb = new TextBox();
            tb.Name = "tb" + textBoxList.Count.ToString();
            tb.Location = new Point(12 + labelList[labelList.Count-1].Width + 6, nextTextBoxY);
            nextTextBoxY += 26;
            tb.Size = new Size(this.Width - 36 - labelList[labelList.Count - 1].Width, 20);
            tb.Text = text;
            tb.TabIndex = 5 + textBoxList.Count * 2;
            textBoxList.Add(tb);
            this.Controls.Add(tb);
            actionButton.Location = new Point(actionButton.Location.X, tb.Location.Y + tb.Height + 6);
            addButton.Location = new Point(addButton.Location.X, tb.Location.Y + tb.Height + 6);
            cancelButton.Location = new Point(cancelButton.Location.X, tb.Location.Y + tb.Height + 6);
            this.Height += 26;
        }

        private void addLabel(string text)
        {
            var label = new Label();
            label.Name = "lbl" + labelList.Count.ToString();
            label.AutoSize = true;
            label.Location = new Point(12, nextLabelY);
            nextLabelY += 26;
            label.Text = text;
            label.TabIndex = 4 + labelList.Count * 2;
            labelList.Add(label);
            this.Controls.Add(label);
        }


        private void showArgs()
        {
            foreach (Label label in labelList) this.Controls.Remove(label);
            foreach (TextBox textBox in textBoxList) this.Controls.Remove(textBox);
            labelList.Clear();
            textBoxList.Clear();
            nextLabelY = 62;
            nextTextBoxY = 59;
            actionButton.Location = new Point(actionButton.Location.X, 59);
            addButton.Location = new Point(addButton.Location.X, 59);
            cancelButton.Location = new Point(cancelButton.Location.X, 59);
            this.Height = 122;
            AutoAction.PlayerAction selectedAction = (AutoAction.PlayerAction)comboBox1.SelectedItem;
            for (int i = 0; i < selectedAction.args.Length; i++)
            {
                addLabel(selectedAction.argNames[i] + ":");
                addTextBox(selectedAction.args[i] is string ? "" : "0");
            }
        }
        
        private void actionButton_Click(object sender, EventArgs e)
        {
            AutoAction.PlayerAction selectedAction = (AutoAction.PlayerAction)comboBox1.SelectedItem;
            object[] Args = new object[selectedAction.args.Length];
            for (int i = 0; i < Args.Length; i++)
            {
                string argString = textBoxList[i].Text;
                if (selectedAction.args[i] is string) Args[i] = argString;
                else if (selectedAction.args[i] is int)
                {
                    int arg = 0;
                    if (int.TryParse(argString, out arg)) Args[i] = arg;
                    else
                    {
                        MessageBox.Show("Incorrect arguments!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
            }
            selectedAction.action.Invoke(Args);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AutoAction.PlayerAction playerAction = (AutoAction.PlayerAction)comboBox1.SelectedItem;
            object[] Args = new object[playerAction.args.Length];
            string text = playerAction.name + " (";
            for (int i = 0; i < Args.Length; i++)
            {
                string argString = textBoxList[i].Text;
                text += text[text.Length - 1] == '(' ? argString : ", " + argString;
                if (playerAction.args[i] is string) Args[i] = argString;
                else if (playerAction.args[i] is int)
                {
                    int arg = 0;
                    if (int.TryParse(argString, out arg)) Args[i] = arg;
                    else
                    {
                        MessageBox.Show("Incorrect arguments!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
            }
            text += ")";
            AutoAction.PlayerAction newAction = new AutoAction.PlayerAction();
            newAction.ID = playerAction.ID;
            newAction.name = playerAction.name;
            newAction.action = playerAction.action;
            newAction.argNames = playerAction.argNames;
            newAction.args = Args;
            ListViewItem action = new ListViewItem { Text = text, Tag = newAction, SubItems = { new ListViewItem.ListViewSubItem { Text = ((int)numericUpDown1.Value).ToString(), Tag = (int)numericUpDown1.Value } } };
            AddInBase.form.addAction(action);
            this.Close();
        }
        
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            showArgs();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
        
    }
}
