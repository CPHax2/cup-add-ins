﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public System.Timers.Timer displayTimer;

        public Form1()
        {
            InitializeComponent();
        }

        public void startDisplayTimer(int time)
        {
            if (displayTimer != null) displayTimer.Dispose();
            displayTimer = null;
            TimeSpan timeLeft = TimeSpan.FromMilliseconds(time);
            AddInBase.form.toolStripStatusLabel1.Text = "Time left to next dig: " + timeLeft.ToString(@"mm\:ss");
            displayTimer = new System.Timers.Timer { Interval = 1000 };
            displayTimer.Elapsed += (obj, args) =>
            {
                time -= 1000;
                if (time < 0)
                {
                    displayTimer.Stop();
                    displayTimer.Dispose();
                }
                timeLeft = TimeSpan.FromMilliseconds(time);
                BeginInvoke((MethodInvoker)delegate 
                {
                    AddInBase.form.toolStripStatusLabel1.Text = "Time left to next dig: " + timeLeft.ToString(@"mm\:ss");
                });
                
            };
            displayTimer.Start();
        }

        private void start()
        {
            PuffleDigging.start();
            button1.Text = "Stop";
        }

        public void stop()
        {
            PuffleDigging.stop();
            displayTimer.Stop();
            displayTimer.Dispose();
            toolStripStatusLabel1.Text = "";
            button1.Text = "Start";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (PuffleDigging.working == false)
            {
                if (PuffleDigging.isWalkingPuffle()) start();
                else MessageBox.Show("You have to be walking a puffle!");
            }
            else stop();
        }

        private void Form1_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            System.Diagnostics.Process.Start("http://official-cup.wikia.com/wiki/Puffle_Digger");
            e.Cancel = true;
        }

    }
}
