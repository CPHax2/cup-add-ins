﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    class PuffleDigging
    {
        public static bool working = false;
        public static int defaultInterval = 60500;
        public static DateTime lastDig = DateTime.Now - TimeSpan.FromMilliseconds(defaultInterval);
        public static System.Timers.Timer digTimer = new System.Timers.Timer { Interval = defaultInterval };
        public static System.Timers.Timer endTimer = new System.Timers.Timer();

        public static void init()
        {
            Core.GetVarAsync("SHELL.puffleTreasureHuntService._lastSuccessfulDig", (response) =>
            {
                string result = response.RawResult;
                string[] parts = result.Split(' ');
                if (parts.Length != 6) return;
                if (parts[2].Length == 1) result = result.Insert(8, "0");
                DateTime ld = new DateTime();
                try
                {
                    ld = DateTime.ParseExact(result, "ddd MMM dd HH:mm:ss 'GMT'K yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (System.FormatException) { return; }
                lastDig = (ld > lastDig) ? ld : lastDig;
            });
            digTimer.Elapsed += (obj, args) =>
            {
                digTimer.Interval = defaultInterval;
                if (isWalkingPuffle())
                {
                    disableDigging();
                    puffleDig();
                    Core.ExecuteFunction("SHELL.stopPlayerIdleCheck");
                    if (AddInBase.form.displayTimer != null) AddInBase.form.displayTimer.Dispose();
                    AddInBase.form.startDisplayTimer(defaultInterval);
                }
                else AddInBase.form.stop();
            };
            endTimer.Elapsed += (obj, args) => 
            {
                enableDigging();
                endTimer.Stop();
            };
            PacketMonitor.On("p#puffledig", (packet, mode) =>
            {
                lastDig = DateTime.Now;
            }, PacketMode.Send);
        }

        public static bool isWalkingPuffle()
        {
            int ID;
            return (int.TryParse(Core.ExecuteFunction("SHELL.puffleManager.getCurrentWalkingId").RawResult, out ID));
        }

        public static void enableDigging()
        {
            Core.SetVarAsync("SHELL.puffleTreasureHuntService._autoReenable", true, null);
            Core.SetVarAsync("SHELL.puffleTreasureHuntService._puffleTreasureHuntingEnabled", true, null);
        }

        private static void disableDigging()
        {
            Core.SetVarAsync("SHELL.puffleTreasureHuntService._autoReenable", false, null);
            Core.SetVarAsync("SHELL.puffleTreasureHuntService._puffleTreasureHuntingEnabled", false, null);
        }

        private static void puffleDig()
        {
            Core.ExecuteFunction("SHELL.puffleTreasureHuntService.huntForTreasure");
        }

        private static int minTimeToNextDig()
        {
            TimeSpan timeSinceLastDig = DateTime.Now - lastDig;
            if (timeSinceLastDig.TotalMilliseconds >= defaultInterval) return 0;
            return defaultInterval - (int)timeSinceLastDig.TotalMilliseconds;
        }

        public static void start()
        {
            working = true;
            endTimer.Stop();
            disableDigging();
            Core.ExecuteFunction("SHELL.stopPlayerIdleCheck");
            int minTime = minTimeToNextDig();
            if (minTime == 0)
            {
                puffleDig();
                digTimer.Interval = defaultInterval;
                digTimer.Start();
                AddInBase.form.startDisplayTimer(defaultInterval);
            }
            else
            {
                digTimer.Interval = minTime;
                digTimer.Start();
                AddInBase.form.startDisplayTimer(minTime);
            }
        }

        public static void stop()
        {
            digTimer.Stop();
            int minTime = minTimeToNextDig();
            endTimer.Interval = minTime > 0 ? minTime : defaultInterval;
            endTimer.Start();
            Core.ExecuteFunction("SHELL.startPlayerIdleCheck");
            working = false;
        }
    }
}
