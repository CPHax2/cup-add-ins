﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ApiPipeline.Hook();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                Core.ExecuteFunction("ENGINE.hideAllPlayerNames");
                PacketMonitor.On("jr", (packet, mode) =>
                {
                    Core.ExecuteFunction("ENGINE.hideAllPlayerNames");
                });
                PacketMonitor.On("gm", (packet, mode) =>
                {
                    Core.ExecuteFunction("ENGINE.hideAllPlayerNames");
                });
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                Core.ExecuteFunction("ENGINE.showAllPlayerNames");
                PacketMonitor.Off();
            }
        }
    }
}