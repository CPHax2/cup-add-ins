﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class EditForm : Cup.Extensibility.Library.CuPForm
    {
        public int[] index { get; set; }
        public string name { get; set; }
        public string packet { get; set; }
        public string mode { get; set; }
        public int interval { get; set; }

        public EditForm()
        {
            InitializeComponent();
        }

        private void showExtendedForm()
        {
            this.Text = "Edit " + index.Length.ToString() + " packets";
            nameCheckBox.Checked = false;
            packetCheckBox.Checked = false;
            intervalCheckBox.Checked = false;
            nameCheckBox.Visible = true;
            packetCheckBox.Visible = true;
            intervalCheckBox.Visible = true;
            this.Width = 392;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            if (index.Length == 1)
            {
                nameTextBox.Text = name;
                packetTextBox.Text = packet;
                if (mode == "Send")
                {
                    radioButton1.Checked = true;
                    button3.Text = "Send";
                }
                else radioButton2.Checked = true;
                intervalUpDown.Value = interval;
                button3.Visible = true;
            }
            else
            {
                showExtendedForm();
                if (name != null) nameTextBox.Text = name;
                if (packet != null) packetTextBox.Text = packet;
                if (interval >= 100 && interval <= 120000) intervalUpDown.Value = interval;
            }
        }
        
        private void Form2_Shown(object sender, EventArgs e)
        {
            if (mode == "Send") radioButton1.Checked = true;
            else if (mode == "Receive") radioButton2.Checked = true;
            else radioButton1.Checked = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string setName = null;
            if (nameCheckBox.Checked) setName = nameTextBox.Text;
            string setPacket = null;
            if (packetCheckBox.Checked) setPacket = packetTextBox.Text;
            string setMode = null;
            if (radioButton1.Checked) setMode = "Send";
            else if (radioButton2.Checked) setMode = "Receive";
            int setInterval = -1;
            if (intervalCheckBox.Checked) setInterval = (int)intervalUpDown.Value;
            AddInBase.form.editSendListItem(index, setName, setPacket, setMode, setInterval);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked) Cup.Extensibility.Library.Core.SendPacketAsync(packetTextBox.Text, null);
            else Cup.Extensibility.Library.Core.ReceivePacketAsync(packetTextBox.Text, null);
        }

        private void nameCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (nameCheckBox.Checked == true) nameTextBox.Enabled = true;
            else nameTextBox.Enabled = false;
        }

        private void packetCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (packetCheckBox.Checked == true) packetTextBox.Enabled = true;
            else packetTextBox.Enabled = false;
        }

        private void intervalCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (intervalCheckBox.Checked == true) intervalUpDown.Enabled = true;
            else intervalUpDown.Enabled = false;
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            button3.Text = radioButton1.Checked ? "Send" : "Receive";
        }
    }
}
