﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public static EditForm editForm;
        public static NewPacketForm newPacketForm;
        private bool isLogging = false;
        private bool sending = false;
        public bool isShowingNames = false;
        
        public Form1()
        {
            InitializeComponent();
            radioButton1_CheckedChanged(null, null);
            ApiPipeline.Hook();
        }

        private void copyText(string text)
        {
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() => { Clipboard.SetText(text); }));
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
        }

        public void switchNamePacketDisplay()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { switchNamePacketDisplay(); });
                return;
            }
            sendList.BeginUpdate();
            if (!isShowingNames)
            {
                packetHeader.Text = "Name";
                namePacketButton.Text = "Show packets";
                foreach (ListViewItem packet in sendList.Items)
                {
                    string packetString = packet.SubItems[0].Text;
                    packet.SubItems[0].Text = (string)packet.SubItems[0].Tag;
                    packet.SubItems[0].Tag = packetString;
                }
                foreach (ListViewItem packet in PacketTools.packetClipboard)
                {
                    string packetString = packet.SubItems[0].Text;
                    packet.SubItems[0].Text = (string)packet.SubItems[0].Tag;
                    packet.SubItems[0].Tag = packetString;
                }
                isShowingNames = true;
            }
            else
            {
                packetHeader.Text = "Packet";
                namePacketButton.Text = "Show names";
                foreach (ListViewItem packet in sendList.Items)
                {
                    string packetString = (string)packet.SubItems[0].Tag;
                    packet.SubItems[0].Tag = packet.SubItems[0].Text;
                    packet.SubItems[0].Text = packetString;
                }
                foreach (ListViewItem packet in PacketTools.packetClipboard)
                {
                    string packetString = (string)packet.SubItems[0].Tag;
                    packet.SubItems[0].Tag = packet.SubItems[0].Text;
                    packet.SubItems[0].Text = packetString;
                }
                isShowingNames = false;
            }
            sendList.EndUpdate();
        }

        private void copySelectedLogPackets()
        {
            List<ListViewItem> packetList = new List<ListViewItem>();
            string s = "";
            foreach (ListViewItem packet in packetLog.SelectedItems)
            {
                s += (s == "" ? "" : "\r\n") + packet.Text;
                ListViewItem newPacket = new ListViewItem { SubItems = { "", "", "" } };
                if (isShowingNames)
                {
                    newPacket.SubItems[0].Text = "New Packet";
                    newPacket.SubItems[0].Tag = packet.Text;
                }
                else
                {
                    newPacket.SubItems[0].Text = packet.Text;
                    newPacket.SubItems[0].Tag = "New Packet";
                }
                newPacket.SubItems[1].Text = packet.ForeColor == Color.Red ? "Send" : "Receive";
                newPacket.SubItems[2].Text = "1000";
                newPacket.SubItems[2].Tag = 1000;
                packetList.Add(newPacket);
            }
            if (s != "") copyText(s);
            PacketTools.copyPackets(packetList);
        }

        private void copySelectedSendListPackets()
        {
            List<ListViewItem> packetList = new List<ListViewItem>();
            string s = "";
            foreach (ListViewItem packet in sendList.SelectedItems)
            {
                s += (s == "" ? "" : "\r\n") + (isShowingNames ? (string)packet.SubItems[0].Tag : packet.SubItems[0].Text);
                packetList.Add(packet);
            }
            if (s != "") copyText(s);
            PacketTools.copyPackets(packetList);
        }

        private void pastePacketsToSendList()
        {
            foreach (ListViewItem packet in PacketTools.packetClipboard)
            {
                sendList.Items.Add((ListViewItem)packet.Clone());
            }
            if (sendList.Items.Count > 0) sendList.Items[sendList.Items.Count - 1].EnsureVisible();
        }

        private void removeSelectedItems()
        {
            foreach (ListViewItem packet in sendList.SelectedItems) packet.Remove();
        }

        private void editSelectedItems()
        {
            if (sendList.SelectedItems.Count == 0) return;
            int[] Index = new int[sendList.SelectedItems.Count];
            int i = 0;
            foreach (ListViewItem item in sendList.SelectedItems) Index[i++] = item.Index;
            editForm = new EditForm { index = Index };
            string name1, packet1;
            if (isShowingNames)
            {
                name1 = sendList.SelectedItems[0].SubItems[0].Text;
                foreach (ListViewItem item in sendList.SelectedItems)
                {
                    if (item.SubItems[0].Text != name1)
                    {
                        name1 = null;
                        break;
                    }
                }
                packet1 = (string)sendList.SelectedItems[0].SubItems[0].Tag;
                foreach (ListViewItem item in sendList.SelectedItems)
                {
                    if ((string)item.SubItems[0].Tag != packet1)
                    {
                        packet1 = null;
                        break;
                    }
                }
            }
            else
            {
                name1 = (string)sendList.SelectedItems[0].SubItems[0].Tag;
                foreach (ListViewItem item in sendList.SelectedItems)
                {
                    if ((string)item.SubItems[0].Tag != name1)
                    {
                        name1 = null;
                        break;
                    }
                }
                packet1 = sendList.SelectedItems[0].SubItems[0].Text;
                foreach (ListViewItem item in sendList.SelectedItems)
                {
                    if (item.SubItems[0].Text != packet1)
                    {
                        packet1 = null;
                        break;
                    }
                }
            }
            editForm.name = name1;
            editForm.packet = packet1;
            string mode1 = sendList.SelectedItems[0].SubItems[1].Text;
            foreach (ListViewItem item in sendList.SelectedItems)
            {
                if (item.SubItems[1].Text != mode1)
                {
                    mode1 = null;
                    break;
                }
            }
            editForm.mode = mode1;
            int interval1 = (int)sendList.SelectedItems[0].SubItems[2].Tag;
            foreach (ListViewItem item in sendList.SelectedItems)
            {
                if ((int)item.SubItems[2].Tag != interval1)
                {
                    interval1 = -1;
                    break;
                }
            }
            editForm.interval = interval1;
            editForm.ShowDialog(this);
        }
        
        public void editSendListItem(int[] items, string name, string packet, string mode, int interval)
        {
            sendList.BeginUpdate();
            foreach(int i in items)
            {
                ListViewItem oldItem = sendList.Items[i];
                ListViewItem newItem = new ListViewItem { SubItems = { "", "", "" } };
                if (isShowingNames)
                {
                    newItem.SubItems[0].Text = (name == null ? oldItem.SubItems[0].Text : name);
                    newItem.SubItems[0].Tag = (packet == null ? (string)oldItem.SubItems[0].Tag : packet);
                }
                else
                {
                    newItem.SubItems[0].Text = (packet == null ? oldItem.SubItems[0].Text : packet);
                    newItem.SubItems[0].Tag = (name == null ? (string)oldItem.SubItems[0].Tag : name);
                }
                newItem.SubItems[1].Text = (mode == null ? oldItem.SubItems[1].Text : mode);
                if (interval >= 100 && interval <= 120000)
                {
                    newItem.SubItems[2].Text = interval.ToString();
                    newItem.SubItems[2].Tag = interval;
                }
                else
                {
                    newItem.SubItems[2].Text = oldItem.SubItems[2].Text;
                    newItem.SubItems[2].Tag = oldItem.SubItems[2].Tag;
                }
                newItem.Checked = oldItem.Checked;
                sendList.Items.RemoveAt(i);
                sendList.Items.Insert(i, newItem);
            }
            sendList.EndUpdate();
        }

        private void checkAll(bool check)
        {
            foreach (ListViewItem packet in sendList.Items) packet.Checked = check;
        }

        private void sendReceiveSendListPacket(int i)
        {
            if (sendList.Items[i].SubItems[1].Text == "Send")
            {
                if (isShowingNames) Core.SendPacketAsync((string)sendList.Items[i].SubItems[0].Tag, null);
                else Core.SendPacketAsync(sendList.Items[i].SubItems[0].Text, null);
            }
            else
            {
                if (isShowingNames) Core.ReceivePacketAsync((string)sendList.Items[i].SubItems[0].Tag, null);
                else Core.ReceivePacketAsync(sendList.Items[i].SubItems[0].Text, null);
            }
        }

        public void loadPackets(ListViewItem[] packetList)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { loadPackets(packetList); });
                return;
            }
            sendList.BeginUpdate();
            foreach (ListViewItem packet in packetList)
            {
                sendList.Items.Add(packet);
            }
            sendList.EndUpdate();
        }

        private void addSelectedLogPacketsToSendList()
        {
            foreach (ListViewItem packet in packetLog.SelectedItems)
            {
                ListViewItem newPacket = new ListViewItem { SubItems = { "", "", "" } };
                if (isShowingNames)
                {
                    newPacket.SubItems[0].Text = "New Packet";
                    newPacket.SubItems[0].Tag = packet.Text;
                }
                else
                {
                    newPacket.SubItems[0].Text = packet.Text;
                    newPacket.SubItems[0].Tag = "New Packet";
                }
                newPacket.SubItems[1].Text = (packet.ForeColor == Color.Red || packet.ForeColor == Color.DarkViolet) ? "Send" : "Receive";
                newPacket.SubItems[2].Text = "1000";
                newPacket.SubItems[2].Tag = 1000;
                sendList.Items.Add(newPacket);
            }
        }

        private void logPacket(string packet, PacketMode mode)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { logPacket(packet, mode); });
                return;
            }
            Color color;
            if (mode == PacketMode.Send)
            {
                if (!checkBox1.Checked) return;
                color = Color.Red;
            }
            else if (mode == PacketMode.Receive)
            {
                if (!checkBox2.Checked) return;
                color = Color.Green;
            }
            else if (mode == PacketMode.AddInSend)
            {
                if (!checkBox3.Checked) return;
                color = Color.DarkViolet;
            }
            else
            {
                if (!checkBox4.Checked) return;
                color = Color.DarkOrange;
            }
            packetLog.Items.Add(new ListViewItem { Text = packet, ForeColor = color });
            packetLog.Items[packetLog.Items.Count - 1].EnsureVisible();
        }

        private void startLogging()
        {
            PacketMonitor.All((packet, mode) => 
            {
                logPacket(packet, mode);
            });
            isLogging = true;
            button1.Text = "Stop logging";
        }

        private void stopLogging()
        {
            PacketMonitor.Off();
            isLogging = false;
            button1.Text = "Start logging";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!isLogging) startLogging();
            else stopLogging();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            packetLog.Items.Clear();
        }

        private void displayPacketLogMenu()
        {
            if (packetLog.SelectedItems.Count > 0) packetLogMenuStrip.Show(Cursor.Position);
        }

        private void packetLog_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                displayPacketLogMenu();
            }
        }

        private void addToSendListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addSelectedLogPacketsToSendList();
        }
        
        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copySelectedLogPackets();
        }

        private void packetLog_ItemActivate(object sender, EventArgs e)
        {
            addSelectedLogPacketsToSendList();
        }

        private void displaySendListMenu()
        {
            sendListMenuStrip.Items[3].Enabled = PacketTools.packetClipboard.Count > 0 ? true : false;
            if (sendList.SelectedItems.Count > 0)
            {
                sendListMenuStrip.Items[0].Enabled = true;
                sendListMenuStrip.Items[1].Enabled = true;
                sendListMenuStrip.Items[2].Enabled = true;
                sendListMenuStrip.Items[4].Enabled = true;
                sendListMenuStrip.Show(Cursor.Position);
            }
            else
            {
                sendListMenuStrip.Items[0].Enabled = false;
                sendListMenuStrip.Items[1].Enabled = false;
                sendListMenuStrip.Items[2].Enabled = false;
                sendListMenuStrip.Items[4].Enabled = false;
                sendListMenuStrip.Show(Cursor.Position);
            }
        }
        
        private void sendList_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                displaySendListMenu();
            }
        }
        
        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editSelectedItems();
        }

        private void sendReceiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sendReceiveSendListPacket(sendList.SelectedItems[0].Index);
        }
        
        private void copyToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            copySelectedSendListPackets();
        }
        
        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pastePacketsToSendList();
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            removeSelectedItems();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                packetLog.Visible = true;
                label1.Visible = true;
                sendList.Visible = false;
                label2.Visible = false;
                loopCheckBox.Visible = false;
                startStopButton.Visible = false;
                newPacketButton.Visible = false;
                openFileButton.Visible = false;
                saveFileButton.Visible = false;
                editButton.Visible = false;
                upButton.Visible = false;
                downButton.Visible = false;
                checkAllButton.Visible = false;
                unCheckAllButton.Visible = false;
                deleteButton.Visible = false;
                eraseButton.Visible = false;
                namePacketButton.Visible = false;
                this.Size = new Size(this.Width, 299);
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                packetLog.Visible = false;
                label1.Visible = false;
                sendList.Location = new Point(12, 85);
                sendList.Visible = true;
                label2.Location = new Point(12, 67);
                label2.Visible = true;
                startStopButton.Location = new Point(startStopButton.Location.X, 289);
                loopCheckBox.Location = new Point(loopCheckBox.Location.X, 293);
                newPacketButton.Location = new Point(newPacketButton.Location.X, 289);
                openFileButton.Location = new Point(openFileButton.Location.X, 289);
                saveFileButton.Location = new Point(saveFileButton.Location.X, 289);
                editButton.Location = new Point(editButton.Location.X, 289);
                upButton.Location = new Point(upButton.Location.X, 289);
                downButton.Location = new Point(downButton.Location.X, 289);
                checkAllButton.Location = new Point(checkAllButton.Location.X, 289);
                unCheckAllButton.Location = new Point(unCheckAllButton.Location.X, 289);
                deleteButton.Location = new Point(deleteButton.Location.X, 289);
                eraseButton.Location = new Point(eraseButton.Location.X, 289);
                namePacketButton.Location = new Point(namePacketButton.Location.X, 289);
                startStopButton.Visible = true;
                loopCheckBox.Visible = true;
                newPacketButton.Visible = true;
                openFileButton.Visible = true;
                saveFileButton.Visible = true;
                editButton.Visible = true;
                upButton.Visible = true;
                downButton.Visible = true;
                checkAllButton.Visible = true;
                unCheckAllButton.Visible = true;
                deleteButton.Visible = true;
                eraseButton.Visible = true;
                namePacketButton.Visible = true;
                this.Size = new Size(this.Width, 352);
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked) 
            {
                packetLog.Visible = true;
                label1.Visible = true;
                sendList.Location = new Point(12, 280);
                sendList.Visible = true;
                label2.Location = new Point(12, 262);
                label2.Visible = true;
                startStopButton.Location = new Point(startStopButton.Location.X, 484);
                loopCheckBox.Location = new Point(loopCheckBox.Location.X, 488);
                newPacketButton.Location = new Point(newPacketButton.Location.X, 484);
                openFileButton.Location = new Point(openFileButton.Location.X, 484);
                saveFileButton.Location = new Point(saveFileButton.Location.X, 484);
                editButton.Location = new Point(editButton.Location.X, 484);
                upButton.Location = new Point(upButton.Location.X, 484);
                downButton.Location = new Point(downButton.Location.X, 484);
                checkAllButton.Location = new Point(checkAllButton.Location.X, 484);
                unCheckAllButton.Location = new Point(unCheckAllButton.Location.X, 484);
                deleteButton.Location = new Point(deleteButton.Location.X, 484);
                eraseButton.Location = new Point(eraseButton.Location.X, 484);
                namePacketButton.Location = new Point(namePacketButton.Location.X, 484);
                startStopButton.Visible = true;
                loopCheckBox.Visible = true;
                newPacketButton.Visible = true;
                openFileButton.Visible = true;
                saveFileButton.Visible = true;
                editButton.Visible = true;
                upButton.Visible = true;
                downButton.Visible = true;
                checkAllButton.Visible = true;
                unCheckAllButton.Visible = true;
                deleteButton.Visible = true;
                eraseButton.Visible = true;
                namePacketButton.Visible = true;
                this.Size = new Size(this.Width, 547);
            };
        }

        private void startSending()
        {
            sending = true;
            sendList.Enabled = false;
            startStopButton.BackgroundImage = Properties.Resources.stop;
            toolTip1.SetToolTip(startStopButton, "Stop");
            newPacketButton.Enabled = false;
            openFileButton.Enabled = false;
            saveFileButton.Enabled = false;
            editButton.Enabled = false;
            upButton.Enabled = false;
            downButton.Enabled = false;
            checkAllButton.Enabled = false;
            unCheckAllButton.Enabled = false;
            deleteButton.Enabled = false;
            eraseButton.Enabled = false;
        }

        private void stopSending()
        {
            PacketTools.timer.Dispose();
            sendList.Enabled = true;
            startStopButton.BackgroundImage = Properties.Resources.start;
            toolTip1.SetToolTip(startStopButton, "Start");
            newPacketButton.Enabled = true;
            openFileButton.Enabled = true;
            saveFileButton.Enabled = true;
            editButton.Enabled = true;
            upButton.Enabled = true;
            downButton.Enabled = true;
            checkAllButton.Enabled = true;
            unCheckAllButton.Enabled = true;
            deleteButton.Enabled = true;
            eraseButton.Enabled = true;
            sending = false;
        }

        private void startStopButton_Click(object sender, EventArgs e)
        {
            if (!sending)
            {
                if (sendList.CheckedItems.Count > 0)
                {
                    startSending();
                    int i = 0;
                    PacketTools.timer = new System.Timers.Timer { Interval = (int)sendList.CheckedItems[0].SubItems[2].Tag };
                    PacketTools.timer.Elapsed += (obj, args) =>
                    {
                        PacketTools.timer.Interval = (int)sendList.CheckedItems[i].SubItems[2].Tag;
                        sendReceiveSendListPacket(sendList.CheckedItems[i++].Index);
                        if (i >= sendList.CheckedItems.Count)
                        {
                            if (loopCheckBox.Checked) i = 0;
                            else stopSending();
                        }
                    };
                    sendReceiveSendListPacket(sendList.CheckedItems[0].Index);
                    if (sendList.CheckedItems.Count > 1)
                    {
                        i++;
                        PacketTools.timer.Start();
                    }
                    else if (loopCheckBox.Checked) PacketTools.timer.Start();
                    else stopSending();
                }
                else MessageBox.Show("Check at least one packet!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else stopSending();
        }

        private void newPacketButton_Click(object sender, EventArgs e)
        {
            newPacketForm = new NewPacketForm();
            newPacketForm.ShowDialog(this);
        }
        
        private void openFileButton_Click(object sender, EventArgs e)
        {
            PacketTools.openFile();
        }
        
        private void saveFileButton_Click(object sender, EventArgs e)
        {
            if (sendList.Items.Count > 0)
            {
                ListViewItem[] packetList = new ListViewItem[sendList.Items.Count];
                int i = 0;
                foreach (ListViewItem packet in sendList.Items)
                {
                    packetList[i++] = packet;
                }
                PacketTools.saveFile(packetList);
            }
            else MessageBox.Show("There are no packets to save!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        
        private void editButton_Click(object sender, EventArgs e)
        {
            editSelectedItems();
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            int selectedCount = sendList.SelectedItems.Count;
            if (selectedCount == 0 || sendList.SelectedItems[0].Index == 0) return;
            foreach (ListViewItem item in sendList.SelectedItems)
            {
                int Index = item.Index;
                sendList.Items.RemoveAt(Index);
                sendList.Items.Insert(Index - 1, item);
            }
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            int count = sendList.Items.Count;
            int selectedCount = sendList.SelectedItems.Count;
            if (selectedCount == 0 || sendList.SelectedItems[selectedCount - 1].Index == count - 1) return;
            for (int i = selectedCount - 1; i >= 0; i--)
            {
                ListViewItem item = sendList.SelectedItems[i];
                int Index = item.Index;
                sendList.Items.RemoveAt(Index);
                sendList.Items.Insert(Index + 1, item);
            }
        }

        private void checkAllButton_Click(object sender, EventArgs e)
        {
            checkAll(true);
        }

        private void unCheckAllButton_Click(object sender, EventArgs e)
        {
            checkAll(false);
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            removeSelectedItems();
        }

        private void eraseButton_Click(object sender, EventArgs e)
        {
            sendList.Items.Clear();
        }        

        private void packetLog_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Apps) displayPacketLogMenu();
            else if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.C)
                {
                    copySelectedLogPackets();
                }
                else if (e.KeyCode == Keys.A)
                {
                    foreach (ListViewItem packet in packetLog.Items)
                    {
                        packet.Selected = true;
                    }
                }
            }
        }

        private void sendList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) removeSelectedItems();
            else if (e.KeyCode == Keys.Apps) displaySendListMenu();
            else if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.C && sendList.SelectedItems.Count != 0) copySelectedSendListPackets();
                else if (e.KeyCode == Keys.V) pastePacketsToSendList();
                else if (e.KeyCode == Keys.A)
                {
                    foreach (ListViewItem packet in sendList.Items) packet.Selected = true;
                }
            }
        }

        private void namePacketButton_Click(object sender, EventArgs e)
        {
            switchNamePacketDisplay();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            packetLogMenuStrip.Dispose();
            sendListMenuStrip.Dispose();
            InitializeContextMenuStrips();
        }

        private void pinCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            TopMost = pinCheckBox.Checked;
        }
    }
}
