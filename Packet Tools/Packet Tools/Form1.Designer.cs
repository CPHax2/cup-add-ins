﻿namespace CuPAddIn
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeContextMenuStrips()
        {
            this.packetLogMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.packetLogMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToSendListToolStripMenuItem,
            this.copyToolStripMenuItem1});
            this.packetLogMenuStrip.Name = "packetLogMenuStrip";
            this.packetLogMenuStrip.ShowImageMargin = false;
            this.packetLogMenuStrip.Size = new System.Drawing.Size(140, 48);

            this.sendListMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sendListMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.sendReceiveToolStripMenuItem,
            this.copyToolStripMenuItem2,
            this.pasteToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.sendListMenuStrip.Name = "contextMenuStrip1";
            this.sendListMenuStrip.ShowImageMargin = false;
            this.sendListMenuStrip.Size = new System.Drawing.Size(122, 114);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.sendListMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendReceiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.packetLog = new System.Windows.Forms.ListView();
            this.columnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.packetLogMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToSendListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loopCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pinCheckBox = new System.Windows.Forms.CheckBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.sendList = new System.Windows.Forms.ListView();
            this.packetHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.modeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.intervalHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.namePacketButton = new System.Windows.Forms.Button();
            this.unCheckAllButton = new System.Windows.Forms.Button();
            this.checkAllButton = new System.Windows.Forms.Button();
            this.saveFileButton = new System.Windows.Forms.Button();
            this.openFileButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.eraseButton = new System.Windows.Forms.Button();
            this.downButton = new System.Windows.Forms.Button();
            this.upButton = new System.Windows.Forms.Button();
            this.startStopButton = new System.Windows.Forms.Button();
            this.newPacketButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.sendListMenuStrip.SuspendLayout();
            this.packetLogMenuStrip.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(213, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Start logging";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(213, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.ForeColor = System.Drawing.Color.Red;
            this.checkBox1.Location = new System.Drawing.Point(6, 15);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(51, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Send";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.ForeColor = System.Drawing.Color.Green;
            this.checkBox2.Location = new System.Drawing.Point(92, 15);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(66, 17);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "Receive";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox4);
            this.groupBox1.Controls.Add(this.checkBox3);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(195, 58);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Types of packets to log:";
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Checked = true;
            this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox4.ForeColor = System.Drawing.Color.DarkOrange;
            this.checkBox4.Location = new System.Drawing.Point(92, 35);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(97, 17);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "AddIn Receive";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.ForeColor = System.Drawing.Color.DarkViolet;
            this.checkBox3.Location = new System.Drawing.Point(6, 35);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(82, 17);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "AddIn Send";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // sendListMenuStrip
            // 
            this.sendListMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.sendReceiveToolStripMenuItem,
            this.copyToolStripMenuItem2,
            this.pasteToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.sendListMenuStrip.Name = "contextMenuStrip1";
            this.sendListMenuStrip.ShowImageMargin = false;
            this.sendListMenuStrip.Size = new System.Drawing.Size(122, 114);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // sendReceiveToolStripMenuItem
            // 
            this.sendReceiveToolStripMenuItem.Name = "sendReceiveToolStripMenuItem";
            this.sendReceiveToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.sendReceiveToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.sendReceiveToolStripMenuItem.Text = "Send/Receive";
            this.sendReceiveToolStripMenuItem.Click += new System.EventHandler(this.sendReceiveToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem2
            // 
            this.copyToolStripMenuItem2.Name = "copyToolStripMenuItem2";
            this.copyToolStripMenuItem2.ShortcutKeyDisplayString = "Ctrl+C";
            this.copyToolStripMenuItem2.Size = new System.Drawing.Size(121, 22);
            this.copyToolStripMenuItem2.Text = "Copy";
            this.copyToolStripMenuItem2.Click += new System.EventHandler(this.copyToolStripMenuItem2_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl +V";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.ShortcutKeyDisplayString = "Del";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Send List:";
            this.label2.Visible = false;
            // 
            // packetLog
            // 
            this.packetLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.packetLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader});
            this.packetLog.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.packetLog.LabelWrap = false;
            this.packetLog.Location = new System.Drawing.Point(12, 85);
            this.packetLog.Name = "packetLog";
            this.packetLog.Size = new System.Drawing.Size(504, 174);
            this.packetLog.TabIndex = 5;
            this.packetLog.UseCompatibleStateImageBehavior = false;
            this.packetLog.View = System.Windows.Forms.View.Details;
            this.packetLog.ItemActivate += new System.EventHandler(this.packetLog_ItemActivate);
            this.packetLog.KeyDown += new System.Windows.Forms.KeyEventHandler(this.packetLog_KeyDown);
            this.packetLog.MouseClick += new System.Windows.Forms.MouseEventHandler(this.packetLog_MouseClick);
            // 
            // columnHeader
            // 
            this.columnHeader.Text = "Packet";
            this.columnHeader.Width = 483;
            // 
            // packetLogMenuStrip
            // 
            this.packetLogMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToSendListToolStripMenuItem,
            this.copyToolStripMenuItem1});
            this.packetLogMenuStrip.Name = "packetLogMenuStrip";
            this.packetLogMenuStrip.ShowImageMargin = false;
            this.packetLogMenuStrip.Size = new System.Drawing.Size(140, 48);
            // 
            // addToSendListToolStripMenuItem
            // 
            this.addToSendListToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.addToSendListToolStripMenuItem.Name = "addToSendListToolStripMenuItem";
            this.addToSendListToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.addToSendListToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.addToSendListToolStripMenuItem.Text = "Add to Send List";
            this.addToSendListToolStripMenuItem.Click += new System.EventHandler(this.addToSendListToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem1
            // 
            this.copyToolStripMenuItem1.Name = "copyToolStripMenuItem1";
            this.copyToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+C";
            this.copyToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.copyToolStripMenuItem1.Text = "Copy";
            this.copyToolStripMenuItem1.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // loopCheckBox
            // 
            this.loopCheckBox.AutoSize = true;
            this.loopCheckBox.Location = new System.Drawing.Point(41, 494);
            this.loopCheckBox.Name = "loopCheckBox";
            this.loopCheckBox.Size = new System.Drawing.Size(50, 17);
            this.loopCheckBox.TabIndex = 9;
            this.loopCheckBox.Text = "Loop";
            this.loopCheckBox.UseVisualStyleBackColor = true;
            this.loopCheckBox.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pinCheckBox);
            this.groupBox2.Controls.Add(this.radioButton3);
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Location = new System.Drawing.Point(319, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(197, 58);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "View";
            // 
            // pinCheckBox
            // 
            this.pinCheckBox.AutoSize = true;
            this.pinCheckBox.Location = new System.Drawing.Point(6, 35);
            this.pinCheckBox.Name = "pinCheckBox";
            this.pinCheckBox.Size = new System.Drawing.Size(88, 17);
            this.pinCheckBox.TabIndex = 3;
            this.pinCheckBox.Text = "Keep on Top";
            this.pinCheckBox.UseVisualStyleBackColor = true;
            this.pinCheckBox.CheckedChanged += new System.EventHandler(this.pinCheckBox_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(147, 14);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(47, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Text = "Both";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(72, 14);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(69, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Send List";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 14);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(60, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Monitor";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Monitor:";
            // 
            // sendList
            // 
            this.sendList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sendList.CheckBoxes = true;
            this.sendList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.packetHeader,
            this.modeHeader,
            this.intervalHeader});
            this.sendList.FullRowSelect = true;
            this.sendList.GridLines = true;
            this.sendList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.sendList.HideSelection = false;
            this.sendList.Location = new System.Drawing.Point(12, 280);
            this.sendList.Name = "sendList";
            this.sendList.Size = new System.Drawing.Size(504, 198);
            this.sendList.TabIndex = 7;
            this.sendList.UseCompatibleStateImageBehavior = false;
            this.sendList.View = System.Windows.Forms.View.Details;
            this.sendList.Visible = false;
            this.sendList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.sendList_KeyDown);
            this.sendList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.sendList_MouseUp);
            // 
            // packetHeader
            // 
            this.packetHeader.Text = "Packet";
            this.packetHeader.Width = 353;
            // 
            // modeHeader
            // 
            this.modeHeader.Text = "Mode";
            // 
            // intervalHeader
            // 
            this.intervalHeader.Text = "Interval (ms)";
            this.intervalHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.intervalHeader.Width = 70;
            // 
            // namePacketButton
            // 
            this.namePacketButton.Location = new System.Drawing.Point(416, 490);
            this.namePacketButton.Name = "namePacketButton";
            this.namePacketButton.Size = new System.Drawing.Size(100, 23);
            this.namePacketButton.TabIndex = 20;
            this.namePacketButton.Text = "Show names";
            this.namePacketButton.UseVisualStyleBackColor = true;
            this.namePacketButton.Visible = false;
            this.namePacketButton.Click += new System.EventHandler(this.namePacketButton_Click);
            // 
            // unCheckAllButton
            // 
            this.unCheckAllButton.BackgroundImage = global::CuPAddIn.Properties.Resources.uncheck_all;
            this.unCheckAllButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.unCheckAllButton.Location = new System.Drawing.Point(329, 490);
            this.unCheckAllButton.Name = "unCheckAllButton";
            this.unCheckAllButton.Size = new System.Drawing.Size(23, 23);
            this.unCheckAllButton.TabIndex = 17;
            this.toolTip1.SetToolTip(this.unCheckAllButton, "Uncheck all packets");
            this.unCheckAllButton.UseVisualStyleBackColor = true;
            this.unCheckAllButton.Visible = false;
            this.unCheckAllButton.Click += new System.EventHandler(this.unCheckAllButton_Click);
            // 
            // checkAllButton
            // 
            this.checkAllButton.BackgroundImage = global::CuPAddIn.Properties.Resources.check_all;
            this.checkAllButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.checkAllButton.Location = new System.Drawing.Point(300, 490);
            this.checkAllButton.Name = "checkAllButton";
            this.checkAllButton.Size = new System.Drawing.Size(23, 23);
            this.checkAllButton.TabIndex = 16;
            this.toolTip1.SetToolTip(this.checkAllButton, "Check all packets");
            this.checkAllButton.UseVisualStyleBackColor = true;
            this.checkAllButton.Visible = false;
            this.checkAllButton.Click += new System.EventHandler(this.checkAllButton_Click);
            // 
            // saveFileButton
            // 
            this.saveFileButton.BackgroundImage = global::CuPAddIn.Properties.Resources.save;
            this.saveFileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.saveFileButton.Location = new System.Drawing.Point(184, 490);
            this.saveFileButton.Name = "saveFileButton";
            this.saveFileButton.Size = new System.Drawing.Size(23, 23);
            this.saveFileButton.TabIndex = 12;
            this.toolTip1.SetToolTip(this.saveFileButton, "Save file");
            this.saveFileButton.UseVisualStyleBackColor = true;
            this.saveFileButton.Visible = false;
            this.saveFileButton.Click += new System.EventHandler(this.saveFileButton_Click);
            // 
            // openFileButton
            // 
            this.openFileButton.BackgroundImage = global::CuPAddIn.Properties.Resources.open_file;
            this.openFileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.openFileButton.Location = new System.Drawing.Point(155, 490);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(23, 23);
            this.openFileButton.TabIndex = 11;
            this.toolTip1.SetToolTip(this.openFileButton, "Open file");
            this.openFileButton.UseVisualStyleBackColor = true;
            this.openFileButton.Visible = false;
            this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
            // 
            // editButton
            // 
            this.editButton.BackgroundImage = global::CuPAddIn.Properties.Resources.Tool;
            this.editButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.editButton.Location = new System.Drawing.Point(213, 490);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(23, 23);
            this.editButton.TabIndex = 13;
            this.toolTip1.SetToolTip(this.editButton, "Edit selected packets");
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Visible = false;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.BackgroundImage = global::CuPAddIn.Properties.Resources.Document_Delete;
            this.deleteButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.deleteButton.Location = new System.Drawing.Point(358, 490);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(23, 23);
            this.deleteButton.TabIndex = 18;
            this.toolTip1.SetToolTip(this.deleteButton, "Delete selected packets");
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Visible = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // eraseButton
            // 
            this.eraseButton.BackgroundImage = global::CuPAddIn.Properties.Resources.bin;
            this.eraseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.eraseButton.Location = new System.Drawing.Point(387, 490);
            this.eraseButton.Name = "eraseButton";
            this.eraseButton.Size = new System.Drawing.Size(23, 23);
            this.eraseButton.TabIndex = 19;
            this.toolTip1.SetToolTip(this.eraseButton, "Erase the list");
            this.eraseButton.UseVisualStyleBackColor = true;
            this.eraseButton.Visible = false;
            this.eraseButton.Click += new System.EventHandler(this.eraseButton_Click);
            // 
            // downButton
            // 
            this.downButton.BackgroundImage = global::CuPAddIn.Properties.Resources.down;
            this.downButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.downButton.Location = new System.Drawing.Point(271, 490);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(23, 23);
            this.downButton.TabIndex = 15;
            this.toolTip1.SetToolTip(this.downButton, "Move selected packets down");
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Visible = false;
            this.downButton.Click += new System.EventHandler(this.downButton_Click);
            // 
            // upButton
            // 
            this.upButton.BackgroundImage = global::CuPAddIn.Properties.Resources.up;
            this.upButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.upButton.Location = new System.Drawing.Point(242, 490);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(23, 23);
            this.upButton.TabIndex = 14;
            this.toolTip1.SetToolTip(this.upButton, "Move selected packets up");
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Visible = false;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // startStopButton
            // 
            this.startStopButton.BackgroundImage = global::CuPAddIn.Properties.Resources.start;
            this.startStopButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.startStopButton.Location = new System.Drawing.Point(12, 490);
            this.startStopButton.Name = "startStopButton";
            this.startStopButton.Size = new System.Drawing.Size(23, 23);
            this.startStopButton.TabIndex = 8;
            this.toolTip1.SetToolTip(this.startStopButton, "Start");
            this.startStopButton.UseVisualStyleBackColor = true;
            this.startStopButton.Visible = false;
            this.startStopButton.Click += new System.EventHandler(this.startStopButton_Click);
            // 
            // newPacketButton
            // 
            this.newPacketButton.BackgroundImage = global::CuPAddIn.Properties.Resources._new;
            this.newPacketButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.newPacketButton.Location = new System.Drawing.Point(126, 490);
            this.newPacketButton.Name = "newPacketButton";
            this.newPacketButton.Size = new System.Drawing.Size(23, 23);
            this.newPacketButton.TabIndex = 10;
            this.toolTip1.SetToolTip(this.newPacketButton, "New packet");
            this.newPacketButton.UseVisualStyleBackColor = true;
            this.newPacketButton.Visible = false;
            this.newPacketButton.Click += new System.EventHandler(this.newPacketButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 523);
            this.Controls.Add(this.namePacketButton);
            this.Controls.Add(this.unCheckAllButton);
            this.Controls.Add(this.checkAllButton);
            this.Controls.Add(this.saveFileButton);
            this.Controls.Add(this.openFileButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.eraseButton);
            this.Controls.Add(this.sendList);
            this.Controls.Add(this.downButton);
            this.Controls.Add(this.upButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.startStopButton);
            this.Controls.Add(this.newPacketButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.loopCheckBox);
            this.Controls.Add(this.packetLog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Packet Tools";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.sendListMenuStrip.ResumeLayout(false);
            this.packetLogMenuStrip.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ContextMenuStrip sendListMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendReceiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button newPacketButton;
        private System.Windows.Forms.ListView packetLog;
        private System.Windows.Forms.ColumnHeader columnHeader;
        private System.Windows.Forms.ContextMenuStrip packetLogMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem addToSendListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem1;
        private System.Windows.Forms.Button startStopButton;
        private System.Windows.Forms.CheckBox loopCheckBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.Button downButton;
        private System.Windows.Forms.ListView sendList;
        private System.Windows.Forms.ColumnHeader packetHeader;
        private System.Windows.Forms.ColumnHeader modeHeader;
        private System.Windows.Forms.ColumnHeader intervalHeader;
        private System.Windows.Forms.Button eraseButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.Button saveFileButton;
        private System.Windows.Forms.Button checkAllButton;
        private System.Windows.Forms.Button unCheckAllButton;
        private System.Windows.Forms.Button namePacketButton;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox pinCheckBox;
    }
}