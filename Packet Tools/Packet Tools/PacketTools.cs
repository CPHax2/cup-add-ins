﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CuPAddIn
{
    class PacketTools
    {
        public static System.Timers.Timer timer = new System.Timers.Timer();
        public static List<ListViewItem> packetClipboard = new List<ListViewItem>();

        public static void copyPackets(List<ListViewItem> packetList)
        {
            packetClipboard.Clear();
            for (int i = 0; i < packetList.Count; i++)
            {
                packetClipboard.Add((ListViewItem)packetList[i].Clone());
            }
        }

        public class PacketType
        {
            public string name { get; set; }
            public string[] packetFormatting { get; set; }
            public string usedIdType { get; set; }
            public bool isUsingCoordinates { get; set; }
            public bool displayWarning { get; set; }

            public override string ToString()
            {
                return name;
            }
        }

        public static PacketType[] defaultPacketList = new PacketType[] 
        { 
            new PacketType 
            { 
                name = "Teleport",
                usedIdType = "Room",
                isUsingCoordinates = true,
                packetFormatting = new string[] { "%xt%s%j#jr%-1%", "", "%", "", "%", "", "%" },
                displayWarning = false
            }, 
            new PacketType 
            {
                name = "Send Move",
                usedIdType = null,
                isUsingCoordinates = true,
                packetFormatting = new string[] { "%xt%s%u#sp%-1%", "", "%", "", "%" },
                displayWarning = false 
            }, 
            new PacketType 
            {
                name = "Send Safe Message",
                usedIdType = "Safe Message",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%u#ss%-1%", "", "%" },
                displayWarning = false
            }, 
            new PacketType 
            {
                name = "Send Emote",
                usedIdType = "Emote",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%u#se%-1%", "", "%" },
                displayWarning = false
            },
            new PacketType
            {
                name = "Send Frame",
                usedIdType = "Frame",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%u#sf%-1%", "", "%" },
                displayWarning = false
            },
            new PacketType
            {
                name = "Throw Snowball",
                usedIdType = null,
                isUsingCoordinates = true,
                packetFormatting = new string[] { "%xt%s%u#sb%-1%", "", "%", "", "%" },
                displayWarning = false
            },
            new PacketType
            {
                name = "Check Player Location",
                usedIdType = "Player",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%u#bf%-1%", "", "%" },
                displayWarning = false
            },
            new PacketType
            {
                name = "Add Item",
                usedIdType = "Item",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%i#ai%-1%", "", "%" },
                displayWarning = true
            },
            new PacketType
            {
                name = "Add Furniture",
                usedIdType = "Furniture",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%g#af%-1%", "", "%" },
                displayWarning = true
            },
            new PacketType
            {
                name = "Add Igloo",
                usedIdType = "Igloo",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%g#au%-1%", "", "%" },
                displayWarning = true
            },
            new PacketType
            {
                name = "Add Floor",
                usedIdType = "Floor",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%g#ag%-1%", "", "%" },
                displayWarning = false
            },
            new PacketType
            {
                name = "Add Location",
                usedIdType = "Location",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%g#aloc%-1%", "", "%" },
                displayWarning = false
            },
            new PacketType
            {
                name = "Add Puffle Item",
                usedIdType = "Puffle Item",
                isUsingCoordinates = false,
                packetFormatting = new string[]{ "%xt%s%p#papi%-1%", "", "%" },
                displayWarning = false
            },
            new PacketType
            {
                name = "Add Stamp",
                usedIdType = "Stamp",
                isUsingCoordinates = false,
                packetFormatting = new string[] { "%xt%s%st#sse%-1%", "", "%" },
                displayWarning = true
            }
        };

        public static void openFile()
        {
            OpenFileDialog dialog = new OpenFileDialog { Filter = "All Packet List Files|*.ptsl;*.spt|Packet Tools Send List (*.ptsl)|*.ptsl|WPE Pro Send Packet (*.spt)|*.spt" };
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                AddInBase.form.BeginInvoke((MethodInvoker)delegate { AddInBase.form.Enabled = false; });
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if (!AddInBase.form.isShowingNames) AddInBase.form.switchNamePacketDisplay();
                    string path = dialog.FileName;
                    if (path.Length > 3 && path.Substring(path.Length - 4, 4) == ".spt")
                    {
                        byte[] spt = System.IO.File.ReadAllBytes(path);
                        int packetsCount = BitConverter.ToInt32(spt, 0);
                        ListViewItem[] packetList = new ListViewItem[packetsCount];
                        int pos = 4;
                        for (int i = 0; i < packetsCount; i++)
                        {
                            int nameLength = BitConverter.ToInt32(spt, pos);
                            pos += 4;
                            string name = System.Text.Encoding.Default.GetString(spt, pos, nameLength);
                            pos += nameLength;
                            int packetLength = BitConverter.ToInt32(spt, pos);
                            pos += 4;
                            string packet = System.Text.Encoding.Default.GetString(spt, pos, packetLength);
                            pos += packetLength;
                            ListViewItem newPacket = new ListViewItem { SubItems = { "", "", "" } };
                            newPacket.SubItems[0].Text = name;
                            newPacket.SubItems[0].Tag = packet;
                            newPacket.SubItems[1].Text = "Send";
                            newPacket.SubItems[2].Text = "1000";
                            newPacket.SubItems[2].Tag = 1000;
                            packetList[i] = newPacket;
                        }
                        AddInBase.form.loadPackets(packetList);
                    }
                    else
                    {
                        byte[] file = System.IO.File.ReadAllBytes(path);
                        int packetsCount = BitConverter.ToInt32(file, 0);
                        ListViewItem[] packetList = new ListViewItem[packetsCount];
                        int pos = 4;
                        for (int i = 0; i < packetsCount; i++)
                        {
                            int nameLength = BitConverter.ToInt32(file, pos);
                            pos += 4;
                            string name = System.Text.Encoding.Default.GetString(file, pos, nameLength);
                            pos += nameLength;
                            int packetLength = BitConverter.ToInt32(file, pos);
                            pos += 4;
                            string packet = System.Text.Encoding.Default.GetString(file, pos, packetLength);
                            pos += packetLength;
                            int interval = BitConverter.ToInt32(file, pos);
                            pos += 4;
                            bool isSend = BitConverter.ToBoolean(file, pos);
                            pos += 1;
                            ListViewItem newPacket = new ListViewItem { SubItems = { "", "", "" } };
                            newPacket.SubItems[0].Text = name;
                            newPacket.SubItems[0].Tag = packet;
                            newPacket.SubItems[1].Text = isSend ? "Send" : "Receive";
                            newPacket.SubItems[2].Text = interval.ToString();
                            newPacket.SubItems[2].Tag = interval;
                            packetList[i] = newPacket;
                        }
                        AddInBase.form.loadPackets(packetList);
                    }
                }
                AddInBase.form.BeginInvoke((MethodInvoker)delegate { AddInBase.form.Enabled = true; });
            }));
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
        }

        public static void saveFile(ListViewItem[] packetList)
        {
            SaveFileDialog dialog = new SaveFileDialog { Filter = "Packet Tools Send List (*.ptsl)|*.ptsl|WPE Pro Send Packet (*.spt)|*.spt" };
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                AddInBase.form.BeginInvoke((MethodInvoker)delegate { AddInBase.form.Enabled = false; });
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string path = dialog.FileName;
                    if (dialog.FilterIndex == 2)
                    {
                        bool dataLoss = false;
                        foreach (ListViewItem item in packetList)
                        {
                            if ((int)item.SubItems[2].Tag != 1000 || item.SubItems[1].Text != "Send")
                            {
                                dataLoss = true;
                                break;
                            }
                        }
                        if (dataLoss && MessageBox.Show("Some packets in the Send List contain data that can not be saved in *.spt file format. "
                        + "If you continue, that information will be lost.", "Warning",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) != DialogResult.OK)
                        {
                            AddInBase.form.Enabled = true;
                            return;
                        }
                        int fileLength = 4 + packetList.Length * 8;
                        foreach (ListViewItem packet in packetList)
                        {
                            fileLength += packet.SubItems[0].Text.Length;
                            fileLength += ((string)(packet.SubItems[0].Tag)).Length;
                        }
                        byte[] file = new byte[fileLength];
                        byte[] numberOfPackets = BitConverter.GetBytes(packetList.Length);
                        System.Buffer.BlockCopy(numberOfPackets, 0, file, 0, 4);
                        int pos = 4;
                        foreach (ListViewItem packet in packetList)
                        {
                            string packetName, packetString;
                            if (AddInBase.form.isShowingNames)
                            {
                                packetName = packet.SubItems[0].Text;
                                packetString = (string)packet.SubItems[0].Tag;
                            }
                            else
                            {
                                packetName = (string)packet.SubItems[0].Tag;
                                packetString = packet.SubItems[0].Text;
                            }
                            byte[] nameLength = BitConverter.GetBytes(packetName.Length);
                            byte[] name = System.Text.Encoding.Default.GetBytes(packetName);
                            byte[] packetLength = BitConverter.GetBytes(packetString.Length);
                            byte[] Packet = System.Text.Encoding.Default.GetBytes(packetString);
                            System.Buffer.BlockCopy(nameLength, 0, file, pos, 4);
                            pos += 4;
                            System.Buffer.BlockCopy(name, 0, file, pos, name.Length);
                            pos += name.Length;
                            System.Buffer.BlockCopy(packetLength, 0, file, pos, 4);
                            pos += 4;
                            System.Buffer.BlockCopy(Packet, 0, file, pos, Packet.Length);
                            pos += Packet.Length;
                        }
                        System.IO.File.WriteAllBytes(path, file);
                    }
                    else
                    {
                        int fileLength = 4 + packetList.Length * 13;
                        foreach (ListViewItem packet in packetList)
                        {
                            fileLength += packet.SubItems[0].Text.Length;
                            fileLength += ((string)(packet.SubItems[0].Tag)).Length;
                        }
                        byte[] file = new byte[fileLength];
                        byte[] numberOfPackets = BitConverter.GetBytes(packetList.Length);
                        System.Buffer.BlockCopy(numberOfPackets, 0, file, 0, 4);
                        int pos = 4;
                        foreach (ListViewItem packet in packetList)
                        {
                            string packetName, packetString;
                            if (AddInBase.form.isShowingNames)
                            {
                                packetName = packet.SubItems[0].Text;
                                packetString = (string)packet.SubItems[0].Tag;
                            }
                            else
                            {
                                packetName = (string)packet.SubItems[0].Tag;
                                packetString = packet.SubItems[0].Text;
                            }
                            bool isSend = packet.SubItems[1].Text == "Send" ? true : false;
                            int packetInterval = (int)packet.SubItems[2].Tag;
                            byte[] nameLength = BitConverter.GetBytes(packetName.Length);
                            byte[] name = System.Text.Encoding.Default.GetBytes(packetName);
                            byte[] packetLength = BitConverter.GetBytes(packetString.Length);
                            byte[] Packet = System.Text.Encoding.Default.GetBytes(packetString);
                            byte[] interval = BitConverter.GetBytes(packetInterval);
                            byte[] mode = BitConverter.GetBytes(isSend);
                            System.Buffer.BlockCopy(nameLength, 0, file, pos, 4);
                            pos += 4;
                            System.Buffer.BlockCopy(name, 0, file, pos, name.Length);
                            pos += name.Length;
                            System.Buffer.BlockCopy(packetLength, 0, file, pos, 4);
                            pos += 4;
                            System.Buffer.BlockCopy(Packet, 0, file, pos, Packet.Length);
                            pos += Packet.Length;
                            System.Buffer.BlockCopy(interval, 0, file, pos, 4);
                            pos += 4;
                            System.Buffer.BlockCopy(mode, 0, file, pos, 1);
                            pos += 1;
                        }
                        System.IO.File.WriteAllBytes(path, file);
                    }
                }
                AddInBase.form.BeginInvoke((MethodInvoker)delegate { AddInBase.form.Enabled = true; });
            }));
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
        }
    }
}
