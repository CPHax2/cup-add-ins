﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class NewPacketForm : Cup.Extensibility.Library.CuPForm
    {
        public NewPacketForm()
        {
            InitializeComponent();
        }
        
        private void Form3_Load(object sender, EventArgs e)
        {
            foreach (PacketTools.PacketType type in PacketTools.defaultPacketList)
            {
                comboBox1.Items.Add(type);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void showArgs()
        {
            PacketTools.PacketType type = (PacketTools.PacketType)comboBox1.SelectedItem;
            if (type.usedIdType != null)
            {
                idLabel.Text = type.usedIdType + " ID:";
                idTextBox.Location = new Point(idLabel.Location.X + idLabel.Width + 6, idTextBox.Location.Y);
                idLabel.Visible = true;
                idTextBox.Visible = true;
            }
            else
            {
                idLabel.Visible = false;
                idTextBox.Visible = false;
            }
            if (type.isUsingCoordinates)
            {
                if (type.usedIdType == null) xLabel.Location = new Point(12, xLabel.Location.Y);
                else xLabel.Location = new Point(idTextBox.Location.X + idTextBox.Width + 6, xLabel.Location.Y);
                xTextBox.Location = new Point(xLabel.Location.X + xLabel.Width + 6, xTextBox.Location.Y);
                yLabel.Location = new Point(xTextBox.Location.X + xTextBox.Width + 6, yLabel.Location.Y);
                yTextBox.Location = new Point(yLabel.Location.X + yLabel.Width + 6, yTextBox.Location.Y);
                xLabel.Visible = true;
                xTextBox.Visible = true;
                yLabel.Visible = true;
                yTextBox.Visible = true;
            }
            else
            {
                xLabel.Visible = false;
                xTextBox.Visible = false;
                yLabel.Visible = false;
                yTextBox.Visible = false;
            }
            if (((PacketTools.PacketType)(comboBox1.SelectedItem)).displayWarning)
            {
                warningLabel.Text = "Warning: Using this packet with wrong ID may result in a ban!";
                warningLabel.Location = new Point(warningLabel.Location.X, idTextBox.Location.Y + idTextBox.Height + 3);
                warningLabel.Visible = true;
                label2.Location = new Point(label2.Location.X, warningLabel.Location.Y + warningLabel.Height + 3);
            }
            else
            {
                warningLabel.Visible = false;
                label2.Location = new Point(label2.Location.X, idLabel.Location.Y + idLabel.Height + 3);
            }
            packetTextBox.Location = new Point(packetTextBox.Location.X, label2.Location.Y + label2.Height + 1);
            groupBox1.Location = new Point(groupBox1.Location.X, packetTextBox.Location.Y + packetTextBox.Height + 6);
            label3.Location = new Point(label3.Location.X, groupBox1.Location.Y + 18);
            intervalUpDown.Location = new Point(intervalUpDown.Location.X, label3.Location.Y - 2);
            sendReceiveButton.Location = new Point(sendReceiveButton.Location.X, groupBox1.Location.Y + groupBox1.Height + 6);
            addButton.Location = new Point(addButton.Location.X, sendReceiveButton.Location.Y);
            closeButton.Location = new Point(closeButton.Location.X, addButton.Location.Y);
            this.Height = sendReceiveButton.Location.Y + sendReceiveButton.Height + 40;
            idTextBox.Text = "0";
            xTextBox.Text = "0";
            yTextBox.Text = "0";
            updatePacketString();
        }

        private void hideArgs()
        {            
            idLabel.Visible = false;
            idTextBox.Visible = false;
            xLabel.Visible = false;
            xTextBox.Visible = false;
            yLabel.Visible = false;
            yTextBox.Visible = false;
            warningLabel.Text = "Warning: Sending some packets like ones trying to add \'bait\'\r\nitems may ban you!";
            warningLabel.Location = new Point(warningLabel.Location.X, comboBox1.Location.Y + comboBox1.Height + 9);
            warningLabel.Visible = true;
            label2.Location = new Point(label2.Location.X, warningLabel.Location.Y + warningLabel.Height + 3);
            packetTextBox.Location = new Point(packetTextBox.Location.X, label2.Location.Y + label2.Height + 1);
            groupBox1.Location = new Point(groupBox1.Location.X, packetTextBox.Location.Y + packetTextBox.Height + 6);
            label3.Location = new Point(label3.Location.X, groupBox1.Location.Y + 18);
            intervalUpDown.Location = new Point(intervalUpDown.Location.X, label3.Location.Y - 2);
            sendReceiveButton.Location = new Point(sendReceiveButton.Location.X, groupBox1.Location.Y + groupBox1.Height + 6);
            addButton.Location = new Point(addButton.Location.X, sendReceiveButton.Location.Y);
            closeButton.Location = new Point(closeButton.Location.X, addButton.Location.Y);
            this.Height = sendReceiveButton.Location.Y + sendReceiveButton.Height + 40;
        }
        
        private void updatePacketString()
        {
            PacketTools.PacketType type = (PacketTools.PacketType)comboBox1.SelectedItem;
            List<string> args = new List<string>();
            if (type.usedIdType != null) args.Add(idTextBox.Text);
            if (type.isUsingCoordinates)
            {
                args.Add(xTextBox.Text);
                args.Add(yTextBox.Text);
            }
            string packet = "";
            int usedArgs = 0;
            foreach (string s in type.packetFormatting)
            {
                packet += s != "" ? s : args[usedArgs++];
            }
            packetTextBox.Text = packet;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                checkBox1.Enabled = true;
                sendReceiveButton.Text = "Send";
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                checkBox1.Enabled = false;
                checkBox1.Checked = false;
                sendReceiveButton.Text = "Receive";
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                comboBox1.Enabled = true;
                packetTextBox.Enabled = false;
                showArgs();
            }
            else
            {
                comboBox1.Enabled = false;
                packetTextBox.Enabled = true;
                hideArgs();
            }
        }

        private void sendReceiveButton_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked) Core.SendPacketAsync(packetTextBox.Text, null);
            else Core.ReceivePacketAsync(packetTextBox.Text, null);
        }
        
        private void addButton_Click(object sender, EventArgs e)
        {
            ListViewItem packet = new ListViewItem { SubItems = { "", "", "" } };
            if (AddInBase.form.isShowingNames)
            {
                packet.SubItems[0].Text = nameTextBox.Text;
                packet.SubItems[0].Tag = packetTextBox.Text;
            }
            else
            {
                packet.SubItems[0].Tag = nameTextBox.Text;
                packet.SubItems[0].Text = packetTextBox.Text;
            }
            if (radioButton1.Checked) packet.SubItems[1].Text = "Send";
            else packet.SubItems[1].Text = "Receive";
            packet.SubItems[2].Text = ((int)intervalUpDown.Value).ToString();
            packet.SubItems[2].Tag = (int)intervalUpDown.Value;
            AddInBase.form.loadPackets(new ListViewItem[1] { packet });
            this.Close();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) showArgs();
        }
        
        private void idTextBox_TextChanged(object sender, EventArgs e)
        {
            updatePacketString();
        }

        private void xTextBox_TextChanged(object sender, EventArgs e)
        {
            updatePacketString();
        }

        private void yTextBox_TextChanged(object sender, EventArgs e)
        {
            updatePacketString();
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
        
    }
}
