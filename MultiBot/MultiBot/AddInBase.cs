﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.AddIn;
using System.Windows.Forms;
using Cup.Extensibility;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    [AddIn("CuPAddIn")]
    public class AddInBase : ICuPAddIn
    {
        private static bool started = false;
        public static MainForm form;

        public void OnBoot()
        {
            
        }

        public void OnExit()
        {
            if (form != null)
            {
                AuxSWF.Unload();
                form.Close();
                form.Dispose();
            }
            started = false;
        }

        public void OnStart()
        {
            if (!started)
            {
                form = new MainForm();
                ApiPipeline.Hook();
                Items.LoadItemList();
                AuxSWF.Load();
                Core.ExecuteFunction("AuxSWF.init");
                MultiBot.Init();
                started = true;
            }
            else
            {
                form.ShowDialog();
            }
        }
    }
}
