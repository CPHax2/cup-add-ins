﻿var SHELL;
var INTERFACE;
var AIRTOWER;
var CROSSHAIR;
var myPlayer;

function init()
{
	SHELL = _global.getCurrentShell();
	INTERFACE = _global.getCurrentInterface();
	AIRTOWER = _global.getCurrentAirtower();
	CROSSHAIR = INTERFACE.CROSSHAIR;
}

function activateCrosshair()
{
	CROSSHAIR._visible = true;
	CROSSHAIR.startDrag(true, 20, 20, 740, 440);
	CROSSHAIR._x = this._xmouse;
	CROSSHAIR._y = this._ymouse;
	CROSSHAIR.target_btn.onRelease = _global.mx.utils.Delegate.create(this, crosshairRelease);
	INTERFACE.snowballCrosshairShown.dispatch();
}

function hideCrosshair()
{
	stopDrag();
	CROSSHAIR._y = -100;
	CROSSHAIR._visible = false;
	Selection.setFocus(null);
}

function crosshairRelease()
{
	var target_x = Math.round(CROSSHAIR._x);
	var target_y = Math.round(CROSSHAIR._y);
	hideCrosshair();
	var player = SHELL.getMyPlayerObject();
	target_x = target_x - player.x;
	target_y = target_y - player.y;
	sendToAddIn(target_x + "%" + target_y);
}

function setPlayerLocationPromptsEnabled(enabled)
{
	if (enabled)
	{
		AIRTOWER.addListener(AIRTOWER.FIND_BUDDY, SHELL.handleGetPlayerLocationById);
	}
	else
	{
		AIRTOWER.removeListener(AIRTOWER.FIND_BUDDY, SHELL.handleGetPlayerLocationById);
	}
}