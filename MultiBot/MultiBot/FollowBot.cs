﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CuPAddIn
{
    public class FollowBot : PCL
    {
        public static string TargetID { get; set; }
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }

        private bool copyMove = false, copyFrame = false, copySnowball = false, copyEmote = false;

        public FollowBot(string name, string password, bool isHash, int x, int y) : base(name, password, isHash)
        {
            OffsetX = x;
            OffsetY = y;
        }

        public void Connect(Server server)
        {
            if (server == null || Name.ToLower() == MultiBot.LocalPlayerUsername) return;
            OnJoin = () => { StartFollow(); };
            Connect(server.IP, server.Port);
        }

        public void Connect()
        {
            Connect(Servers.CurrentServer);
        }

        public void StartFollow()
        {
            OnPacket("bf", (packet) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[6] == TargetID)
                {
                    JoinRoom(Packet[4]);
                }
            });
            OnPacket("jr", (packet) =>
            {
                string[] Packet = packet.Split('%');
                bool isPlayerInRoom = false;
                for (int i = 5; i < Packet.Length - 1; i++)
                {
                    if (Packet[i].Split('|')[0] == TargetID)
                    {
                        isPlayerInRoom = true;
                        break;
                    }
                }
                if (!isPlayerInRoom) CheckPlayerLocation(TargetID);
            });
            OnPacket("rp", (packet) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == TargetID) CheckPlayerLocation(TargetID);
            });
            CheckPlayerLocation(TargetID);
            CopyMove = AddInBase.form.CopyMove;
            CopyFrame = AddInBase.form.CopyFrame;
            CopySnowball = AddInBase.form.CopySnowball;
            CopyEmote = AddInBase.form.CopyEmote;
            CopyMessage = AddInBase.form.CopyMessage;
        }

        public void StopFollow()
        {
            Off("bf");
            Off("jr");
            Off("rp");
            CopyMove = false;
            CopyFrame = false;
            CopySnowball = false;
            CopyEmote = false;
            CopyMessage = false;
        }

        public bool CopyMove
        {
            get { return copyMove; }
            set
            {
                copyMove = value;
                if (value)
                {
                    OnPacket("sp", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID)
                        {
                            int X, Y;
                            int.TryParse(Packet[5], out X);
                            int.TryParse(Packet[6], out Y);
                            Move(X + OffsetX, Y + OffsetY);
                        }
                    });
                    OnPacket("followpath", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) FollowPath(Packet[5]);
                    });
                    OnPacket("tp", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID)
                        {
                            int X, Y;
                            int.TryParse(Packet[5], out X);
                            int.TryParse(Packet[6], out Y);
                            Teleport(X + OffsetX, Y + OffsetY);
                        }
                    });
                }
                else
                {
                    Off("sp");
                    Off("followpath");
                    Off("tp");
                }
            }
        }

        public bool CopyFrame
        {
            get { return copyFrame; }
            set
            {
                copyFrame = value;
                if (value)
                {
                    OnPacket("sf", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) SendFrame(Packet[5]);
                    });
                    OnPacket("sa", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) SendFrame(Packet[5]);
                    });
                }
                else
                {
                    Off("sf");
                    Off("sa");
                }
            }
        }

        public bool CopySnowball
        {
            get { return copySnowball; }
            set
            {
                copySnowball = value;
                if (value)
                {
                    OnPacket("sb", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) ThrowSnowball(Packet[5], Packet[6]);
                    });
                }
                else Off("sb");
            }
        }

        public bool CopyEmote
        {
            get { return copyEmote; }
            set
            {
                copyEmote = value;
                if (value)
                {
                    OnPacket("se", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) SendEmote(Packet[5]);
                    });
                }
                else
                {
                    Off("se");
                }
            }
        }

        public bool CopyMessage
        {
            set
            {
                if (value)
                {
                    OnPacket("sm", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) SendPhraseChatMessageByText(Packet[5]);
                    });
                    OnPacket("ss", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) SendSafeMessage(Packet[5]);
                    });
                    OnPacket("sj", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) SendJoke(Packet[5]);
                    });
                    OnPacket("sg", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) SendTourGuideMessage(Packet[5]);
                    });
                    OnPacket("sc", (packet) =>
                    {
                        string[] Packet = packet.Split('%');
                        if (Packet[4] == TargetID) SendMessage(Packet[5]);
                    });
                }
                else
                {
                    Off("sm");
                    Off("ss");
                    Off("sj");
                    Off("sg");
                    Off("sc");
                }
            }
        }
    }
}
