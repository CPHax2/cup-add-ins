﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public class Server
    {
        public readonly int ID;
        public readonly string Name;
        public readonly string IP;
        public readonly int Port;

        public Server(int id, string name, string ip, int port)
        {
            ID = id;
            Name = name;
            IP = ip;
            Port = port;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public static class Servers
    {
        public static string CurrentLang
        {
            get { return Core.ExecuteFunction("SHELL.getLanguageAbbreviation").RawResult; }
        }

        public static List<Server> CurrentLangServers
        {
            get
            {
                switch (CurrentLang)
                {
                    case "en":
                        return ENservers;
                    case "pt":
                        return PTservers;
                    case "fr":
                        return FRservers;
                    case "es":
                        return ESservers;
                    default:
                        return null;
                }
            }
        }

        public static Server CurrentServer
        {
            get
            {
                List<Server> servers = CurrentLangServers;
                if (servers == null) return null;
                int id;
                if (!int.TryParse(Core.GetVar("SHELL.current_world_obj.id").RawResult, out id)) return null;
                foreach (Server server in servers)
                {
                    if (server.ID == id) return server;
                }
                return null;
            }
        }

        public static List<Server> ENservers = new List<Server>
        {
            new Server(3132, "Abominable", "204.75.167.19", 3724),
            new Server(3708, "Adventure (SC)", "204.75.167.58", 3724),
            new Server(3135, "Alaska", "204.75.167.20", 3724),
            new Server(3111, "Alpine", "204.75.167.12", 9875),
            new Server(3165, "Altitude (SC)", "204.75.167.53", 6112),
            new Server(3552, "Antarctic (SC)", "204.75.167.57", 6112),
            new Server(3157, "Arctic", "204.75.167.26", 9875),
            new Server(3170, "Ascent", "204.75.167.30", 9875),
            new Server(3202, "Aurora", "204.75.167.32", 3724),
            new Server(3121, "Avalanche", "204.75.167.15", 6112),
            new Server(3709, "Beanbag (SC)", "204.75.167.58", 6112),
            new Server(3406, "Beanie", "204.75.167.42", 6112),
            new Server(3314, "Belly Slide", "204.75.167.40", 3724),
            new Server(3183, "Below Zero", "204.75.167.47", 3724),
            new Server(3172, "Berg", "204.75.167.47", 6112),
            new Server(3152, "Big Foot", "204.75.167.25", 9875),
            new Server(3410, "Big Snow", "204.75.167.43", 6112),
            new Server(3401, "Big Surf", "204.75.167.41", 3724),
            new Server(3100, "Blizzard", "204.75.167.9", 3724),
            new Server(3160, "Bobsled", "204.75.167.27", 9875),
            new Server(3408, "Bonza (SC)", "204.75.167.55", 6112),
            new Server(3309, "Boots (SC)", "204.75.167.54", 9875),
            new Server(3310, "Breeze", "204.75.167.38", 9875),
            new Server(3411, "Brumby", "204.75.167.43", 9875),
            new Server(3154, "Bubblegum (SC)", "204.75.167.53", 3724),
            new Server(3162, "Bunny Hill", "204.75.167.11", 9875),
            new Server(3177, "Cabin", "204.75.167.49", 3724),
            new Server(3208, "Canoe (SC)", "204.75.167.53", 9875),
            new Server(3215, "Caribou", "204.75.167.36", 3724),
            new Server(3209, "Chinook", "204.75.167.34", 3724),
            new Server(3146, "Christmas", "204.75.167.23", 9875),
            new Server(3179, "Cloudy", "204.75.167.49", 9875),
            new Server(3301, "Cold Front", "204.75.167.36", 9875),
            new Server(3181, "Cold Snap", "204.75.167.46", 6112),
            new Server(3173, "Cozy", "204.75.167.47", 9875),
            new Server(3710, "Cream Soda (SC)", "204.75.167.58", 9875),
            new Server(3311, "Crunch", "204.75.167.39", 3724),
            new Server(3128, "Crystal", "204.75.167.17", 9875),
            new Server(3300, "Deep Freeze", "204.75.167.36", 6112),
            new Server(3164, "Deep Snow", "204.75.167.29", 3724),
            new Server(3405, "Down Under", "204.75.167.42", 3724),
            new Server(3680, "Downhill (SC)", "204.75.167.44", 3724),
            new Server(3513, "Dry Ice (SC)", "204.75.167.56", 3724),
            new Server(3681, "Elevation (SC)", "204.75.167.44", 6112),
            new Server(3711, "Fiesta (SC)", "204.75.167.59", 3724),
            new Server(3151, "Fjord", "204.75.167.25", 6112),
            new Server(3124, "Flippers", "204.75.167.16", 6112),
            new Server(3104, "Flurry", "204.75.167.12", 6112),
            new Server(3178, "Fog", "204.75.167.49", 6112),
            new Server(3120, "Freezer", "204.75.167.15", 3724),
            new Server(3107, "Frostbite", "204.75.167.15", 9875),
            new Server(3304, "Frosty", "204.75.167.37", 9875),
            new Server(3302, "Frozen", "204.75.167.37", 3724),
            new Server(3654, "Glacial (SC)", "204.75.167.56", 9875),
            new Server(3201, "Glacier", "204.75.167.10", 3724),
            new Server(3712, "Grasshopper (SC)", "204.75.167.59", 6112),
            new Server(3204, "Great White", "204.75.167.32", 9875),
            new Server(3116, "Grizzly", "204.75.167.14", 6112),
            new Server(3133, "Half Pipe", "204.75.167.19", 6112),
            new Server(3143, "Hibernate", "204.75.167.22", 9875),
            new Server(3720, "Hockey", "204.75.167.50", 3724),
            new Server(3713, "Hot Chocolate (SC)", "204.75.167.59", 9875),
            new Server(3138, "Husky", "204.75.167.21", 3724),
            new Server(3724, "Hypothermia", "204.75.167.51", 6112),
            new Server(3140, "Ice Age", "204.75.167.21", 9875),
            new Server(3101, "Ice Berg", "204.75.167.22", 3724),
            new Server(3161, "Ice Box", "204.75.167.28", 3724),
            new Server(3112, "Ice Breaker", "204.75.167.13", 3724),
            new Server(3308, "Ice Cave (SC)", "204.75.167.54", 6112),
            new Server(3127, "Ice Cold", "204.75.167.17", 6112),
            new Server(3153, "Ice Cream (SC)", "204.75.167.52", 9875),
            new Server(3402, "Ice Cube (SC)", "204.75.167.55", 3724),
            new Server(3119, "Ice Pack (SC)", "204.75.167.52", 6112),
            new Server(3130, "Ice Palace", "204.75.167.18", 6112),
            new Server(3213, "Ice Pond", "204.75.167.11", 3724),
            new Server(3212, "Ice Rink (SC)", "204.75.167.54", 3724),
            new Server(3169, "Ice Shelf", "204.75.167.30", 6112),
            new Server(3148, "Icebound", "204.75.167.24", 6112),
            new Server(3313, "Iceland", "204.75.167.39", 9875),
            new Server(3108, "Icicle", "204.75.167.28", 6112),
            new Server(3721, "Jack Frost", "204.75.167.50", 6112),
            new Server(3714, "Jackhammer (SC)", "204.75.167.60", 3724),
            new Server(3147, "Klondike", "204.75.167.24", 3724),
            new Server(3404, "Kosciuszko", "204.75.167.41", 9875),
            new Server(3115, "Mammoth", "204.75.167.14", 3724),
            new Server(3149, "Marshmallow", "204.75.167.24", 9875),
            new Server(3159, "Matterhorn", "204.75.167.27", 6112),
            new Server(3715, "Migrator (SC)", "204.75.167.60", 6112),
            new Server(3683, "Misty (SC)", "204.75.167.45", 3724),
            new Server(3307, "Mittens", "204.75.167.38", 6112),
            new Server(3176, "Mountain", "204.75.167.48", 9875),
            new Server(3203, "Mukluk", "204.75.167.32", 6112),
            new Server(3716, "Mullet (SC)", "204.75.167.60", 9875),
            new Server(3200, "North Pole", "204.75.167.31", 6112),
            new Server(3167, "Northern Lights", "204.75.167.29", 9875),
            new Server(3407, "Outback", "204.75.167.42", 9875),
            new Server(3722, "Oyster", "204.75.167.9", 9875),
            new Server(3142, "Parka", "204.75.167.22", 6112),
            new Server(3656, "Patagonia (SC)", "204.75.167.57", 3724),
            new Server(3182, "Permafrost", "204.75.167.46", 9875),
            new Server(3723, "Pine Needles", "204.75.167.51", 3724),
            new Server(3512, "Polar (SC)", "204.75.167.55", 9875),
            new Server(3207, "Polar Bear", "204.75.167.33", 9875),
            new Server(3122, "Powder Ball", "204.75.167.11", 6112),
            new Server(3717, "Puddle (SC)", "204.75.167.61", 3724),
            new Server(3156, "Rainbow", "204.75.167.26", 6112),
            new Server(3155, "Rocky Road", "204.75.167.26", 3724),
            new Server(3141, "Sabertooth", "204.75.167.9", 6112),
            new Server(3718, "Sardine (SC)", "204.75.167.61", 6112),
            new Server(3403, "Sasquatch", "204.75.167.41", 6112),
            new Server(3315, "Sherbet", "204.75.167.40", 6112),
            new Server(3158, "Shiver", "204.75.167.27", 3724),
            new Server(3719, "Skates (SC)", "204.75.167.61", 9875),
            new Server(3180, "Sled", "204.75.167.46", 3724),
            new Server(3144, "Sleet", "204.75.167.23", 3724),
            new Server(3175, "Slippers", "204.75.167.48", 6112),
            new Server(3103, "Slushy", "204.75.167.31", 9875),
            new Server(3105, "Snow Angel", "204.75.167.38", 3724),
            new Server(3118, "Snow Ball (SC)", "204.75.167.52", 3724),
            new Server(3129, "Snow Bank", "204.75.167.18", 3724),
            new Server(3134, "Snow Board", "204.75.167.19", 9875),
            new Server(3110, "Snow Cone", "204.75.167.10", 6112),
            new Server(3514, "Snow Covered (SC)", "204.75.167.56", 6112),
            new Server(3106, "Snow Day", "204.75.167.35", 6112),
            new Server(3306, "Snow Drift", "204.75.167.10", 9875),
            new Server(3303, "Snow Flake", "204.75.167.37", 6112),
            new Server(3114, "Snow Fort", "204.75.167.13", 9875),
            new Server(3113, "Snow Globe", "204.75.167.13", 6112),
            new Server(3139, "Snow Plow", "204.75.167.21", 6112),
            new Server(3205, "Snow Shoe", "204.75.167.33", 3724),
            new Server(3211, "Snowbound", "204.75.167.34", 9875),
            new Server(3171, "Snowcap", "204.75.167.31", 3724),
            new Server(3214, "Snowfall", "204.75.167.35", 9875),
            new Server(3166, "Snowmobile", "204.75.167.29", 6112),
            new Server(3409, "Snowy River", "204.75.167.43", 3724),
            new Server(3400, "South Pole", "204.75.167.40", 9875),
            new Server(3168, "Southern Lights", "204.75.167.30", 3724),
            new Server(3174, "Sparkle", "204.75.167.48", 3724),
            new Server(3126, "Sub Zero", "204.75.167.17", 3724),
            new Server(3123, "Summit", "204.75.167.16", 3724),
            new Server(3682, "Tea (SC)", "204.75.167.44", 9875),
            new Server(3136, "Thermal", "204.75.167.20", 6112),
            new Server(3137, "Toboggan", "204.75.167.20", 9875),
            new Server(3109, "Tundra", "204.75.167.12", 3724),
            new Server(3131, "Tuxedo", "204.75.167.18", 9875),
            new Server(3145, "Vanilla", "204.75.167.23", 6112),
            new Server(3163, "Walrus", "204.75.167.28", 9875),
            new Server(3150, "White House", "204.75.167.25", 3724),
            new Server(3102, "White Out", "204.75.167.50", 9875),
            new Server(3312, "Wind Chill", "204.75.167.39", 6112),
            new Server(3117, "Winter Land", "204.75.167.14", 9875),
            new Server(3210, "Wool Socks", "204.75.167.34", 6112),
            new Server(3125, "Yeti", "204.75.167.16", 9875),
            new Server(3206, "Yukon", "204.75.167.33", 6112),
            new Server(3725, "Zipline", "204.75.167.51", 9875)
        };
        public static List<Server> PTservers = new List<Server>
        {
            new Server(3165, "Altitude (SC)", "204.75.167.53", 6112),
            new Server(3552, "Ant\u00e1rtida (SC)", "204.75.167.57", 6112),
            new Server(3815, "Aurora Boreal", "204.75.167.89", 3724),
            new Server(3517, "Avalanche", "204.75.167.64", 9875),
            new Server(3708, "Aventura (SC)", "204.75.167.58", 3724),
            new Server(3408, "Bacana (SC)", "204.75.167.55", 6112),
            new Server(3690, "Bloco de Gelo", "204.75.167.68", 6112),
            new Server(3118, "Bola de Neve (SC)", "204.75.167.52", 3724),
            new Server(3518, "Boreal", "204.75.167.65", 3724),
            new Server(3309, "Botas de Esquim\u00f3 (SC)", "204.75.167.54", 9875),
            new Server(3714, "Britadeira (SC)", "204.75.167.60", 3724),
            new Server(3208, "Canoa (SC)", "204.75.167.53", 9875),
            new Server(3308, "Caverna de Gelo (SC)", "204.75.167.54", 6112),
            new Server(3682, "Ch\u00e1 (SC)", "204.75.167.44", 9875),
            new Server(3154, "Chiclete (SC)", "204.75.167.53", 3724),
            new Server(3713, "Chocolate Quente (SC)", "204.75.167.59", 9875),
            new Server(3675, "Cordilheira", "204.75.167.66", 3724),
            new Server(3710, "Cream Soda (SC)", "204.75.167.58", 9875),
            new Server(3814, "Cristal de Gelo", "204.75.167.88", 9875),
            new Server(3402, "Cubo de Gelo (SC)", "204.75.167.55", 3724),
            new Server(3680, "Descida (SC)", "204.75.167.44", 3724),
            new Server(3502, "Deu Branco", "204.75.167.62", 6112),
            new Server(3811, "Estalactite", "204.75.167.87", 9875),
            new Server(3711, "Fiesta (SC)", "204.75.167.59", 3724),
            new Server(3519, "Floco de neve", "204.75.167.65", 6112),
            new Server(3685, "Flor da Neve", "204.75.167.66", 9875),
            new Server(3686, "Frap\u00ea", "204.75.167.67", 3724),
            new Server(3515, "Freezer", "204.75.167.64", 3724),
            new Server(3504, "Friaca", "204.75.167.62", 9875),
            new Server(3712, "Gafanhoto (SC)", "204.75.167.59", 6112),
            new Server(3505, "Geladeira", "204.75.167.63", 3724),
            new Server(3654, "Geleira (SC)", "204.75.167.56", 9875),
            new Server(3119, "Gelinho (SC)", "204.75.167.52", 6112),
            new Server(3513, "Gelo Seco (SC)", "204.75.167.56", 3724),
            new Server(3520, "Glacial", "204.75.167.65", 9875),
            new Server(3509, "Granizo", "204.75.167.63", 9875),
            new Server(3508, "Inverno", "204.75.167.63", 6112),
            new Server(3810, "Mamute", "204.75.167.87", 6112),
            new Server(3812, "Meias de L\u00e3", "204.75.167.88", 3724),
            new Server(3715, "Migrator (SC)", "204.75.167.60", 6112),
            new Server(3695, "Montanha Nevada", "204.75.167.70", 3724),
            new Server(3716, "Mullet  (SC)", "204.75.167.60", 9875),
            new Server(3816, "Neblina", "204.75.167.89", 6112),
            new Server(3514, "Nevado (SC)", "204.75.167.56", 6112),
            new Server(3683, "Nublado (SC)", "204.75.167.45", 3724),
            new Server(3516, "P\u00f3lo Sul", "204.75.167.64", 6112),
            new Server(3656, "Patag\u00f4nia (SC)", "204.75.167.57", 3724),
            new Server(3719, "Patins (SC)", "204.75.167.61", 9875),
            new Server(3809, "Picol\u00e9", "204.75.167.87", 3724),
            new Server(3689, "Pizza Fria", "204.75.167.68", 3724),
            new Server(3717, "Po\u00e7a (SC)", "204.75.167.61", 3724),
            new Server(3512, "Polar (SC)", "204.75.167.55", 9875),
            new Server(3691, "Pororoca Polar", "204.75.167.68", 9875),
            new Server(3212, "Rinque de Gelo (SC)", "204.75.167.54", 3724),
            new Server(3709, "Saca de Caf\u00e9 (SC)", "204.75.167.58", 6112),
            new Server(3718, "Sardinha (SC)", "204.75.167.61", 6112),
            new Server(3684, "Sib\u00e9ria", "204.75.167.66", 6112),
            new Server(3153, "Sorvete (SC)", "204.75.167.52", 9875),
            new Server(3693, "Sundae", "204.75.167.69", 6112),
            new Server(3681, "Telef\u00e9rico (SC)", "204.75.167.44", 6112),
            new Server(3688, "Torta de Atum", "204.75.167.67", 9875),
            new Server(3694, "Tudo Branco", "204.75.167.69", 9875),
            new Server(3817, "Tundra", "204.75.167.89", 9875),
            new Server(3692, "Vale Branco", "204.75.167.69", 3724),
            new Server(3813, "Ventania", "204.75.167.88", 6112),
            new Server(3687, "Ventilador", "204.75.167.67", 6112),
            new Server(3501, "Zero Grau", "204.75.167.62", 3724)
        };
        public static List<Server> FRservers = new List<Server>
        {
            new Server(3165, "Altitude (SC)", "204.75.167.53", 6112),
            new Server(3552, "Antarctique (SC)", "204.75.167.57", 6112),
            new Server(3681, "Ascension (SC)", "204.75.167.44", 6112),
            new Server(3554, "Aurore Bor\u00e9ale", "204.75.167.84", 3724),
            new Server(3708, "Aventure (SC)", "204.75.167.58", 3724),
            new Server(3309, "Bottes (SC)", "204.75.167.54", 9875),
            new Server(3729, "Bouillotte", "204.75.167.85", 9875),
            new Server(3118, "Boule de Neige (SC)", "204.75.167.52", 3724),
            new Server(3551, "Brise", "204.75.167.83", 6112),
            new Server(3683, "Brumeux (SC)", "204.75.167.45", 3724),
            new Server(3208, "Cano\u00eb (SC)", "204.75.167.53", 9875),
            new Server(3727, "Cascade", "204.75.167.85", 3724),
            new Server(3308, "Caverne (SC)", "204.75.167.54", 6112),
            new Server(3709, "Chaise Longue (SC)", "204.75.167.58", 6112),
            new Server(3154, "Chewing-gum (SC)", "204.75.167.53", 3724),
            new Server(3713, "Chocolat Chaud (SC)", "204.75.167.59", 9875),
            new Server(3153, "Cr\u00e8me Glac\u00e9e (SC)", "204.75.167.52", 9875),
            new Server(3680, "Descente (SC)", "204.75.167.44", 3724),
            new Server(3514, "Enneig\u00e9 (SC)", "204.75.167.56", 6112),
            new Server(3711, "Fiesta (SC)", "204.75.167.59", 3724),
            new Server(3717, "Flaque d'Eau (SC)", "204.75.167.61", 3724),
            new Server(3553, "Flocon", "204.75.167.83", 9875),
            new Server(3402, "Gla\u00e7ons (SC)", "204.75.167.55", 3724),
            new Server(3513, "Glace S\u00e8che (SC)", "204.75.167.56", 3724),
            new Server(3119, "Glaci\u00e8re (SC)", "204.75.167.52", 6112),
            new Server(3654, "Glaciaire (SC)", "204.75.167.56", 9875),
            new Server(3550, "Jour de Neige", "204.75.167.83", 3724),
            new Server(3730, "Marmotte", "204.75.167.86", 3724),
            new Server(3714, "Marteau-Piqueur (SC)", "204.75.167.60", 3724),
            new Server(3715, "Migrateur (SC)", "204.75.167.60", 6112),
            new Server(3728, "Mousqueton", "204.75.167.85", 6112),
            new Server(3656, "Patagonie (SC)", "204.75.167.57", 3724),
            new Server(3212, "Patinoire (SC)", "204.75.167.54", 3724),
            new Server(3719, "Patins \u00e0 Glace (SC)", "204.75.167.61", 9875),
            new Server(3512, "Polaire (SC)", "204.75.167.55", 9875),
            new Server(3726, "Pompon", "204.75.167.84", 9875),
            new Server(3716, "Rouget (SC)", "204.75.167.60", 9875),
            new Server(3718, "Sardine (SC)", "204.75.167.61", 6112),
            new Server(3712, "Scarab\u00e9e (SC)", "204.75.167.59", 6112),
            new Server(3710, "Soda Mousse (SC)", "204.75.167.58", 9875),
            new Server(3408, "Super (SC)", "204.75.167.55", 6112),
            new Server(3731, "Tartine", "204.75.167.86", 6112),
            new Server(3682, "Th\u00e9 (SC)", "204.75.167.44", 9875),
            new Server(3555, "Yeti", "204.75.167.84", 6112)
        };
        public static List<Server> ESservers = new List<Server>
        {
            new Server(3734, "Abordaje ping\u00fcino", "204.75.167.150", 3724),
            new Server(3735, "Abracadabra", "204.75.167.150", 6112),
            new Server(3736, "Aguanieve", "204.75.167.150", 9875),
            new Server(3677, "Al agua pato", "204.75.167.78", 6112),
            new Server(3737, "Alas y aletas", "204.75.167.151", 3724),
            new Server(3738, "Aletas con ritmo", "204.75.167.151", 6112),
            new Server(3676, "Aletas de fuego", "204.75.167.78", 3724),
            new Server(3664, "Aletas heladas", "204.75.167.74", 6112),
            new Server(3696, "Aletazo ninja", "204.75.167.79", 6112),
            new Server(3165, "Altitud (SC)", "204.75.167.53", 6112),
            new Server(3739, "Amuleto ninja", "204.75.167.151", 9875),
            new Server(3663, "Andino", "204.75.167.74", 3724),
            new Server(3552, "Ant\u00e1rtida (SC)", "204.75.167.57", 6112),
            new Server(3740, "Arc\u00f3n de disfraces", "204.75.167.152", 3724),
            new Server(3697, "Arcoiris boreal ", "204.75.167.79", 9875),
            new Server(3666, "Aurora boreal", "204.75.167.75", 3724),
            new Server(3651, "Avalancha", "204.75.167.71", 3724),
            new Server(3708, "Aventuras (SC)", "204.75.167.58", 3724),
            new Server(3698, "Bah\u00eda rock", "204.75.167.80", 3724),
            new Server(3657, "Bajo cero", "204.75.167.72", 6112),
            new Server(3655, "Ballena azul", "204.75.167.72", 3724),
            new Server(3710, "Batido de crema  (SC)", "204.75.167.58", 9875),
            new Server(3119, "Bloque de hielo (SC)", "204.75.167.52", 6112),
            new Server(3118, "Bola de nieve (SC)", "204.75.167.52", 3724),
            new Server(3408, "Bonzo (SC)", "204.75.167.55", 6112),
            new Server(3699, "Bosque de Chocolate", "204.75.167.80", 6112),
            new Server(3741, "Bosque encantado", "204.75.167.152", 6112),
            new Server(3309, "Botas (SC)", "204.75.167.54", 9875),
            new Server(3673, "C\u00f3digo agente", "204.75.167.77", 6112),
            new Server(3709, "Caf\u00e9 en grano  (SC)", "204.75.167.58", 6112),
            new Server(3742, "Calcetines de lana", "204.75.167.152", 9875),
            new Server(3743, "Campamento ping\u00fcino", "204.75.167.153", 3724),
            new Server(3700, "Canci\u00f3n de nieve", "204.75.167.80", 9875),
            new Server(3208, "Canoa (SC)", "204.75.167.53", 9875),
            new Server(3667, "Castillo de cristal", "204.75.167.75", 6112),
            new Server(3308, "Caverna helada (SC)", "204.75.167.54", 6112),
            new Server(3717, "Chapoteo (SC)", "204.75.167.61", 3724),
            new Server(3154, "Chicle globo (SC)", "204.75.167.53", 3724),
            new Server(3713, "Chocolate caliente (SC)", "204.75.167.59", 9875),
            new Server(3744, "Choque de aletas", "204.75.167.153", 6112),
            new Server(3745, "Copito de nieve", "204.75.167.153", 9875),
            new Server(3402, "Cubo de hielo (SC)", "204.75.167.55", 3724),
            new Server(3680, "Cuesta abajo (SC)", "204.75.167.44", 3724),
            new Server(3658, "Deshielo", "204.75.167.72", 9875),
            new Server(3701, "Divertinguin", "204.75.167.81", 3724),
            new Server(3746, "DJ Aletas", "204.75.167.154", 3724),
            new Server(3747, "Dulce o truco", "204.75.167.154", 6112),
            new Server(3672, "Elemento ninja", "204.75.167.77", 9875),
            new Server(3681, "Elevaci\u00f3n (SC)", "204.75.167.44", 6112),
            new Server(3748, "En sus marcas, ping\u00fci ya!", "204.75.167.154", 9875),
            new Server(3668, "Escultura de hielo", "204.75.167.75", 9875),
            new Server(3665, "Estalactita", "204.75.167.74", 9875),
            new Server(3702, "Estrella polar", "204.75.167.81", 6112),
            new Server(3749, "Exploradores de hielo", "204.75.167.155", 3724),
            new Server(3711, "Fiesta (SC)", "204.75.167.59", 3724),
            new Server(3750, "Fresca melod\u00eda", "204.75.167.155", 6112),
            new Server(3751, "G\u00e9iser", "204.75.167.155", 9875),
            new Server(3654, "Glaciar (SC)", "204.75.167.56", 9875),
            new Server(3752, "Hechizo de hada", "204.75.167.156", 3724),
            new Server(3153, "Helado de crema (SC)", "204.75.167.52", 9875),
            new Server(3513, "Hielo seco (SC)", "204.75.167.56", 3724),
            new Server(3753, "Hurac\u00e1n ninja", "204.75.167.156", 6112),
            new Server(3669, "Igl\u00falandia", "204.75.167.76", 3724),
            new Server(3674, "Invasi\u00f3n puffle", "204.75.167.76", 9875),
            new Server(3671, "Isla pirata", "204.75.167.77", 3724),
            new Server(3679, "Isla secreta", "204.75.167.79", 3724),
            new Server(3704, "Lluvia de estrellas", "204.75.167.82", 3724),
            new Server(3754, "Luces, puffles y acci\u00f3n!", "204.75.167.156", 9875),
            new Server(3705, "Mejor amigo", "204.75.167.82", 6112),
            new Server(3715, "Migrator (SC)", "204.75.167.60", 6112),
            new Server(3755, "Movimiento ninja", "204.75.167.157", 3724),
            new Server(3716, "Mullet (SC)", "204.75.167.60", 9875),
            new Server(3756, "Mundo glacial", "204.75.167.157", 6112),
            new Server(3683, "Nebulosa (SC)", "204.75.167.45", 3724),
            new Server(3514, "Nevado (SC)", "204.75.167.56", 6112),
            new Server(3653, "Nevisca", "204.75.167.71", 9875),
            new Server(3703, "Neviscas", "204.75.167.81", 9875),
            new Server(3706, "Nieve rosada", "204.75.167.82", 9875),
            new Server(3707, "Ola surfera", "204.75.167.70", 6112),
            new Server(3661, "Oso polar", "204.75.167.73", 6112),
            new Server(3656, "Patagonia (SC)", "204.75.167.57", 3724),
            new Server(3719, "Patines (SC)", "204.75.167.61", 9875),
            new Server(3670, "Ping\u00fcin\u00f3polis", "204.75.167.76", 6112),
            new Server(3652, "Ping\u00fcinera", "204.75.167.71", 6112),
            new Server(3212, "Pista de patinaje (SC)", "204.75.167.54", 3724),
            new Server(3757, "Pizzarock", "204.75.167.157", 9875),
            new Server(3512, "Polar (SC)", "204.75.167.55", 9875),
            new Server(3660, "Polo sur", "204.75.167.73", 3724),
            new Server(3758, "Puffito de agua", "204.75.167.158", 3724),
            new Server(3759, "Pufflelandia", "204.75.167.158", 6112),
            new Server(3760, "Refugio en el \u00c1rbol", "204.75.167.158", 9875),
            new Server(3712, "Saltamontes (SC)", "204.75.167.59", 6112),
            new Server(3718, "Sardinas enlatadas (SC)", "204.75.167.61", 6112),
            new Server(3682, "T\u00e9 helado (SC)", "204.75.167.44", 9875),
            new Server(3714, "Taladrando (SC)", "204.75.167.60", 3724),
            new Server(3678, "Trineo de nieve", "204.75.167.78", 9875),
            new Server(3662, "Yeti", "204.75.167.73", 9875)
        };
    }
}
