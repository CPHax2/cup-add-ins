﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CuPAddIn
{
    public class Items
    {
        private static List<Item> itemList = new List<Item>();
        public static Item[] SafeItems { get; private set; }
        public static void LoadItemList()
        {
            string jsonString = new System.Net.WebClient().DownloadString("http://media1.clubpenguin.com/play/en/web_service/game_configs/paper_items.json");
            string[] json = jsonString.Split('}');
            List<Item> safe = new List<Item>();
            for (int i = 0; i < json.Length - 1; i++)
            {
                string id = "", name = "";
                int type = 0, cost = 0;
                bool isMember = false, isBait = false, isEpf = false, isGameAchievable = false, isExclusive = false, isTourGuide = false;
                string[] item = json[i].Split('"');
                for (int j = 1; j < item.Length - 1; j++)
                {
                    switch (item[j])
                    {
                        case "paper_item_id":
                            id = item[++j];
                            id = id.Substring(1, id.Length - 2);
                            break;
                        case "label":
                            name = item[j += 2];
                            break;
                        case "type":
                            string t = item[++j];
                            t = t.Substring(1, t.Length - 2);
                            int.TryParse(t, out type);
                            break;
                        case "cost":
                            string c = item[++j];
                            c = c.Substring(1, c.Length - 2);
                            int.TryParse(c, out cost);
                            break;
                        case "is_member":
                            j++;
                            isMember = item[j].Substring(1, item[j].Length - 2) == "true";
                            break;
                        case "is_bait":
                            isBait = item[j += 2] != "0";
                            break;
                        case "is_epf":
                            isEpf = item[j += 2] != "0";
                            break;
                        case "is_game_achievable":
                            isGameAchievable = item[j += 2] != "0";
                            break;
                        case "exclusive":
                            isExclusive = item[j += 2] != "0";
                            break;
                        case "make_tour_guide":
                            isTourGuide = item[j += 2] != "0";
                            break;
                    }
                }
                Item newItem = new Item(id, name, (Item.ItemType)type, cost, isMember, isBait, isEpf, isGameAchievable, isExclusive, isTourGuide);
                if (newItem.IsSafeToAdd) safe.Add(newItem);
                itemList.Add(newItem);
            }
            SafeItems = safe.ToArray();
            safe.Clear();
        }

        public static Item GetItemById(string id)
        {
            foreach (Item item in itemList)
            {
                if (item.ID == id) return item;
            }
            return null;
        }

        public static bool IsSafe(string id)
        {
            foreach (Item item in SafeItems)
            {
                if (item.ID == id) return true;
            }
            return false;
        }

        public class Item
        {
            public Item(string id, string name, ItemType type, int cost, bool isMember, bool isBait, bool isEpf, bool isGameAchievable, bool isExclusive, bool isTourGuide)
            {
                ID = id;
                Name = name;
                Type = type;
                Cost = cost;
                IsMember = isMember;
                IsBait = isBait;
                IsEPF = isEpf;
                IsGameAchievable = isGameAchievable;
                IsExclusive = isExclusive;
                IsTourGuide = isTourGuide;
            }

            public enum ItemType : short
            {
                Color = 1,
                Head = 2,
                Face = 3,
                Neck = 4,
                Body = 5,
                Hand = 6,
                Feet = 7,
                Pin = 8,
                Background = 9,
                Award = 10
            }

            public string ID { get; private set; }
            public string Name { get; private set; }
            public ItemType Type { get; private set; }
            public int Cost { get; private set; }
            public bool IsMember { get; private set; }
            public bool IsBait { get; private set; }
            public bool IsEPF { get; private set; }
            public bool IsGameAchievable { get; private set; }
            public bool IsExclusive { get; private set; }
            public bool IsTourGuide { get; private set; }
            public bool IsSafeToAdd
            {
                get
                {
                    return !IsBait && !IsEPF && !IsGameAchievable && !IsExclusive && !IsTourGuide;
                }
            }
        }
    }
}
