﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;
using System.Net.Http;

namespace CuPAddIn
{
    static class MultiBot
    {
        public static string LocalPlayerDisplayName { get; private set; }
        public static string LocalPlayerUsername { get; private set; }
        public static string Lang { get; private set; }
        public static string CurrentTrackInfo { get; private set; }
        public static bool AutoConnect
        {
            get { return autoConnect; }
            set
            {
                autoConnect = value;
                UserSettings.Set(value ? "1" : "0");
            }
        }
        private static bool autoConnect = true;

        public static void Init()
        {
            autoConnect = UserSettings.Get() != "0";
            LocalPlayer.SetClientSide = false;
            PacketMonitor.On("broadcastingmusictracks", (packet, mode) =>
            {
                string[] Packet = packet.Split('%');
                if (Packet[4] == "0") CurrentTrackInfo = "";
                else
                {
                    string[] track = Packet[6].Split(',')[0].Split('|');
                    CurrentTrackInfo = track[0] + "%" + track[3];
                }
            });
            Lang = Servers.CurrentLang;
            LocalPlayerDisplayName = Core.ExecuteFunction("SHELL.getNicknameById", LocalPlayer.Id).RawResult;
            LocalPlayerUsername = Core.GetVar("AIRTOWER._airtower.username").RawResult.ToLower();
            AddInBase.form.SetTarget(LocalPlayer.Id.ToString(), LocalPlayerDisplayName, false);
        }

        public static void SaveList(FollowBot[] bots)
        {
            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog { Filter = "CP Bot List (*.cpbl)|*.cpbl" };
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                try
                {
                    string path = dialog.FileName;
                    int fileSize = 4 + 40 * bots.Length;
                    foreach (FollowBot bot in bots)
                    {
                        fileSize += bot.Name.Length;
                    }
                    byte[] data = new byte[fileSize];
                    byte[] numberOfBots = BitConverter.GetBytes((uint)bots.Length);
                    Buffer.BlockCopy(numberOfBots, 0, data, 0, 4);
                    int pos = 4;
                    foreach (FollowBot bot in bots)
                    {
                        byte[] nameLength = BitConverter.GetBytes(bot.Name.Length);
                        byte[] name = Encoding.UTF8.GetBytes(bot.Name);
                        byte[] hash = Encoding.UTF8.GetBytes(bot.PasswordHash);
                        byte[] x = BitConverter.GetBytes((short)bot.OffsetX);
                        byte[] y = BitConverter.GetBytes((short)bot.OffsetY);
                        Buffer.BlockCopy(nameLength, 0, data, pos, 4);
                        pos += 4;
                        Buffer.BlockCopy(name, 0, data, pos, name.Length);
                        pos += name.Length;
                        Buffer.BlockCopy(hash, 0, data, pos, 32);
                        pos += 32;
                        Buffer.BlockCopy(x, 0, data, pos, 2);
                        pos += 2;
                        Buffer.BlockCopy(y, 0, data, pos, 2);
                        pos += 2;
                    }
                    using (System.IO.MemoryStream fs = new System.IO.MemoryStream(data))
                    {
                        SFile sfile = new SFile();
                        System.IO.Stream file = System.IO.File.Create(path);
                        sfile.EncryptFile(fs, ref file, true);
                        file.Dispose();
                    }
                }
                catch { System.Windows.Forms.MessageBox.Show("Save File Error!", "MultiBot - Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation); }
            }));
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
        }

        public static void OpenList()
        {
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog { Filter = "CP Bot List (*.cpbl)|*.cpbl" };
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                try
                {
                    byte[] data;
                    using (System.IO.FileStream fs = new System.IO.FileStream(dialog.FileName, System.IO.FileMode.Open))
                    {
                        SFile sfile = new SFile();
                        System.IO.Stream ms = new System.IO.MemoryStream();
                        sfile.DecryptFile(fs, ref ms, false);
                        data = ((System.IO.MemoryStream)ms).ToArray();
                    }
                    uint botCount = BitConverter.ToUInt32(data, 0);
                    FollowBot[] list = new FollowBot[botCount];
                    int pos = 4;
                    for (int i = 0; i < botCount; i++)
                    {
                        int nameLength = BitConverter.ToInt32(data, pos);
                        pos += 4;
                        string name = Encoding.UTF8.GetString(data, pos, nameLength);
                        pos += nameLength;
                        string hash = Encoding.UTF8.GetString(data, pos, 32);
                        pos += 32;
                        int x = BitConverter.ToInt16(data, pos);
                        pos += 2;
                        int y = BitConverter.ToInt16(data, pos);
                        pos += 2;
                        FollowBot bot = new FollowBot(name, hash, true, x, y);
                        list[i] = bot;
                    }
                    AddInBase.form.AddBots(list);
                }
                catch { System.Windows.Forms.MessageBox.Show("Open File Error!", "MultiBot - Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation); }
            }));
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
        }
    }
}
