﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Net.Http;

namespace CuPAddIn
{
    public class PCL : IDisposable
    {
        public delegate void ConnectDelegate();
        public delegate void ErrorDelegate(string errorID, string errorStr);
        public delegate void ReceivePacketDelegate(string packet);
        public delegate void StatusChangeDelegate(PCLStatus status);
        public ConnectDelegate OnJoin { get; set; }
        public ErrorDelegate OnError { get; set; }
        public ReceivePacketDelegate ReceivePacket { get; set; }
        public StatusChangeDelegate OnStatusChanged { get; set; }
        public string Name { get { return user; } }
        public string PasswordHash { get { return passHash; } }
        public string Id { get { return playerId; } }
        public List<string> Items { get; set; }
        public PCLStatus Status
        {
            get { return status; }
            private set
            {
                status = value;
                if (OnStatusChanged != null) OnStatusChanged(value);
            }
        }

        private ConnectDelegate OnConnect { get; set; }
        private Dictionary<string, ReceivePacketDelegate> callbacks = new Dictionary<string, ReceivePacketDelegate>();
        private TcpClient loginSocket, gameSocket;
        private NetworkStream ns;
        private string user, packetBuffer, confirmationHash = "", loginDataRaw = "", loginKey = "", internalRoomId = "-1", playerId = "";
        private byte[] buffer = new byte[1024];
        private bool isHash;
        private string passHash;
        private System.Timers.Timer heartbeat = new System.Timers.Timer { Interval = 60000 }, checkConnectionTimer = new System.Timers.Timer { Interval = 2000 };
        private PCLStatus status = PCLStatus.Offline;
        private PhraseChatClient pcc;

        public enum PCLStatus : int
        {
            Offline = 0,
            Connecting = 1,
            Online = 2,
            Error = 3
        }

        public PCL(string username, string password, bool hash)
        {
            user = username;
            isHash = hash;
            passHash = isHash ? password : SwappedMD5(password).ToUpper();
            heartbeat.Elapsed += (s, e) =>
            {
                sendHeartbeat();
            };
            checkConnectionTimer.Elapsed += (s, e) =>
            {
                if (gameSocket == null || !gameSocket.Connected)
                {
                    checkConnectionTimer.Stop();
                    heartbeat.Stop();
                    Status = PCLStatus.Error;
                    OnError("-1", "Connection Lost");
                }
            };
            pcc = new PhraseChatClient(MultiBot.Lang);
            Items = new List<string>();
        }

        public void Connect(string IP, int port)
        {
            Status = PCLStatus.Connecting;
            OnConnect = () =>
            {
                gameConnect(IP, port);
            };
            loginSocket = new TcpClient();
            loginSocket.BeginConnect("204.75.167.165", 3724, OnSocketConnect, null);
        }

        public void Reconnect(string IP, int port)
        {
            LogOut();
            System.Timers.Timer t = new System.Timers.Timer { Interval = 500 };
            t.Elapsed += (s, e) =>
            {
                t.Stop();
                Connect(IP, port);
                t.Dispose();
            };
            t.Start();
        }

        public void LogOut()
        {
            if (Status != PCLStatus.Online && Status != PCLStatus.Connecting) return;
            try
            {
                Status = PCLStatus.Offline;
                checkConnectionTimer.Stop();
                heartbeat.Stop();
                Off();
                OnConnect = null;
                OnJoin = null;
                loginSocket.Close();
                gameSocket.Close();
                ns.Close(400);
            }
            catch { }
        }

        private void OnSocketConnect(IAsyncResult res)
        {
            try
            {
                loginSocket.EndConnect(res);
                ns = loginSocket.GetStream();
                ReceivePacket = HandleRawPacket;
                ns.BeginRead(buffer, 0, 1024, OnReceivePacket, null);
                SendPacket("<msg t='sys'><body action='rndK' r='-1'></body></msg>");
            }
            catch
            {
#if DEBUG
                Cup.Extensibility.Library.Log.Write("AddIn", "[OnSocketConnect] Error", Cup.Extensibility.Library.Log.Type.Error);
#endif
            }
        }

        private void OnReceivePacket(IAsyncResult res)
        {
            try
            {
                int bytesRead = ns.EndRead(res);
                if (bytesRead == 0) return;

                string[] raw = (packetBuffer + Encoding.UTF8.GetString(
                    buffer, 0, bytesRead)).Split('\0');

                for (int i = 0; i < raw.Length - 1; i++)
                {
                    ReceivePacket(raw[i]);
#if DEBUG
                    Cup.Extensibility.Library.Log.Write("ReceivePacket", "[" + Name + "] " + raw[i], Cup.Extensibility.Library.Log.Type.Debug);
#endif
                }

                packetBuffer = raw[raw.Length - 1];
                ns.BeginRead(buffer, 0, 1024, OnReceivePacket, null);
            }
            catch
            {
#if DEBUG
                Cup.Extensibility.Library.Log.Write("Add-In", "[OnReceivePacket] Error", Cup.Extensibility.Library.Log.Type.Error);
#endif
            }
        }

        private void HandleRawPacket(string str)
        {
            if (str[0] == '<')
            {
                if (str.Contains("rndK"))
                {
                    string rndK = Stribet(str, "<k>", "</k>");
                    SendPacket("<msg t='sys'><body action='login' r='0'><login z='w1'><nick><![CDATA[" +
                        user.ToLower() + "]]></nick><pword><![CDATA[" + GenerateKey(rndK) +
                        "]]></pword></login></body></msg>");
                }
            }
            else
            {
                string[] pack = str.Split('%').Skip(1).ToArray();
                try
                {
                    switch (pack[1])
                    {
                        case "e":
                            Status = PCLStatus.Error;
                            if (OnError != null) OnError(pack[3], str);
                            break;
                        case "l":
                            confirmationHash = pack[4];
                            loginDataRaw = pack[3];
                            string[] ld = loginDataRaw.Split('|');
                            playerId = ld[0];
                            loginKey = ld[3];
                            if (OnConnect != null) OnConnect();
                            break;
                    }
                }
                catch { }
            }
        }

        public void OnPacket(string handler, ReceivePacketDelegate callback)
        {
            ReceivePacketDelegate rpd;
            if (callbacks.TryGetValue(handler, out rpd))
            {
                callbacks[handler] = callback;
            }
            else callbacks.Add(handler, callback);
        }

        private void gameConnect(string IP, int port)
        {
            if (Status != PCLStatus.Connecting) return;
            gameSocket = new TcpClient();
            gameSocket.BeginConnect(IP, port, (res) =>
            {
                try
                {
                    gameSocket.EndConnect(res);
                    ns = gameSocket.GetStream();
                    ReceivePacket = (packet) =>
                    {
                        if (packet[0] == '<')
                        {
                            if (packet.Contains("rndK"))
                            {
                                string rand_key = Stribet(packet, "<k>", "</k>");
                                SendPacket("<msg t='sys'><body action='login' r='0'><login z='w1'><nick><![CDATA[" +
                                    loginDataRaw + "]]></nick><pword><![CDATA[" + SwappedMD5(loginKey + rand_key) + loginKey +
                                    "#" + confirmationHash + "]]></pword></login></body></msg>");
                            }
                        }
                        else
                        {
                            string[] pack = packet.Split('%').Skip(1).ToArray();
                            switch (pack[1])
                            {
                                case "l":
                                    string playerId = loginDataRaw.Split('|')[0];
                                    SendPacket("%xt%s%j#js%-1%" + playerId + "%" + loginKey + "%en%");
                                    break;
                                case "js":
                                    Status = PCLStatus.Online;
                                    heartbeat.Start();
                                    checkConnectionTimer.Start();
                                    GetItems();
                                    if (OnJoin != null) OnJoin();
                                    ReceivePacket = processPacket;
                                    break;
                            }
                        }
                    };
                    ns.BeginRead(buffer, 0, 1024, OnReceivePacket, null);
                    SendPacket("<msg t='sys'><body action='rndK' r='-1'></body></msg>");
                }
                catch
                {
                    if (Status == PCLStatus.Connecting) Status = PCLStatus.Error;
                    else Status = PCLStatus.Offline;
                    OnError("-2", "EndConnect Error");
                }
            }, null);
        }

        private void processPacket(string packet)
        {
            try
            {
                string[] Packet = packet.Split('%');
                string handler = Packet[2];
                if (handler == "jr") internalRoomId = Packet[3];
                else if (handler == "ban")
                {
                    Status = PCLStatus.Error;
                    if (OnError != null) OnError(Packet[4], packet);
                }
                else if (handler == "e")
                {
                    if (gameSocket == null || !gameSocket.Connected) Status = PCLStatus.Error;
                    if (OnError != null) OnError(Packet[4], packet);
                }
                else if (handler == "ai")
                {
                    Items.Insert(0, packet.Split('%')[4]);
                }
                ReceivePacketDelegate rpd;
                if (callbacks.TryGetValue(handler, out rpd) && rpd != null) rpd(packet);
            }
            catch { }
        }

        public void Off()
        {
            callbacks.Clear();
        }

        public void Off(string handler)
        {
            OnPacket(handler, null);
        }

        public void SendPacket(string pack)
        {
            if (ns == null || pack.Length == 0) return;
            if (Status != PCLStatus.Online)
            {
                if (loginSocket == null || !loginSocket.Connected) return;
            }
            else if (gameSocket == null || !gameSocket.Connected) return;
            byte[] send = Encoding.UTF8.GetBytes(pack + '\0');
            ns.Write(send, 0, send.Length);
#if DEBUG
            Cup.Extensibility.Library.Log.Write("SendPacket", "[" + Name + "] "+ pack, Cup.Extensibility.Library.Log.Type.Debug);
#endif
        }

        private string Stribet(string i, string dl, string dr)
        {
            int pl = i.IndexOf(dl) + dl.Length;
            int pr = i.IndexOf(dr, pl);
            return i.Substring(pl, pr - pl);
        }

        private string SwappedMD5(string str)
        {
            string hash = "";
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] bin = md5.ComputeHash(Encoding.UTF8.GetBytes(str));
                foreach (char c in bin) hash += ((int)c).ToString("x2");
            }
            return hash.Substring(16, 16) + hash.Substring(0, 16);
        }

        private string GenerateKey(string rndK)
        {
            return SwappedMD5(passHash + rndK +
                "a1ebe00441f5aecb185d0ec178ca2305Y(02.>'H}t\":E1_root");
        }

        private void GetItems()
        {
            OnPacket("gi", (packet) =>
            {
                string[] items = packet.Split('%');
                Items = items.Skip(4).Take(items.Length - 5).ToList();
            });
            SendPacket("%xt%s%i#gi%-1%");
        }

        private void sendHeartbeat()
        {
            SendPacket("%xt%s%u#h%" + internalRoomId + "%");
        }

        public void CheckPlayerLocation(string ID)
        {
            SendPacket("%xt%s%u#bf%" + internalRoomId + "%" + ID + "%");
        }

        public void JoinRoom(string ID, int x = 0, int y = 0)
        {
            SendPacket("%xt%s%j#jr%" + internalRoomId + "%" + ID + "%" + x + "%" + y + "%");
        }

        public void Move(int x, int y)
        {
            SendPacket("%xt%s%u#sp%" + internalRoomId + "%" + x + "%" + y + "%");
        }

        public void SendFrame(string ID)
        {
            SendPacket("%xt%s%u#sf%" + internalRoomId + "%" + ID + "%");
        }

        public void ThrowSnowball(string x, string y)
        {
            SendPacket("%xt%s%u#sb%" + internalRoomId + "%" + x + "%" + y + "%");
        }

        public void SendEmote(string ID)
        {
            SendPacket("%xt%s%u#se%" + internalRoomId + "%" + ID + "%");
        }

        public void SendMessage(string message)
        {
            SendPacket("%xt%s%m#sm%" + internalRoomId + "%" + playerId + "%" + message + "%");
        }

        public void SendPhraseChatMessage(string pcid)
        {
            SendPacket("%xt%s%m#pcam%" + internalRoomId + "%" + pcid + "%");
        }

        public async void SendPhraseChatMessageByText(string text)
        {
            string pcid = await pcc.GetPCID(text);
            if (pcid != null) SendPhraseChatMessage(pcid);
            else SendMessage(text);
        }

        public void SendSafeMessage(string ID)
        {
            SendPacket("%xt%s%u#ss%" + internalRoomId + "%" + ID + "%");
        }

        public void SendJoke(string ID)
        {
            SendPacket("%xt%s%u#sj%" + internalRoomId + "%" + ID + "%");
        }

        public void SendTourGuideMessage(string ID)
        {
            SendPacket("%xt%s%u#sg%" + internalRoomId + "%" + ID + "%");
        }

        public void FollowPath(string ID)
        {
            SendPacket("%xt%s%u#followpath%" + internalRoomId + "%" + ID + "%");
        }

        public void Teleport(int x, int y)
        {
            SendPacket("%xt%s%u#tp%" + internalRoomId + "%" + x + "%" + y + "%");
        }

        public void LikeTrack(string trackInfo)
        {
            if (trackInfo == "") return;
            SendPacket("%xt%s%musictrack#liketrack%" + internalRoomId + "%" + trackInfo + "%");
        }

        public void LikeIgloo(string internalId)
        {
            SendPacket("%xt%s%g#li%" + internalId + "%");
        }

        public void OpenNewspaper()
        {
            SendPacket("%xt%s%t#at%" + internalRoomId + "%1%1%");
        }

        public void OpenBluePrints()
        {
            SendPacket("%xt%s%t#at%" + internalRoomId + "%2%1%");
        }

        public void ClosePrints()
        {
            SendPacket("%xt%s%t#rt%" + internalRoomId + "%1%%");
        }

        public void AddItem(string ID)
        {
            SendPacket("%xt%s%i#ai%" + internalRoomId + "%" + ID + "%");
        }

        public void UpdateHeadItem(string ID)
        {
            SendPacket("%xt%s%s#uph%" + internalRoomId + "%" + ID + "%");
        }

        public void UpdateFaceItem(string ID)
        {
            SendPacket("%xt%s%s#upf%" + internalRoomId + "%" + ID + "%");
        }

        public void UpdateNeckItem(string ID)
        {
            SendPacket("%xt%s%s#upn%" + internalRoomId + "%" + ID + "%");
        }

        public void UpdateBodyItem(string ID)
        {
            SendPacket("%xt%s%s#upb%" + internalRoomId + "%" + ID + "%");
        }

        public void UpdateHandItem(string ID)
        {
            SendPacket("%xt%s%s#upa%" + internalRoomId + "%" + ID + "%");
        }

        public void UpdateFeetItem(string ID)
        {
            SendPacket("%xt%s%s#upe%" + internalRoomId + "%" + ID + "%");
        }

        public void UpdateColor(string ID)
        {
            SendPacket("%xt%s%s#upc%" + internalRoomId + "%" + ID + "%");
        }

        public void UpdateFlag(string ID)
        {
            SendPacket("%xt%s%s#upl%" + internalRoomId + "%" + ID + "%");
        }

        public void UpdatePhoto(string ID)
        {
            SendPacket("%xt%s%s#upp%" + internalRoomId + "%" + ID + "%");
        }

        public void Dispose()
        {
            ((IDisposable)loginSocket).Dispose();
            ((IDisposable)gameSocket).Dispose();
            ns.Dispose();
            heartbeat.Dispose();
            checkConnectionTimer.Dispose();
            pcc.Dispose();
        }
    }

    class PhraseChatClient : IDisposable
    {
        public string Language { get; private set; }
        private HttpClient httpClient = new HttpClient();

        public PhraseChatClient(string lang)
        {
            Language = lang;
            httpClient.BaseAddress = new Uri("https://api.disney.com/social/autocomplete/v2/search/messages");
            httpClient.DefaultRequestHeaders.Host = "api.disney.com";
            httpClient.DefaultRequestHeaders.Connection.Add("keep-alive");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("FD", "08306ECE-C36C-4939-B65F-4225F37BD296:905664F40E29B95CF5810B2ACA85497C7430BB1498E74B52");
            httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Referrer = new Uri("http://media1.clubpenguin.com/play/v2/client/club_penguin.swf?");
        }

        public async System.Threading.Tasks.Task<string> GetPCID(string text)
        {
            string json = "{\"product\":\"pen\",\"original_text\":\"" + text + "\",\"language\":\"" + Language + "\"}";
            var response = await httpClient.PostAsync("https://api.disney.com/social/autocomplete/v2/search/messages", new StringContent(json, Encoding.UTF8, "application/json"));
            var responseString = await response.Content.ReadAsStringAsync();
            string[] parts = responseString.Split('"');
            if (parts[1] == "id") return parts[3];
            return null;
        }

        public void Dispose()
        {
            httpClient.Dispose();
        }
    }
}
