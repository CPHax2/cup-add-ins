﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    static class SelfBot
    {
        private static string targetId = "";
        private static int offsetX = 0, offsetY = 0;
        private static bool copyMove = true, copyFrame = true, copySnowBall = true, copyEmote = true, copyMessage = false, isWaiting = false;
        private static RoomType targetRoomType = RoomType.None;
        private static System.Timers.Timer checkLocationTimer = new System.Timers.Timer { Interval = 2000 };
        private static PhraseChatClient pcc;

        static SelfBot()
        {
            pcc = new PhraseChatClient(MultiBot.Lang);
            checkLocationTimer.Elapsed += (s, e) =>
            {
                sendCheckLocation();
            };
        }

        public enum RoomType : int
        {
            None = 0,
            Regular = 1,
            Game = 2,
            OwnIgloo = 3,
            OtherIgloo = 4,
            Backyard = 5,
        }

        private static void setIsWaiting(bool enabled)
        {
            if (enabled)
            {
                checkLocationTimer.Start();
                sendCheckLocation();
                setTriggersEnabled(true);
            }
            else
            {
                checkLocationTimer.Stop();
                setTriggersEnabled(false);
            }
            isWaiting = enabled;
        }

        public static RoomType TargetRoomType
        {
            get { return targetRoomType; }
            private set
            {
                if (value != targetRoomType)
                {
                    if (value == RoomType.Game) setIsWaiting(true);
                    else if (targetRoomType == RoomType.Game) setIsWaiting(false);
                    targetRoomType = value;
                    if (Active) OnRoomTypeChanged(value);
                }
            }
        }

        private static RoomType roomTypeFromResponse(string packet)
        {
            string[] pack = packet.Split('%');
            if (pack[4] == "-1" || pack[4] == "1100") return RoomType.None;
            if (pack[5] == "igloo") return RoomType.OwnIgloo;
            if (pack[5] == "backyard") return RoomType.Backyard;
            int roomId = -1;
            int.TryParse(pack[4], out roomId);
            if (roomId >= 1000) return RoomType.OtherIgloo;
            if (roomId >= 900 && roomId < 1000 || roomId == 898) return RoomType.Game;
            if (roomId > 0) return RoomType.Regular;
            return RoomType.None;
        }

        public delegate void OnStartDelegate(bool targetOnline);
        public delegate void OnRoomTypeChangedDelegate(RoomType roomType);
        public delegate void OnLogoffDelegate();

        public static OnStartDelegate OnStart { get; set; }
        public static OnRoomTypeChangedDelegate OnRoomTypeChanged { get; set; }
        public static OnLogoffDelegate OnLogoff { get; set; }
        public static int OffsetX
        {
            get { return offsetX; }
        }

        public static int OffsetY
        {
            get { return offsetY; }
        }

        public static void SetOffset(int x, int y)
        {
            offsetX = x;
            offsetY = y;
            if (Active && CopyMove) LocalPlayer.Move(targetX + OffsetX, targetY + OffsetY);
        }

        private static int targetX
        {
            get
            {
                string response = Core.GetVar("SHELL.playerModel._playerCollection._itemLookup." + TargetID + ".x").RawResult;
                double x;
                if (double.TryParse(response, out x)) return (int)x;
                return -1;
            }
        }

        private static int targetY
        {
            get
            {
                string response = Core.GetVar("SHELL.playerModel._playerCollection._itemLookup." + TargetID + ".y").RawResult;
                double y;
                if (double.TryParse(response, out y)) return (int)y;
                return -1;
            }
        }

        public static string TargetID
        {
            get { return targetId; }
            set
            {
                targetId = value;
                if (Active)
                {
                    int x = targetX, y = targetY;
                    if (x > 0 && y > 0) LocalPlayer.Move(x + OffsetX, y + OffsetY);
                    else Begin();
                }
            }
        }

        public static bool Active { get; private set; }

        public static void Begin()
        {
            PacketMonitor.On("bf", (packet, mode) =>
            {
                Core.ExecuteFunction("AuxSWF.setPlayerLocationPromptsEnabled", true);
                string[] pack = packet.Split('%');
                if (pack[6] == TargetID)
                {
                    TargetRoomType = roomTypeFromResponse(packet);
                    start();
                    OnStart?.Invoke(true);
                    if (!isWaiting)
                    {
                        int roomId = int.Parse(pack[4]);
                        if (roomId != Room.ExternalID) Room.Join(roomId);
                    }
                }
                else if (pack[6] == "-1")
                {
                    Stop();
                    OnStart?.Invoke(false);
                }
            });
            sendCheckLocation();
        }

        private static void start()
        {
            Active = true;
            PacketMonitor.On("bf", (packet, mode) =>
            {
                Core.ExecuteFunction("AuxSWF.setPlayerLocationPromptsEnabled", true);
                string[] pack = packet.Split('%');
                if (pack[6] == TargetID)
                {
                    TargetRoomType = roomTypeFromResponse(packet);
                    if (!isWaiting)
                    {
                        int roomId = int.Parse(pack[4]);
                        if (roomId != Room.ExternalID) Room.Join(roomId);
                    }
                }
                else if (pack[6] == "-1")
                {
                    Stop();
                    OnLogoff?.Invoke();
                }
            });
            PacketMonitor.On("jr", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                bool isPlayerInRoom = false;
                for (int i = 5; i < pack.Length - 1; i++)
                {
                    string[] player = pack[i].Split('|');
                    if (player[0] == TargetID)
                    {
                        isPlayerInRoom = true;
                        if (CopyMove) LocalPlayer.Move(int.Parse(player[12]) + OffsetX, int.Parse(player[13]) + OffsetY);
                        break;
                    }
                }
                if (!isPlayerInRoom) sendCheckLocation();
            });
            PacketMonitor.On("rp", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID) sendCheckLocation();
            });
            PacketMonitor.On("j#crl", (packet, mode) =>
            {
                if (!isWaiting) setTriggersEnabled(false);
            }, PacketMode.Send);
            if (CopyMove) setCopyMove(true);
            if (CopyFrame) setCopyFrame(true);
            if (CopySnowBall) setCopySnowBall(true);
            if (CopyEmote) setCopyEmote(true);
            if (CopyMessage) setCopyMessage(true);
            if (!isWaiting) setTriggersEnabled(false);
        }

        public static void Stop()
        {
            checkLocationTimer.Stop();
            setTriggersEnabled(true);
            PacketMonitor.Off("bf");
            PacketMonitor.Off("jr");
            PacketMonitor.Off("rp");
            PacketMonitor.Off("j#crl");
            setCopyMove(false);
            setCopyFrame(false);
            setCopySnowBall(false);
            setCopyEmote(false);
            setCopyMessage(false);
            Active = false;
            TargetRoomType = RoomType.None;
        }

        private static void setCopyMove(bool enabled)
        {
            if (!enabled)
            {
                PacketMonitor.Off("sp");
                PacketMonitor.Off("followpath");
                PacketMonitor.Off("tp");
                return;
            }
            PacketMonitor.On("sp", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] != TargetID) return;
                int x, y;
                x = int.Parse(pack[5]);
                y = int.Parse(pack[6]);
                LocalPlayer.Move(x + OffsetX, y + OffsetY);
            });
            PacketMonitor.On("followpath", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID) followPath(pack[5]);
            });
            PacketMonitor.On("tp", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID)
                {
                    int x, y;
                    int.TryParse(pack[5], out x);
                    int.TryParse(pack[6], out y);
                    teleport(x + OffsetX, y + OffsetY);
                }
            });
            int tx = targetX, ty = targetY;
            if (tx > 0 && ty > 0) LocalPlayer.Move(tx + OffsetX, ty + OffsetY);
        }

        private static void setCopyFrame(bool enabled)
        {
            if (!enabled)
            {
                PacketMonitor.Off("sf");
                PacketMonitor.Off("sa");
                return;
            }
            PacketMonitor.On("sf", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID)
                {
                    int id = int.Parse(pack[5]);
                    LocalPlayer.SendFrame(id);
                }
            });
            PacketMonitor.On("sa", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID)
                {
                    int id = int.Parse(pack[5]);
                    LocalPlayer.SendFrame(id);
                }
            });
        }

        private static void setCopySnowBall(bool enabled)
        {
            if (!enabled)
            {
                PacketMonitor.Off("sb");
                return;
            }
            PacketMonitor.On("sb", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID)
                {
                    int x, y;
                    x = int.Parse(pack[5]);
                    y = int.Parse(pack[6]);
                    LocalPlayer.ThrowSnowball(x, y);
                }
            });
        }

        private static void setCopyEmote(bool enabled)
        {
            if (!enabled)
            {
                PacketMonitor.Off("se");
                return;
            }
            PacketMonitor.On("se", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID)
                {
                    int id = int.Parse(pack[5]);
                    LocalPlayer.SendEmote(id);
                }
            });
        }

        private static void setCopyMessage(bool enabled)
        {
            if (!enabled)
            {
                PacketMonitor.Off("sm");
                PacketMonitor.Off("ss");
                PacketMonitor.Off("sj");
                PacketMonitor.Off("sg");
                PacketMonitor.Off("sc");
                return;
            }
            PacketMonitor.On("sm", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID) sendPhraseChatMessageByText(pack[5]);
            });
            PacketMonitor.On("ss", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID)
                {
                    Core.ExecuteFunction("INTERFACE.sendSafeMessage", pack[5]);
                }
            });
            PacketMonitor.On("sj", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID)
                {
                    int id = int.Parse(pack[5]);
                    LocalPlayer.Message.Joke(id);
                }
            });
            PacketMonitor.On("sg", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID) LocalPlayer.Message.TourGuideMessage();
            });
            PacketMonitor.On("sc", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                if (pack[4] == TargetID) LocalPlayer.Message.Say(pack[5]);
            });
        }

        public static bool CopyMove
        {
            get { return copyMove; }
            set
            {
                if (Active) setCopyMove(value);
                copyMove = value;
            }
        }

        public static bool CopyFrame
        {
            get { return copyFrame; }
            set
            {
                if (Active) setCopyFrame(value);
                copyFrame = value;
            }
        }

        public static bool CopySnowBall
        {
            get { return copySnowBall; }
            set
            {
                if (Active) setCopySnowBall(value);
                copySnowBall = value;
            }
        }

        public static bool CopyEmote
        {
            get { return copyEmote; }
            set
            {
                if (Active) setCopyEmote(value);
                copyEmote = value;
            }
        }

        public static bool CopyMessage
        {
            get { return copyMessage; }
            set
            {
                if (Active) setCopyMessage(value);
                copyMessage = value;
            }
        }

        private static async void sendPhraseChatMessageByText(string text)
        {
            Core.ExecuteFunction("INTERFACE.handlePhraseAutocompleteText", text);
            string pcid = await pcc.GetPCID(text);
            if (pcid != null) sendPhraseChatMessage(pcid);
            else Core.ExecuteFunction("INTERFACE.sendMessage", text);
        }

        private static void sendPhraseChatMessage(string pcid)
        {
            Core.SendPacketAsync("%xt%s%m#pcam%" + Room.InternalID + "%" + pcid + "%", null);
        }

        private static void sendMove(int x, int y)
        {
            if (x >= 0 && y >= 0)
            {
                LocalPlayer.Move(x + offsetX, y + offsetY);
            }
        }

        private static void followPath(string pathID)
        {
            Core.SendPacketAsync("%xt%s%u#followpath%" + Room.InternalID + "%" + pathID + "%", null);
        }

        private static void teleport(int x, int y)
        {
            Core.ExecuteFunction("ENGINE.sendPlayerTeleport", x + offsetX, y + offsetY);
        }

        private static void sendCheckLocation()
        {
            Core.ExecuteFunction("AuxSWF.setPlayerLocationPromptsEnabled", false);
            Core.SendPacketAsync("%xt%s%u#bf%" + Room.InternalID + "%" + TargetID + "%", null);
        }

        private static void setTriggersEnabled(bool enabled)
        {
            Core.SetVarAsync("ENGINE.is_mouse_active", enabled, null);
        }
    }
}
