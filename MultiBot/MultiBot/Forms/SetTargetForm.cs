﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class SetTargetForm : CuPForm
    {
        public SetTargetForm()
        {
            InitializeComponent();
        }

        private void setTarget(string id, string name)
        {
            AddInBase.form.SetTarget(id, name, false);
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string text = textBox1.Text;
            int id;
            if (int.TryParse(text, out id) && id > 0)
            {
                PacketMonitor.On("pbi", (packet, mode) =>
                {
                    string[] Packet = packet.Split('%');
                    if (Packet[5] == text)
                    {
                        PacketMonitor.Off("pbi");
                        setTarget(text, Packet[6]);
                    }
                });
                Core.SendPacket("%xt%s%u#pbi%-1%" + text + "%");
            }
            else
            {
                PacketMonitor.On("pbn", (packet, mode) =>
                {
                    string[] Packet = packet.Split('%');
                    if (Packet[5] != "")
                    {
                        PacketMonitor.Off("pbn");
                        setTarget(Packet[5], Packet[6]);
                    }
                    else MessageBox.Show("Wrong name!", "MultiBot - Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                });
                Core.SendPacket("%xt%s%u#pbn%-1%" + text + "%");
            }
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            setTarget(LocalPlayer.Id.ToString(), MultiBot.LocalPlayerDisplayName);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string playerCardId = Core.ExecuteFunction("INTERFACE.getActivePlayerId").RawResult;
            int id;
            if (!int.TryParse(playerCardId, out id) || id <= 0)
            {
                MessageBox.Show("No player card open!", "MultiBot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            setTarget(playerCardId, Core.ExecuteFunction("SHELL.getNicknameById", id).RawResult);
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                button1_Click(null, null);
            }
        }
    }
}
