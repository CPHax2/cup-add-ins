﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class ActionForm : Cup.Extensibility.Library.CuPForm
    {
        private FollowBot[] bots;
        private List<Label> labelList = new List<Label>();
        private List<TextBox> textBoxList = new List<TextBox>();
        private static ActionType[] actions = new ActionType[]
        {
            new ActionType("Send Move with Offset", "Send", new string[] { "X", "Y" }, new ActionType.ActionDelegate((bots, args) =>
            {
                int x = -1, y = -1;
                if (!int.TryParse(args[0], out x) || !int.TryParse(args[1], out y))
                {
                    MessageBox.Show("Invalid coordinates!", "MultiBot - Action Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                foreach(FollowBot bot in bots) bot.Move(x + bot.OffsetX, y + bot.OffsetY);
            })),
            new ActionType("Send Move", "Send", new string[] { "X", "Y" }, new ActionType.ActionDelegate((bots, args) =>
            {
                int x = -1, y = -1;
                if (!int.TryParse(args[0], out x) || !int.TryParse(args[1], out y))
                {
                    MessageBox.Show("Invalid coordinates!", "MultiBot - Action Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                foreach(FollowBot bot in bots) bot.Move(x, y);
            })),
            new ActionType("Send Frame", "Send", new string[] { "ID" }, new ActionType.ActionDelegate((bots, args) =>
            {
                foreach(FollowBot bot in bots) bot.SendFrame(args[0]);
            })),
            new ActionType("Throw Snowball", "Throw", new string[] { "X", "Y" }, new ActionType.ActionDelegate((bots, args) =>
            {
                foreach(FollowBot bot in bots) bot.ThrowSnowball(args[0], args[1]);
            })),
            new ActionType("Send Emote", "Send", new string[] { "ID" }, new ActionType.ActionDelegate((bots, args) =>
            {
                foreach(FollowBot bot in bots) bot.SendEmote(args[0]);
            })),
            new ActionType("Send Message", "Send", new string[] { "Message" }, new ActionType.ActionDelegate((bots, args) =>
            {
                foreach(FollowBot bot in bots) bot.SendPhraseChatMessageByText(args[0]);
            })),
            new ActionType("Send Joke", "Send", new string[] { "ID" }, new ActionType.ActionDelegate((bots, args) =>
            {
                foreach(FollowBot bot in bots) bot.SendJoke(args[0]);
            }))
        };
        
        public ActionForm(FollowBot[] list)
        {
            InitializeComponent();
            foreach (ActionType actionType in actions) comboBox1.Items.Add(actionType);
            comboBox1.SelectedIndex = 0;
            bots = list;
            Text = "Action (" + (list.Length > 1 ? list.Length + " bots" : list[0].Name) + ")";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ActionType at = comboBox1.SelectedItem as ActionType;
            string[] args = new string[at.ArgNames.Length];
            for (int i = 0; i < args.Length; i++)
            {
                args[i] = textBoxList[i].Text;
            }
            at.Action(bots, args);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Clear();
            ActionType at = comboBox1.SelectedItem as ActionType;
            foreach (Label l in labelList) l.Dispose();
            labelList.Clear();
            foreach (TextBox tb in textBoxList) tb.Dispose();
            textBoxList.Clear();
            int nextLocation = 12, nextTabIndex = 2;
            foreach (string s in at.ArgNames)
            {
                Label l = new Label();
                l.Text = s + ":";
                l.Location = new Point(nextLocation, 44);
                l.Parent = this;
                l.AutoSize = true;
                l.TabIndex = nextTabIndex++;
                labelList.Add(l);
                TextBox tb = new TextBox();
                tb.Location = new Point(l.Location.X + l.Width + 6, 41);
                tb.Width = s == "Message" ? (Width - tb.Location.X - 18) : 40;
                tb.KeyDown += textBox_KeyDown;
                tb.Parent = this;
                tb.TabIndex = nextTabIndex++;
                textBoxList.Add(tb);
                nextLocation = tb.Location.X + tb.Width + 6;
            }
            button1.Text = at.ButtonText;
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(null, null);
                e.SuppressKeyPress = true;
            }
        }
    }

    class ActionType
    {
        public ActionType(string name, string buttonText, string[] argNames, ActionDelegate action)
        {
            Name = name;
            ButtonText = buttonText;
            ArgNames = argNames;
            Action = action;
        }

        public string Name { get; private set; }
        public string ButtonText { get; private set; }
        public string[] ArgNames { get; private set; }
        public delegate void ActionDelegate(FollowBot[] bots, params string[] arguments);
        public ActionDelegate Action { get; private set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
