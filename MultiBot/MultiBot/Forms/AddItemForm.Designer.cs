﻿namespace CuPAddIn
{
    partial class AddItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.addedLabel = new System.Windows.Forms.Label();
            this.notEnoughCoinsLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.memberLabel = new System.Windows.Forms.Label();
            this.sentLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Item ID:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(121, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(62, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(53, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // addedLabel
            // 
            this.addedLabel.AutoSize = true;
            this.addedLabel.Location = new System.Drawing.Point(12, 80);
            this.addedLabel.Name = "addedLabel";
            this.addedLabel.Size = new System.Drawing.Size(50, 13);
            this.addedLabel.TabIndex = 3;
            this.addedLabel.Text = "Added: 0";
            this.addedLabel.Visible = false;
            // 
            // notEnoughCoinsLabel
            // 
            this.notEnoughCoinsLabel.AutoSize = true;
            this.notEnoughCoinsLabel.Location = new System.Drawing.Point(12, 100);
            this.notEnoughCoinsLabel.Name = "notEnoughCoinsLabel";
            this.notEnoughCoinsLabel.Size = new System.Drawing.Size(103, 13);
            this.notEnoughCoinsLabel.TabIndex = 4;
            this.notEnoughCoinsLabel.Text = "Not enough coins: 0";
            this.notEnoughCoinsLabel.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Adding item to X accounts:";
            this.label4.Visible = false;
            // 
            // memberLabel
            // 
            this.memberLabel.AutoSize = true;
            this.memberLabel.Location = new System.Drawing.Point(12, 120);
            this.memberLabel.Name = "memberLabel";
            this.memberLabel.Size = new System.Drawing.Size(117, 13);
            this.memberLabel.TabIndex = 6;
            this.memberLabel.Text = "Membership required: 0";
            this.memberLabel.Visible = false;
            // 
            // sentLabel
            // 
            this.sentLabel.AutoSize = true;
            this.sentLabel.Location = new System.Drawing.Point(12, 60);
            this.sentLabel.Name = "sentLabel";
            this.sentLabel.Size = new System.Drawing.Size(41, 13);
            this.sentLabel.TabIndex = 7;
            this.sentLabel.Text = "Sent: 0";
            this.sentLabel.Visible = false;
            // 
            // AddItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(208, 47);
            this.Controls.Add(this.sentLabel);
            this.Controls.Add(this.memberLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.notEnoughCoinsLabel);
            this.Controls.Add(this.addedLabel);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddItemForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Item";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label addedLabel;
        private System.Windows.Forms.Label notEnoughCoinsLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label memberLabel;
        private System.Windows.Forms.Label sentLabel;
    }
}