﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class EditForm : CuPForm
    {
        private ListViewItem[] items;
        private bool editCredentials = false;
        public EditForm(ListViewItem[] list)
        {
            InitializeComponent();
            items = list;
            if (list.Length == 1 && ((list[0].Tag as FollowBot).Status == PCL.PCLStatus.Offline || (list[0].Tag as FollowBot).Status == PCL.PCLStatus.Error))
            {
                usernameTextBox.Text = (list[0].Tag as FollowBot).Name;
                editCredentials = true;
            }
            else
            {
                if (list.Length > 1) Text = "Edit " + list.Length + " Bots";
                label3.Visible = false;
                usernameTextBox.Visible = false;
                label4.Visible = false;
                passwordTextBox.Visible = false;
                Height = 102;
            }
            int x = ((FollowBot)list[0].Tag).OffsetX;
            int y = ((FollowBot)list[0].Tag).OffsetY;
            foreach (ListViewItem lvi in list)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.OffsetX != x)
                {
                    x = 0;
                    break;
                }
            }
            foreach (ListViewItem lvi in list)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.OffsetY != y)
                {
                    y = 0;
                    break;
                }
            }
            xTextBox.Text = x.ToString();
            yTextBox.Text = y.ToString();
            AuxSWF.OnMessage = (msg) =>
            {
                string[] parts = msg.Split('%');
                xTextBox.Text = parts[0];
                yTextBox.Text = parts[1];
            };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FollowBot newBot = null;
            if (editCredentials)
            {
                if (usernameTextBox.Text.Length < 4)
                {
                    MessageBox.Show("Username too short!", "Multi Bot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (passwordTextBox.Text.Length > 0 && passwordTextBox.Text.Length < 4)
                {
                    MessageBox.Show("Password too short!", "Multi Bot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                FollowBot bot = items[0].Tag as FollowBot;
                if (usernameTextBox.Text != bot.Name || passwordTextBox.Text.Length > 0)
                {
                    bool keepPassword = passwordTextBox.Text.Length == 0;
                    newBot = new FollowBot(usernameTextBox.Text, keepPassword ? bot.PasswordHash : passwordTextBox.Text, keepPassword, bot.OffsetX, bot.OffsetY);
                    if (usernameTextBox.Text.ToLower() != bot.Name.ToLower() && AddInBase.form.GetIndex(newBot) >= 0)
                    {
                        MessageBox.Show("Penguin is already on the list!", "Multi Bot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    newBot.OnStatusChanged = bot.OnStatusChanged;
                    newBot.OnError = bot.OnError;
                }
            }
            int x, y;
            if (!int.TryParse(xTextBox.Text, out x) || !int.TryParse(yTextBox.Text, out y))
            {
                MessageBox.Show("Invalid offset!", "Multi Bot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (newBot != null)
            {
                items[0].Tag = newBot;
                if (sender as string == "credential") newBot.Connect();
            }
            foreach (ListViewItem item in items)
            {
                ((FollowBot)item.Tag).OffsetX = x;
                ((FollowBot)item.Tag).OffsetY = y;
            }
            AddInBase.form.EditBot(items);
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Core.ExecuteFunction("AuxSWF.activateCrosshair");
        }

        private void EditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            AuxSWF.OnMessage = null;
            Core.ExecuteFunction("AuxSWF.hideCrosshair");
        }

        private void credentialTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click("credential", null);
                e.SuppressKeyPress = true;
            }
        }

        private void offsetTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click("offset", null);
                e.SuppressKeyPress = true;
            }
        }
    }
}
