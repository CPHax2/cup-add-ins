﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class MainForm : CuPForm
    {
        public MainForm()
        {
            InitializeComponent();
            label3.Text = "";
            Cup.Extensibility.Library.Server.OnChanged = () =>
            {
#if DEBUG
                Log.Write("Add-In", "Server changed", Log.Type.Info);
#endif
                Server s = Servers.CurrentServer;
                foreach (ListViewItem item in listView1.Items)
                {
                    FollowBot bot = (FollowBot)item.Tag;
                    if (bot.Status == PCL.PCLStatus.Online || bot.Status == PCL.PCLStatus.Connecting) bot.Reconnect(s.IP, s.Port);
                }
            };
            SelfBot.OnStart = (targetOnline) =>
            {
                OnSelfBotStart(targetOnline);
            };
            SelfBot.OnRoomTypeChanged = (roomType) =>
            {
                OnSelfBotUpdate(roomType);
            };
            SelfBot.OnLogoff = () =>
            {
                OnSelfBotStop();
            };
        }

        private bool isEditingOffset = false, isEditingTarget = false;
        private string selfBotTarget = "";
        private ListViewItem heldDownItem;

        public bool CopyMove { get { return checkBox1.Checked; } }
        public bool CopyFrame { get { return checkBox2.Checked; } }
        public bool CopySnowball { get { return checkBox3.Checked; } }
        public bool CopyEmote { get { return checkBox4.Checked; } }
        public bool CopyMessage { get { return checkBox5.Checked; } }

        private void updateStatus(ListViewItem lvi)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { updateStatus(lvi); });
                return;
            }
            Color statusColor;
            string statusMessage = "";
            switch ((lvi.Tag as FollowBot).Status)
            {
                case PCL.PCLStatus.Offline:
                    statusMessage = "Offline";
                    statusColor = Color.Black;
                    break;
                case PCL.PCLStatus.Connecting:
                    statusMessage = "Connecting";
                    statusColor = Color.Blue;
                    break;
                case PCL.PCLStatus.Online:
                    statusMessage = "Online";
                    statusColor = Color.Green;
                    break;
                case PCL.PCLStatus.Error:
                    statusColor = Color.Red;
                    break;
                default:
                    statusColor = Color.Black;
                    break;
            }
            lvi.SubItems[2].ForeColor = statusColor;
            if (statusMessage.Length > 0) lvi.SubItems[2].Text = statusMessage;
        }

        private void onError(ListViewItem lvi, string eId, string errorStr)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { onError(lvi, eId, errorStr); });
                return;
            }
            switch (eId)
            {
                case "-1":
                    lvi.SubItems[2].Text = errorStr;
                    break;
                case "-2":
                    lvi.SubItems[2].Text = "Connection error";
                    break;
                case "101":
                    lvi.SubItems[2].Text = "Wrong password";
                    break;
                case "601":
                    {
                        string duration = errorStr.Split('%')[5];
                        if (duration == "-1") duration = "Forever";
                        else duration += "h";
                        lvi.SubItems[2].Text = "Banned [" + duration + "]";
                    }
                    break;
                case "602":
                    lvi.SubItems[2].Text = "Banned [1h]";
                    break;
                case "603":
                    lvi.SubItems[2].Text = "Banned [Forever]";
                    break;
                case "610":
                case "611":
                    {
                        string duration = errorStr.Split('%')[6];
                        if (duration == "-1") duration = "Forever";
                        else duration += "h";
                        lvi.SubItems[2].Text = "Banned [" + duration + "]";
                    }
                    break;
                default:
                    return;
            }
            lvi.SubItems[2].ForeColor = Color.Red;
        }

        public void AddBot(FollowBot bot)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { AddBot(bot); });
                return;
            }
            ListViewItem lvi = new ListViewItem { Text = bot.Name, SubItems = { bot.OffsetX + ", " + bot.OffsetY, "" }, Tag = bot, UseItemStyleForSubItems = false };
            updateStatus(lvi);
            listView1.Items.Add(lvi);
            listView1.Columns[2].Width = -2;
            lvi.EnsureVisible();
            bot.OnStatusChanged = (status) =>
            {
                updateStatus(lvi);
            };
            bot.OnError = (eId, errorStr) =>
            {
                onError(lvi, eId, errorStr);
            };
        }

        public void AddBots(FollowBot[] bots)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { AddBots(bots); });
                return;
            }
            listView1.SuspendLayout();
            foreach (FollowBot bot in bots)
            {
                int index = GetIndex(bot);
                if (index == -1) AddBot(bot);
                else
                {
                    FollowBot listViewBot = (FollowBot)listView1.Items[index].Tag;
                    listViewBot.OffsetX = bot.OffsetX;
                    listViewBot.OffsetY = bot.OffsetY;
                    listView1.Items[index].SubItems[1].Text = bot.OffsetX + ", " + bot.OffsetY;
                }
            }
            listView1.ResumeLayout();
        }

        public void EditBot(ListViewItem[] list)
        {
            foreach (ListViewItem lvi in list)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (listView1.Items[lvi.Index].Text != bot.Name)
                {
                    listView1.Items[lvi.Index].Text = bot.Name;
                    updateStatus(lvi);
                }
                listView1.Items[lvi.Index].SubItems[1].Text = bot.OffsetX + ", " + bot.OffsetY;
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AddBotForm abf = new AddBotForm();
            abf.ShowDialog(this);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            RemoveSelected();
        }

        private void iglooLikeButton_Click(object sender, EventArgs e)
        {
            string id = Room.InternalID.ToString();
            foreach (ListViewItem item in listView1.Items)
            {
                FollowBot bot = (FollowBot)item.Tag;
                if (bot.Status == PCL.PCLStatus.Online) bot.LikeIgloo(id);
            }
        }

        public int GetIndex(FollowBot bot)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                if (((FollowBot)lvi.Tag).Name.ToLower() == bot.Name.ToLower()) return lvi.Index;
            }
            return -1;
        }        

        private void openFileButton_Click(object sender, EventArgs e)
        {
            MultiBot.OpenList();
        }
        
        private void saveFileButton_Click(object sender, EventArgs e)
        {
            FollowBot[] bots = new FollowBot[listView1.Items.Count];
            for (int i = 0; i < bots.Length; i++)
            {
                bots[i] = (FollowBot)listView1.Items[i].Tag;
            }
            MultiBot.SaveList(bots);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                ((FollowBot)lvi.Tag).CopyMove = checkBox1.Checked;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                ((FollowBot)lvi.Tag).CopyFrame = checkBox2.Checked;
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                ((FollowBot)lvi.Tag).CopySnowball = checkBox3.Checked;
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                ((FollowBot)lvi.Tag).CopyEmote = checkBox4.Checked;
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                ((FollowBot)lvi.Tag).CopyMessage = checkBox5.Checked;
            }
        }

        private void MainForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                contextMenuStrip1.Dispose();
                this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
                this.contextMenuStrip1.SuspendLayout();
                this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                this.actionToolStripMenuItem,
                this.connectToolStripMenuItem,
                this.editToolStripMenuItem,
                this.inventoryToolStripMenuItem,
                this.selectAllToolStripMenuItem,
                this.removeToolStripMenuItem});
                this.contextMenuStrip1.Name = "contextMenuStrip1";
                this.contextMenuStrip1.ShowImageMargin = false;
                this.contextMenuStrip1.Size = new System.Drawing.Size(163, 92);
                this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
                this.listView1.ContextMenuStrip = this.contextMenuStrip1;
                this.contextMenuStrip1.ResumeLayout(false);
                listView1.MouseUp -= listView1_MouseUp;
                listView1.MouseDown -= listView1_MouseDown;
                listView1.MouseMove -= listView1_MouseMove;
                listView1.MouseUp += listView1_MouseUp;
                listView1.MouseDown += listView1_MouseDown;
                listView1.MouseMove += listView1_MouseMove;
            }
        }

        private void SelectAll()
        {
            foreach (ListViewItem lvi in listView1.Items) lvi.Selected = true;
        }

        private void RemoveSelected()
        {
            int count = listView1.SelectedItems.Count;
            int[] toRemove = new int[count];
            for (int i = 0; i < count; i++) toRemove[i] = listView1.SelectedItems[i].Index;
            for (int i = count - 1; i >= 0; i--)
            {
                FollowBot bot = (FollowBot)listView1.Items[toRemove[i]].Tag;
                if(bot.Status == PCL.PCLStatus.Online || bot.Status == PCL.PCLStatus.Connecting) bot.LogOut();
                listView1.Items.RemoveAt(toRemove[i]);
            }
            listView1.Columns[2].Width = -2;
        }

        private void connectSelected()
        {
            foreach (ListViewItem lvi in listView1.SelectedItems)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.Status == PCL.PCLStatus.Online || bot.Status == PCL.PCLStatus.Connecting) continue;
                bot.Connect();
            }
        }

        private void disconnectSelected()
        {
            foreach (ListViewItem lvi in listView1.SelectedItems)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                bot.LogOut();
            }
        }

        private void ActionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<FollowBot> list = new List<FollowBot>();
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                FollowBot bot = item.Tag as FollowBot;
                if (bot.Status == PCL.PCLStatus.Online || bot.Status == PCL.PCLStatus.Connecting) list.Add(bot);
            }
            if (list.Count == 0)
            {
                MessageBox.Show("To perform an action you must select at least one active account!", "MultiBot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            new ActionForm(list.ToArray()).ShowDialog(this);
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PCL.PCLStatus status = ((FollowBot)listView1.FocusedItem.Tag).Status;
            if (status != PCL.PCLStatus.Online && status != PCL.PCLStatus.Connecting) connectSelected();
            else disconnectSelected();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListViewItem[] list = new ListViewItem[listView1.SelectedItems.Count];
            for (int i = 0; i < list.Length; i++) list[i] = listView1.SelectedItems[i];
            new EditForm(list).ShowDialog(this);
        }

        private void InventoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<FollowBot> list = new List<FollowBot>();
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                FollowBot bot = item.Tag as FollowBot;
                if (bot.Status == PCL.PCLStatus.Online) list.Add(bot);
            }
            if (list.Count == 0)
            {
                MessageBox.Show("To see inventory you must select at least one active account!", "MultiBot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            new InventoryForm(list.ToArray()).ShowDialog(this);
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectAll();
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveSelected();
        }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.A) SelectAll();
            }
            else if (e.Modifiers != Keys.Shift)
            {
                if (e.KeyCode == Keys.Delete) RemoveSelected();
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            bool selected = listView1.SelectedItems.Count > 0;
            if (selected)
            {
                PCL.PCLStatus status = ((FollowBot)listView1.FocusedItem.Tag).Status;
                contextMenuStrip1.Items[1].Text = (status != PCL.PCLStatus.Online && status != PCL.PCLStatus.Connecting) ? "Connect" : "Disconnect";
            }
            actionToolStripMenuItem.Enabled = selected;
            connectToolStripMenuItem.Enabled = selected;
            editToolStripMenuItem.Enabled = selected;
            inventoryToolStripMenuItem.Enabled = selected;
            selectAllToolStripMenuItem.Enabled = listView1.Items.Count > 0;
            removeToolStripMenuItem.Enabled = selected;
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.Status == PCL.PCLStatus.Online || bot.Status == PCL.PCLStatus.Connecting) continue;
                bot.Connect();
            }
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.Status != PCL.PCLStatus.Online && bot.Status != PCL.PCLStatus.Connecting) continue;
                bot.LogOut();
            }
        }

        private void musicLikeButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.Status == PCL.PCLStatus.Online) bot.LikeTrack(MultiBot.CurrentTrackInfo);
            }
        }

        private void blueprintButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.Status == PCL.PCLStatus.Online) bot.OpenBluePrints();
            }
        }

        private void newspaperButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.Status == PCL.PCLStatus.Online) bot.OpenNewspaper();
            }
        }

        private void closePrintButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.Status == PCL.PCLStatus.Online) bot.ClosePrints();
            }
        }

        private void randomColorsButton_Click(object sender, EventArgs e)
        {
            string[] colorIDs = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "15", "16" };
            Random r = new Random();
            foreach (ListViewItem lvi in listView1.Items)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.Status == PCL.PCLStatus.Online)
                {
                    string id = colorIDs[r.Next(colorIDs.Length)];
                    bot.UpdateColor(id);
                }
            }
        }

        public void SetTarget(string id, string name, bool selfBot)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { SetTarget(id, name, selfBot); });
                return;
            }
            if (selfBot)
            {
                if (id == LocalPlayer.Id.ToString())
                {
                    MessageBox.Show("You can not target yourself!", "MultiBot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                setEditTarget(false);
                if (id == SelfBot.TargetID) return;
                SelfBot.TargetID = id;
                if (name.Length == 0) name = id;
                selfBotTarget = name;
                label2.Text = "Target Player: " + name;
                if (!SelfBot.Active)
                {
                    startStopButton.Enabled = false;
                    editTargetButton.Enabled = false;
                    SelfBot.Begin();
                }
                return;
            }
            FollowBot.TargetID = id;
            label1.Text = "Target Player: " + (name != null ? name : id);
            foreach (ListViewItem lvi in listView1.Items)
            {
                FollowBot bot = (FollowBot)lvi.Tag;
                if (bot.Status == PCL.PCLStatus.Online) bot.CheckPlayerLocation(id);
            }
        }

        private void changeTargetButton_Click(object sender, EventArgs e)
        {
            new SetTargetForm().ShowDialog(this);
        }

        private void removeAllButton_Click(object sender, EventArgs e)
        {
            for (int i = listView1.Items.Count - 1; i >= 0; i--)
            {
                FollowBot bot = (FollowBot)listView1.Items[i].Tag;
                bot.LogOut();
                listView1.Items.RemoveAt(i);
            }
            listView1.Columns[2].Width = -2;
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (tabControl1.SelectedTab == tabPage3)
            {
                setEditTarget(false);
                setEditOffset(false);
            }
        }

        private void OnSelfBotStart(bool targetOnline)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { OnSelfBotStart(targetOnline); });
                return;
            }
            startStopButton.Enabled = true;
            editTargetButton.Enabled = true;
            OnSelfBotUpdate(SelfBot.TargetRoomType);
            if (targetOnline) startStopButton.Text = "Stop";
            else MessageBox.Show("Target player is offline.", "Self Bot", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void OnSelfBotUpdate(SelfBot.RoomType roomType)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { OnSelfBotUpdate(roomType); });
                return;
            }
            switch (roomType)
            {
                case SelfBot.RoomType.Game:
                    label3.Text = "Target player is in a game";
                    break;
                case SelfBot.RoomType.OwnIgloo:
                    label3.Text = "Target player is in their igloo";
                    break;
                case SelfBot.RoomType.OtherIgloo:
                    label3.Text = "Target player is in an igloo";
                    break;
                case SelfBot.RoomType.Backyard:
                    label3.Text = "Target player is in their backyard";
                    break;
                default:
                    label3.Text = "";
                    break;
            }
        }

        private void OnSelfBotStop()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { OnSelfBotStop(); });
                return;
            }
            label3.Text = "";
            startStopButton.Text = "Start";
            MessageBox.Show("Target player has logged off.", "Self Bot", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void editTargetButton_Click(object sender, EventArgs e)
        {
            if (!isEditingTarget) setEditTarget(true);
            else
            {
                string playerCardId = Core.ExecuteFunction("INTERFACE.getActivePlayerId").RawResult;
                int id;
                if (!int.TryParse(playerCardId, out id) || id <= 0)
                {
                    MessageBox.Show("No player card open!", "MultiBot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                SetTarget(id.ToString(), Core.ExecuteFunction("SHELL.getNicknameById", id).RawResult, true);
            }
        }

        private void startStopButton_Click(object sender, EventArgs e)
        {
            if (!SelfBot.Active)
            {
                setEditTarget(false);
                setEditOffset(false);
                if (SelfBot.TargetID != "")
                {
                    startStopButton.Enabled = false;
                    editTargetButton.Enabled = false;
                    SelfBot.Begin();
                }
                else MessageBox.Show("No target player selected!", "Self Bot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                label3.Text = "";
                startStopButton.Text = "Start";
                SelfBot.Stop();
            }
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            SelfBot.CopyMove = checkBox7.Checked;
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            SelfBot.CopyFrame = checkBox8.Checked;
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            SelfBot.CopySnowBall = checkBox9.Checked;
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            SelfBot.CopyEmote = checkBox10.Checked;
        }

        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {
            SelfBot.CopyMessage = checkBox11.Checked;
        }

        private void editOffsetButton_Click(object sender, EventArgs e)
        {
            setEditOffset(!isEditingOffset);
        }

        private void setOffsetButton_Click(object sender, EventArgs e)
        {
            if (!isEditingOffset)
            {
                SelfBot.SetOffset(0, 0);
                label7.Text = "X: 0";
                label8.Text = "Y: 0";
                return;
            }
            int x, y;
            if (!int.TryParse(textBox1.Text, out x) || !int.TryParse(textBox2.Text, out y) || x > 700 || y > 400 || x < -700 | y < -400)
            {
                MessageBox.Show("Invalid offset values!", "Self Bot", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            SelfBot.SetOffset(x, y);
            label7.Text = "X: " + x;
            label8.Text = "Y: " + y;
            setEditOffset(false);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabPage3) Height = 250;
            else Height = 481;
        }

        private void setTargetButton_Click(object sender, EventArgs e)
        {
            string text = textBox3.Text;
            int id;
            if (int.TryParse(text, out id) && id > 0)
            {
                if (text == SelfBot.TargetID)
                {
                    setEditTarget(false);
                    return;
                }
                PacketMonitor.On("pbi", (packet, mode) =>
                {
                    string[] Packet = packet.Split('%');
                    if (Packet[5] == text)
                    {
                        PacketMonitor.Off("pbi");
                        SetTarget(text, Packet[6], true);
                    }
                });
                Core.SendPacket("%xt%s%u#pbi%-1%" + text + "%");
            }
            else
            {
                if (text.Length == 0 || text.ToLower() == selfBotTarget.ToLower())
                {
                    setEditTarget(false);
                    return;
                }
                PacketMonitor.On("pbn", (packet, mode) =>
                {
					PacketMonitor.Off("pbn");
                    string[] Packet = packet.Split('%');
                    if (Packet[5] != "") SetTarget(Packet[5], Packet[6], true);
                    else MessageBox.Show("Wrong name!", "MultiBot - Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                });
                Core.SendPacket("%xt%s%u#pbn%-1%" + text + "%");
            }
        }

        private void numericTextBoxCheck(TextBox textBox, KeyEventArgs e)
        {
            if ((e.Modifiers == Keys.Control) || (e.Modifiers == (Keys.Control | Keys.Shift)))
            {
                if (e.KeyCode != Keys.A && e.KeyCode != Keys.C && e.KeyCode != Keys.V && e.KeyCode != Keys.Z && e.KeyCode != Keys.Y && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else if (e.Modifiers == Keys.Shift)
            {
                if (e.KeyCode != Keys.Left && e.KeyCode != Keys.Right) e.SuppressKeyPress = true;
            }
            else
            {
                if (e.KeyCode == Keys.OemMinus || e.KeyCode == Keys.Subtract)
                {
                    if ((textBox.Text.Length != 0) && ((textBox.SelectionStart != 0) || ((textBox.Text[0] == '-') && (textBox.SelectionLength == 0)))) e.SuppressKeyPress = true;
                }
                else if ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9))
                {
                    if ((textBox.Text.Length >= 3) && (textBox.Text[0] != '-') && (textBox.SelectionLength == 0)) e.SuppressKeyPress = true;
                    if ((textBox.Text.Length != 0) && (textBox.Text[0] == '-') && (textBox.SelectionStart == 0) && (textBox.SelectionLength == 0)) e.SuppressKeyPress = true;
                }
                else if ((e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete) && (e.KeyCode != Keys.Left) && (e.KeyCode != Keys.Right)) e.SuppressKeyPress = true;
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                textBox2.Focus();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                setEditOffset(false);
            }
            numericTextBoxCheck(textBox1, e);
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                setOffsetButton_Click(null, null);
                editOffsetButton.Focus();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                setEditOffset(false);
            }
            numericTextBoxCheck(textBox2, e);
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                setTargetButton_Click(null, null);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                setEditTarget(false);
            }
        }

        private void setEditTarget(bool enabled)
        {
            if (enabled)
            {
                editTargetButton.Text = "Player Card";
                textBox3.Text = selfBotTarget;
                textBox3.Visible = true;
                setTargetButton.Visible = true;
                textBox3.Focus();
            }
            else
            {
                editTargetButton.Text = "Edit Target";
                textBox3.Visible = false;
                setTargetButton.Visible = false;
            }
            isEditingTarget = enabled;
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1 && ((FollowBot)listView1.SelectedItems[0].Tag).Status == PCL.PCLStatus.Error ||
               ((FollowBot)listView1.SelectedItems[0].Tag).Status == PCL.PCLStatus.Offline) editToolStripMenuItem_Click(null, null);
            else ActionToolStripMenuItem_Click(null, null);
        }

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left || listView1.SelectedItems.Count < 1) return;
            listView1.AutoArrange = false;
            heldDownItem = listView1.GetItemAt(e.X, e.Y);
        }

        private void listView1_MouseMove(object sender, MouseEventArgs e)
        {
            if (heldDownItem == null) return;
            ListViewItem newItem = listView1.GetItemAt(e.X, e.Y);
            if (newItem == null) return;
            int change = newItem.Index - heldDownItem.Index;
            int count = listView1.Items.Count, selectedCount = listView1.SelectedItems.Count;
            if (listView1.SelectedItems[0].Index + change < 0)
            {
                change = -listView1.SelectedItems[0].Index;
            }
            else if (listView1.SelectedItems[selectedCount - 1].Index + change >= count)
            {
                change = count - listView1.SelectedItems[selectedCount - 1].Index - 1;
            }
            if (change == 0) return;
            listView1.BeginUpdate();
            if (change < 0)
            {
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {
                    ListViewItem item = listView1.SelectedItems[i];
                    int index = item.Index;
                    listView1.Items.RemoveAt(index);
                    listView1.Items.Insert(index + change, item);
                }
            }
            else
            {
                for (int i = listView1.SelectedItems.Count - 1; i >= 0; i--)
                {
                    ListViewItem item = listView1.SelectedItems[i];
                    int index = item.Index;
                    listView1.Items.RemoveAt(index);
                    listView1.Items.Insert(index + change, item);
                }
            }
            listView1.EndUpdate();
        }

        private void listView1_MouseUp(object sender, MouseEventArgs e)
        {
            heldDownItem = null;
            listView1.AutoArrange = true;
        }

        private void setEditOffset(bool enabled)
        {
            if (enabled)
            {
                editOffsetButton.Text = "Cancel";
                setOffsetButton.Text = "Set";
                textBox1.Text = SelfBot.OffsetX.ToString();
                textBox2.Text = SelfBot.OffsetY.ToString();
                textBox1.Visible = true;
                textBox2.Visible = true;
                textBox1.Focus();
            }
            else
            {
                editOffsetButton.Text = "Edit";
                setOffsetButton.Text = "Reset";
                if (groupBox3.ContainsFocus) editOffsetButton.Focus();
                textBox1.Visible = false;
                textBox2.Visible = false;
            }
            isEditingOffset = enabled;
        }
    }
}
