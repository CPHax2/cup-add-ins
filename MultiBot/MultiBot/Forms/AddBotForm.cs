﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class AddBotForm : CuPForm
    {
        public AddBotForm()
        {
            InitializeComponent();
            checkBox1.Checked = MultiBot.AutoConnect;
            AuxSWF.OnMessage = (msg) =>
            {
                string[] parts = msg.Split('%');
                xTextBox.Text = parts[0];
                yTextBox.Text = parts[1];
            };
        }

        private void setEnabled(bool enable)
        {
            button1.Enabled = enable;
            textBox1.Enabled = enable;
            textBox2.Enabled = enable;
            xTextBox.Enabled = enable;
            yTextBox.Enabled = enable;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length < 4)
            {
                MessageBox.Show("Username too short!", "Multi Bot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (textBox2.Text.Length < 4)
            {
                MessageBox.Show("Password too short!", "Multi Bot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            int x = 0, y = 0;
            if (!int.TryParse(xTextBox.Text, out x) || !int.TryParse(yTextBox.Text, out y))
            {
                MessageBox.Show("Invalid offset!", "Multi Bot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            FollowBot bot = new FollowBot(textBox1.Text, textBox2.Text, false, x, y);
            if (AddInBase.form.GetIndex(bot) >= 0)
            {
                MessageBox.Show("Penguin is already on the list!", "Multi Bot - Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            AddInBase.form.AddBot(bot);
            if (MultiBot.AutoConnect) bot.Connect();
            Close();
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(null, null);
                e.SuppressKeyPress = true;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            MultiBot.AutoConnect = checkBox1.Checked;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Core.ExecuteFunction("AuxSWF.activateCrosshair");
        }

        private void AddBotForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            AuxSWF.OnMessage = null;
            Core.ExecuteFunction("AuxSWF.hideCrosshair");
        }
    }
}
