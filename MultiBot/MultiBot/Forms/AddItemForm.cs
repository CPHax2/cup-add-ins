﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class AddItemForm : Cup.Extensibility.Library.CuPForm
    {
        private InventoryForm invForm;
        private FollowBot[] bots;
        private int addedCount = 0, notEnoughCoinsCount = 0, membershipRequiredCount = 0, responseCount = 0, sent = 0;
        private List<FollowBot> itemAddedBots = new List<FollowBot>();

        public AddItemForm(FollowBot[] bots, InventoryForm sender)
        {
            InitializeComponent();
            this.bots = bots;
            invForm = sender;
        }

        private void addAvailableItem(string itemId, FollowBot[] bots)
        {
            foreach (FollowBot bot in bots)
            {
                bot.OnPacket("ai", (packet) =>
                {
                    bot.OnPacket("ai", null);
                    bot.OnPacket("e", null);
                    updateAddedCount();
                    itemAddedBots.Add(bot);
                    if (++responseCount == bots.Length) onAddFinished(itemId);
                });
                bot.OnPacket("e", (packet) =>
                {
                    string errorCode = packet.Split('%')[4];
                    bool isItemAddError = true;
                    switch (errorCode)
                    {
                        case "402": // item unavailable
                        case "410":
                            break;
                        case "400": // item already in inventory
                            break;
                        case "401": // not enough coins
                            updateNotEnoughCoinsCount();
                            break;
                        case "999": // membership required
                            updateMembershipRequiredCount();
                            break;
                        default:
                            isItemAddError = false;
                            break;
                    }
                    if (!isItemAddError) return;
                    bot.OnPacket("ai", null);
                    bot.OnPacket("e", null);
                    if (++responseCount == bots.Length) onAddFinished(itemId);
                });
                bot.AddItem(itemId);
                updateSentCount();
            }
        }

        private void tryAddItem(string itemId, FollowBot[] bots)
        {
            if (bots.Length == 1) label4.Text = "Adding item to 1 account:";
            else label4.Text = "Adding item to " + bots.Length + " accounts:";
            showStatus(true);
            FollowBot bot = bots[0];
            bot.OnPacket("ai", (packet) =>
            {
                bot.OnPacket("ai", null);
                bot.OnPacket("e", null);
                itemAddedBots.Add(bot);
                updateAddedCount();
                if (bots.Length == 1) onAddFinished(itemId);
                else addAvailableItem(itemId, bots.Skip(1).ToArray());
            });
            bot.OnPacket("e", (packet) =>
            {
                string errorCode = packet.Split('%')[4];
                bool isItemAddError = true, available = true;
                switch (errorCode)
                {
                    case "402": // item unavailable
                    case "410":
                        available = false;
                        break;
                    case "400": // item already in inventory
                        break;
                    case "401": // not enough coins
                        updateNotEnoughCoinsCount();
                        break;
                    case "999": // membership required
                        updateMembershipRequiredCount();
                        break;
                    default:
                        isItemAddError = false;
                        available = false;
                        break;
                }
                if (!isItemAddError) return;
                bot.OnPacket("ai", null);
                bot.OnPacket("e", null);
                if (!available)
                {
                    MessageBox.Show("Item not available!", "Multibot", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    onAddFinished(itemId);
                    return;
                }
                if (bots.Length == 1) onAddFinished(itemId);
                else addAvailableItem(itemId, bots.Skip(1).ToArray());
            });
            bot.AddItem(itemId);
            updateSentCount();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            updateAddedCount(true);
            updateNotEnoughCoinsCount(true);
            updateMembershipRequiredCount(true);
            updateSentCount(true);
            responseCount = 0;
            string itemId = textBox1.Text;
            if (!Items.IsSafe(itemId))
            {
                showStatus(false);
                MessageBox.Show("Can not add item!", "Multibot", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            itemAddedBots.Clear();
            button1.Enabled = false;
            textBox1.Enabled = false;
            InventoryForm.InventoryItem i = invForm.GetItemById(itemId);
            if (i == null || i.Bots == null)
            {
                tryAddItem(itemId, bots);
                return;
            }
            List<FollowBot> botsWithItem = i.Bots;
            List<FollowBot> botsWithoutItem = new List<FollowBot>();
            foreach (FollowBot bot in bots)
            {
                if (botsWithItem.IndexOf(bot) < 0) botsWithoutItem.Add(bot);
            }
            if (botsWithoutItem.Count > 0) tryAddItem(itemId, botsWithoutItem.ToArray());
            else
            {
                showStatus(false);
                MessageBox.Show("Item already in inventory!", "Multibot", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                onAddFinished(itemId);
            }
        }

        private void onAddFinished(string itemId)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { onAddFinished(itemId); });
                return;
            }
            if (itemAddedBots.Count > 0)
            {
                invForm.AddInventoryItem(itemId, itemAddedBots);
            }
            button1.Enabled = true;
            textBox1.Enabled = true;
            textBox1.Focus();
        }

        private void showStatus(bool show)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { showStatus(show); });
                return;
            }
            if (show)
            {
                label4.Visible = true;
                sentLabel.Visible = true;
                addedLabel.Visible = true;
                notEnoughCoinsLabel.Visible = true;
                memberLabel.Visible = true;
                Height = 171;
            }
            else
            {
                label4.Visible = false;
                sentLabel.Visible = false;
                addedLabel.Visible = false;
                notEnoughCoinsLabel.Visible = false;
                memberLabel.Visible = false;
                Height = 76;
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                button1_Click(null, null);
            }
        }

        private void updateAddedCount(bool reset = false)
        {
            if(InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { updateAddedCount(reset); });
                return;
            }
            addedCount = reset ? 0 : addedCount + 1;
            addedLabel.Text = "Added: " + addedCount;
        }

        private void updateNotEnoughCoinsCount(bool reset = false)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { updateNotEnoughCoinsCount(reset); });
                return;
            }
            notEnoughCoinsCount = reset ? 0 : notEnoughCoinsCount + 1;
            notEnoughCoinsLabel.Text = "Not enough coins: " + notEnoughCoinsCount;
        }

        private void updateMembershipRequiredCount(bool reset = false)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { updateMembershipRequiredCount(reset); });
                return;
            }
            membershipRequiredCount = reset ? 0 : membershipRequiredCount + 1;
            memberLabel.Text = "Membership required: " + membershipRequiredCount;
        }

        private void updateSentCount(bool reset = false)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate { updateSentCount(reset); });
                return;
            }
            sent = reset ? 0 : sent + 1;
            sentLabel.Text = "Sent: " + sent;
        }
    }
}
