﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace CuPAddIn
{
    public partial class InventoryForm : Cup.Extensibility.Library.CuPForm
    {
        private FollowBot[] bots;
        private List<InventoryItem> itemList = new List<InventoryItem>(), selectedTypeItems = new List<InventoryItem>();
        private PictureBox[] pictureBoxes;
        private int totalPages = 1, currentPage = 1;

        public InventoryForm(FollowBot[] list)
        {
            InitializeComponent();
            
            pictureBoxes = new PictureBox[]
            {
                pictureBox1, pictureBox2, pictureBox3, pictureBox4, pictureBox5, pictureBox6,
                pictureBox7, pictureBox8, pictureBox9, pictureBox10, pictureBox11, pictureBox12
            };
            bots = list;
            Text = "Inventory (" + (list.Length > 1 ? list.Length + " bots" : list[0].Name) + ")";
            InitItemList();
            comboBox1.SelectedIndex = 10;
        }

        public class InventoryItem
        {
            public Items.Item Item { get; private set; }
            public List<FollowBot> Bots { get; private set; }

            public InventoryItem(string id)
            {
                Item = Items.GetItemById(id);
                Bots = new List<FollowBot>();
            }
        }

        public void AddInventoryItem(string itemId, List<FollowBot> bots)
        {
            InventoryItem item = GetItemById(itemId);
            if (item == null)
            {
                item = new InventoryItem(itemId);
                item.Bots.AddRange(bots);
                itemList.Insert(0, item);
            }
            else
            {
                item.Bots.AddRange(bots);
                itemList.Remove(item);
                itemList.Insert(0, item);
            }
            if (SelectedItemType != 0 && item.Item.Type != SelectedItemType) return;
            UpdateSelectedTypeItems();
            loadPage(1);
            foreach (PictureBox pb in pictureBoxes)
            {
                InventoryItem i = pb.Tag as InventoryItem;
                if (i != null && i.Item.ID == itemId)
                {
                    pb.Refresh();
                    break;
                }
            }
        }

        public InventoryItem GetItemById(string id)
        {
            foreach(InventoryItem i in itemList)
            {
                if (i.Item.ID == id) return i;
            }
            return null;
        }

        private void InitItemList()
        {
            for (int i = 0; i < bots.Length; i++)
            {
                if (bots[i].Items == null) continue;
                foreach (string itemId in bots[i].Items)
                {
                    InventoryItem item = GetItemById(itemId);
                    if (item == null)
                    {
                        item = new InventoryItem(itemId);
                        itemList.Add(item);
                    }
                    item.Bots.Add(bots[i]);
                }
            }
        }

        private void UpdateSelectedTypeItems()
        {
            selectedTypeItems.Clear();
            Items.Item.ItemType type = SelectedItemType;
            if (type == 0)
            {
                foreach (InventoryItem item in itemList) selectedTypeItems.Add(item);
            }
            else
            {
                foreach (InventoryItem item in itemList)
                {
                    if (item.Item.Type == type) selectedTypeItems.Add(item);
                }
            }
            totalPages = selectedTypeItems.Count > 0 ? (int)Math.Ceiling((double)selectedTypeItems.Count / pictureBoxes.Length) : 1;
        }

        private void loadPage(int page)
        {
            if (page < 1 || page > totalPages) return;
            currentPage = page;
            upButton.Enabled = currentPage > 1;
            downButton.Enabled = currentPage < totalPages;
            int startIndex = pictureBoxes.Length * (currentPage - 1);
            int maxItems = Math.Min(pictureBoxes.Length, selectedTypeItems.Count - startIndex);
            label1.Text = "Page " + currentPage + " of " + totalPages;
            label1.Location = new Point(pictureBox2.Location.X + (pictureBox2.Width - label1.Width) / 2 - 2, label1.Location.Y);
            for (int i = 0; i < maxItems; i++)
            {
                PictureBox pb = pictureBoxes[i];
                InventoryItem item = selectedTypeItems[startIndex + i];
                pb.Tag = item;
                toolTip1.SetToolTip(pb, item.Item.Name + " (" + item.Item.ID + ")");
                loadPicture(pb);
            }
            for (int i = maxItems; i < pictureBoxes.Length; i++)
            {
                PictureBox pb = pictureBoxes[i];
                pb.Tag = null;
                pb.Image = null;
                pb.BackgroundImage = Properties.Resources.BG_no_item;
                toolTip1.SetToolTip(pb, null);
            }
        }

        private async void loadPicture(PictureBox pb)
        {
            InventoryItem item = pb.Tag as InventoryItem;
            if (item == null)
            {
                pb.Image = null;
                pb.BackgroundImage = Properties.Resources.BG_no_item;
                return;
            }
            pb.BackgroundImage = Properties.Resources.BG_normal;
            pb.Image = Properties.Resources.loading;
            pb.Image = await GetImageById(item.Item.ID);
            if (pb.Tag as InventoryItem == null) pb.Image = null;
        }

        private async System.Threading.Tasks.Task<Image> GetImageById(string itemId)
        {
            string url = "http://media1.clubpenguin.com/game/items/images/paper/icon/60/" + itemId + ".png";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync());
                System.IO.Stream stream = response.GetResponseStream();
                return Image.FromStream(stream);
            }
            catch
            {
                url = "http://media8.clubpenguin.com/game/items/images/paper/icon/60/" + itemId + ".png";
                request = (HttpWebRequest)WebRequest.Create(url);
                try
                {
                    HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync());
                    System.IO.Stream stream = response.GetResponseStream();
                    return Image.FromStream(stream);
                }
                catch { return Properties.Resources.no_image; }
            }
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (bots.Length < 2) return;
            PictureBox pb = sender as PictureBox;
            if (pb == null) return;
            InventoryItem item = pb.Tag as InventoryItem;
            if (item == null) return;
            Graphics g = e.Graphics;
            Bitmap bm = Properties.Resources.count;
            g.DrawImage(bm, new Point(pb.Width - bm.Width, 0));
            string count = item.Bots.Count.ToString();
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            g.DrawString(count, Font, Brushes.White, new Rectangle(new Point(pb.Width - bm.Width, 0), bm.Size), sf);
        }

        private void pictureBox_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            InventoryItem item = pb.Tag as InventoryItem;
            if (item == null) return;
            pb.BackgroundImage = Properties.Resources.BG_selected;
            pb.Cursor = Cursors.Hand;
        }

        private void pictureBox_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            InventoryItem item = pb.Tag as InventoryItem;
            if (item == null) return;
            pb.BackgroundImage = Properties.Resources.BG_normal;
            pb.Cursor = Cursors.Default;
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            PictureBox pb = sender as PictureBox;
            InventoryItem item = pb.Tag as InventoryItem;
            if (item == null) return;
            Items.Item i = Items.GetItemById(item.Item.ID);
            foreach (FollowBot bot in item.Bots)
            {
                switch (i.Type)
                {
                    case Items.Item.ItemType.Color:
                        bot.UpdateColor(i.ID);
                        break;
                    case Items.Item.ItemType.Head:
                        bot.UpdateHeadItem(i.ID);
                        break;
                    case Items.Item.ItemType.Face:
                        bot.UpdateFaceItem(i.ID);
                        break;
                    case Items.Item.ItemType.Neck:
                        bot.UpdateNeckItem(i.ID);
                        break;
                    case Items.Item.ItemType.Body:
                        bot.UpdateBodyItem(i.ID);
                        break;
                    case Items.Item.ItemType.Hand:
                        bot.UpdateHandItem(i.ID);
                        break;
                    case Items.Item.ItemType.Feet:
                        bot.UpdateFeetItem(i.ID);
                        break;
                    case Items.Item.ItemType.Pin:
                        bot.UpdateFlag(i.ID);
                        break;
                    case Items.Item.ItemType.Background:
                        bot.UpdatePhoto(i.ID);
                        break;
                }
            }
        }

        private Items.Item.ItemType SelectedItemType
        {
            get
            {
                switch (comboBox1.SelectedIndex)
                {
                    case 0:
                        return Items.Item.ItemType.Head;
                    case 1:
                        return Items.Item.ItemType.Face;
                    case 2:
                        return Items.Item.ItemType.Neck;
                    case 3:
                        return Items.Item.ItemType.Body;
                    case 4:
                        return Items.Item.ItemType.Hand;
                    case 5:
                        return Items.Item.ItemType.Feet;
                    case 6:
                        return Items.Item.ItemType.Color;
                    case 7:
                        return Items.Item.ItemType.Pin;
                    case 8:
                        return Items.Item.ItemType.Background;
                    case 9:
                        return Items.Item.ItemType.Award;
                    case 10:
                    default:
                        return 0;
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            removeButton.Enabled = comboBox1.SelectedIndex < 9 && comboBox1.SelectedIndex != 6;
            UpdateSelectedTypeItems();
            loadPage(1);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            foreach (FollowBot bot in bots)
            {
                switch (comboBox1.SelectedIndex)
                {
                    case 0:
                        bot.UpdateHeadItem("0");
                        break;
                    case 1:
                        bot.UpdateFaceItem("0");
                        break;
                    case 2:
                        bot.UpdateNeckItem("0");
                        break;
                    case 3:
                        bot.UpdateBodyItem("0");
                        break;
                    case 4:
                        bot.UpdateHandItem("0");
                        break;
                    case 5:
                        bot.UpdateFeetItem("0");
                        break;
                    case 7:
                        bot.UpdateFlag("0");
                        break;
                    case 8:
                        bot.UpdatePhoto("0");
                        break;
                }
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            new AddItemForm(bots, this).ShowDialog(this);
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            loadPage(currentPage - 1);
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            loadPage(currentPage + 1);
        }
    }
}
